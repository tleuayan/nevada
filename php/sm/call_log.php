<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "call_log";
  $title = "Звонки";
  $page = $menu;
  $item_table = "call_log";  
  
  include("head.php"); 
  include("js.php"); 
	    
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php 
            include("left_menu.php"); 
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?php
            
            $filter_call_date_begin = $_REQUEST["filter_call_date_begin"];
            $filter_call_date_end = $_REQUEST["filter_call_date_end"];
            
            if($filter_call_date_begin!=null){
                    $filterSQL = " and c.calldate >= '".$filter_call_date_begin."'"; 
            }else{
                    $filterSQL = " and c.calldate >= '".$currBeginDate."'"; 
            }

            if($filter_call_date_end!=null){
                    $filterSQL = $filterSQL." and c.calldate <= '".$filter_call_date_end."'";		
            } else{
                    $filterSQL = $filterSQL." and c.calldate <= '".$currEndDate."' ";
            }    
            
            $filter_prefix_string = $_REQUEST["filter_prefix_string"];
            if($filter_prefix_string!=null){
                    $prefixSQL = "";
                    $prefix_arr = explode(";", $filter_prefix_string);
                    
                    for($i=0;$i<count($prefix_arr)-1;$i++){                        
                        if($prefixSQL==""){
                            $prefixSQL .=" SUBSTR(c.src,1,3)='".$prefix_arr[$i]."'";                       
                        }else{
                            $prefixSQL .=" or SUBSTR(c.src,1,3)='".$prefix_arr[$i]."'";                       
                        }                          
                    }
                    //echo " prefixSQL = ".$prefixSQL;
                    
                    $filterSQL .= " and (".$prefixSQL.")";
            }            

            $filter_status_string = $_REQUEST["filter_status_string"];
            if($filter_status_string!=null){
                    $statusSQL = "";
                    $status_arr = explode(";", $filter_status_string);
                    
                    for($i=0;$i<count($status_arr)-1;$i++){                        
                        if($statusSQL==""){
                            $statusSQL .=" c.disposition = '".$status_arr[$i]."' ";                       
                        }else{
                            $statusSQL .=" or c.disposition ='".$status_arr[$i]."' ";                       
                        }                          
                    }
                    //echo " prefixSQL = ".$prefixSQL;
                    
                    $filterSQL .= " and (".$statusSQL.")";
            }            

            $filter_callerid = $_REQUEST["filter_callerid"];
            if($filter_callerid!=null){                
                $filterSQL .= " and c.src like '%".$filter_callerid."%' ";
            }

            
            ?>
            
            
            <!--CONTENT--> 	
            <div>										
                <fieldset>
                    <legend>
                    <?php echo $title;?>
                    </legend>			
                    <div class="text-center">
                        
                        <div class="btn-group">
                            <button type="button" class="btn btn-success" onclick="showFilterModal()"><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Фильтр</button>                    
                            <button type="button" class="btn btn-info" onclick="refresh()">Обновить&nbsp;&nbsp;<span class="glyphicon glyphicon-refresh"></span></button>
                        </div>                        
                    </div>
                    
                    <div class="text-center">
                        
                        <?php 
                            $page_rows_number  = 100;
                            $query = " SELECT count(0) as counter";                            
                            $query .=" FROM ";
                            $query .=" asteriskcdrdb.cdr c LEFT JOIN works.call_log l ON l.cdr_id = c.uniqueid";
                            $query .=" WHERE 1=1 ";                            
                            $query .= $filterSQL;                                                                                    
                            $rs = mysql_query($query);	
                            
                            while($row= mysql_fetch_array($rs))								
                            {                    
                                $row_counter = $row["counter"];   
                            }    
                            $pages = $row_counter / $page_rows_number;
                            $page_half = $row_counter%$page_rows_number;
                            //echo "<br>row_counter = ".$row_counter;
                            //echo "<br>pages = ".$pages;
                            //echo "<br>page_half = ".$page_half;
                            
                            if($_REQUEST["page"]==null){
                                $page = 1;                                
                            }else{
                                $page = $_REQUEST["page"];                                                                
                            }          
                            
                            $limit_min = $page*100 - 100;
                            $limit_max = $page*100;
                            $limit_sql = " LIMIT ".$limit_min.",".$limit_max;
                            
                            $max_pages = round($pages,0,PHP_ROUND_HALF_DOWN);
                            
                            if($page_half<50){
                                $max_pages++;
                            }
                            //echo "<br>max_pages = ".$max_pages;
                        ?>
                        <ul class="pagination pagination-sm">
                            <li><a href="#"  onclick="goOnPage(1)">&laquo;</a></li>
                            <?php 
                                for($i=1;$i<=$max_pages;$i++){                                
                                    $class = ($page==$i)?"active":"";                            
                                    echo "<li class=".$class."><a href='#' onclick='goOnPage(".$i.")'>".$i."</a></li>";                                                   
                                }
                            ?>                                                      
                            <li><a href="#"  onclick="goOnPage(<?php echo $max_pages;?>)">&raquo;</a></li>
                        </ul>                    
                    </div>
                    
                     Звонки с <?php echo $limit_min+1;?> по <?php echo ($limit_max>$row_counter)?$row_counter:$limit_max;?> из <?php echo $row_counter;?>   
                     <table class="table table-condensed table-hover tab">							
                         <thead>
                             <TR>							
                                 <th class="text-center">Дата</th>                                                                  
                                 <th class="text-center">Номер</th>                                 
                                 <th class="text-center">Статус</th>                                 
                                 <th class="text-center">Общяя продолжительность</th>
                                 <th class="text-center">Продолжительность разговора</th>
                                 <th class="text-center">Логи</th>
                                 <th></th>									
                             </TR>
                         <thead>
                         <tbody>
                         <?php 
                            $counter = 0;
                            //$query = "select *, case when i.is_work_time=true then "true" else "false" end as is_work_time from ".$item_table." i  ";
                            $query = " SELECT *,";
                            $query .=" case when l.is_work_time=true then 'true' else 'false' end as is_work_time ";
                            $query .=" FROM ";
                            $query .=" asteriskcdrdb.cdr c LEFT JOIN works.call_log l ON l.cdr_id = c.uniqueid";
                            $query .=" WHERE 1=1 ";
                            //$query .=" c.uniqueid = l.cdr_id ";
                            $query .= $filterSQL;
                            $query .=" ORDER BY ";
                            $query .=" calldate desc";
                            $query .=$limit_sql;
                            //echo $query;
                            $rs = mysql_query($query);	
                            while($row= mysql_fetch_array($rs))								
                            {
                                $counter++;
                                /*
                                if($row["disposition"]=="ANSWERED"){
                                    $tr_class = "success";
                                }else if($row["disposition"]=="FAILED"){
                                    $tr_class = "danger";
                                }else if($row["disposition"]=="NO ANSWER"){
                                    $tr_class = "warning";
                                }else if($row["disposition"]=="BUSY"){
                                    $tr_class = "info";
                                }
                                */
                                ?>
                                <tr class="<?php echo $tr_class;?>">									
                                        <td class="col-xs-2 text-center"> 
                                            <?php echo $row["calldate"]?>
                                        </td>
                                        <td class="col-xs-2 text-center"> 
                                            <?php echo $row["src"]?>
                                        </td>
                                        <td class="col-xs-2 text-center"> 
                                            <?php 
                                            if($row["disposition"]=="ANSWERED"){
                                                echo "Отвечен";
                                            }else if($row["disposition"]=="FAILED"){
                                                echo "Не удалось";
                                            }else if($row["disposition"]=="NO ANSWER"){
                                                echo "Не отвечен";
                                            }else if($row["disposition"]=="BUSY"){
                                                echo "Занято";
                                            }
                                            ?>
                                        </td>
                                        
                                        <td class="col-xs-2 text-center"> 
                                            <?php echo $row["duration"]?> сек.
                                            ~
                                            <?php 
                                            $min = $row["duration"]/60;                                            
                                            $min_splitted = explode(".",$min);
                                            echo ($min_splitted[1]==0)?0:$min_splitted[0]+1;?> мин.                                            
                                        </td>
                                        <td class="col-xs-2 text-center"> 
                                            <?php echo $row["billsec"]?> сек.
                                            ~
                                            <?php 
                                            $min = $row["billsec"]/60;                                            
                                            $min_splitted = explode(".",$min);
                                            echo ($min_splitted[1]==0)?0:$min_splitted[0]+1;?> мин.                                            
                                        </td>
                                        <td class="col-xs-2 text-center">
                                            <a href = "/logs/<?php echo $row["uniqueid"]?>.html"><?php echo $row["uniqueid"]?></a>
                                        </td>
                                    </tr>									
                            <?php
                            }																																	
                            ?>							
                         </tbody>
                     </table>				
                </fieldset>
            </div>	

            <!--ITEM -->
            <div class="modal fade" id="FilterModal" name="FilterModal" tabindex="-1" role="dialog" aria-labelledby="FilterModal" aria-hidden="true">
                <form class="form-horizontal" role="form" id="FilterForm"  method="post">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" ><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Настройка фильтра</h4>
                                </div>
                                <div class="modal-body">		                                        
                                    <input type="hidden" id="page" name="page" value="<?php echo $page;?>">
                                    <input type="hidden" id="page_rows_number" name="page_rows_number" value="<?php echo $page_rows_number;?>">                                    
                                    <input type="hidden" id="filter_prefix_string" name="filter_prefix_string" value="<?php echo $filter_prefix_string;?>">
                                    <input type="hidden" id="filter_status_string" name="filter_status_string" value="<?php echo $filter_status_string;?>">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Дата</label>
                                            <div class="col-sm-4">
                                                <div class="input-group date" id="call_date_begin" data-date-format="YYYY-MM-DD HH:mm:ss">
                                                  <span class="input-group-addon">C</span>
                                                  <input type="text" class="form-control" id="filter_call_date_begin" name="filter_call_date_begin" value="<?php echo ($filter_call_date_begin!=null)?$filter_call_date_begin:$currBeginDate?>"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>                                                                                                        
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group date" id="call_date_end" data-date-format="YYYY-MM-DD HH:mm:ss">
                                                  <span class="input-group-addon">До</span>
                                                    <input type="text" class="form-control" id="filter_call_date_end" name="filter_call_date_end" value="<?php echo ($filter_call_date_end!=null)?$filter_call_date_end:$currEndDate?>"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>                                                                                                        
                                            </div>    
                                            <script type="text/javascript">
                                                $(function () {
                                                    $("#call_date_begin").datetimepicker();
                                                    $("#call_date_end").datetimepicker();
                                                    $("#call_date_begin").on("dp.change",function (e) {
                                                       $("#call_date_end").data("DateTimePicker").setMinDate(e.date);
                                                    });
                                                    $("#call_date_end").on("dp.change",function (e) {
                                                       $("#call_date_begin").data("DateTimePicker").setMaxDate(e.date);
                                                    });
                                                });
                                            </script>                                                

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Префикс</label>
                                            <div class="col-sm-5">
                                                <select id="filter_operator_prefix" name="filter_operator_prefix" class="selectpicker span12" multiple title='Выберите префикс...'>
                                                    <optgroup label="Kcell">                                                        
                                                        <option value="701">701</option>
                                                        <option value="702">702</option>
                                                        <option value="775">775</option>
                                                        <option value="778">778</option>
                                                    </optgroup>    
                                                    <optgroup label="Beeline">
                                                        <option value="777">777</option>
                                                        <option value="705">705</option>
                                                        <option value="771">771</option>                                                    
                                                    </optgroup>    
                                                    <optgroup label="Tele2">
                                                        <option value="707">707</option>
                                                    </optgroup>    

                                                </select>
                                            </div>    
                                        </div>         
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Номер</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="filter_callerid" name="filter_callerid" value="<?php echo $filter_callerid;?>" placeholder="Введите цифры номера">
                                            </div>
                                        </div>         
       
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Статус</label>
                                            <div class="col-sm-8">
                                                <select id="filter_status" id="filter_status" name="filter_status" class="selectpicker span10" multiple title='Выберите статус ...'>
                                                    <option value="ANSWERED">отвечен</option>
                                                    <option value="NO ANSWER">не отвечен</option>
                                                    <option value="BUSY">занято</option>
                                                    <option value="FAILED">не удалось</option>
                                                </select>
                                            </div>
                                        </div>   
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Общяя продолжительность</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                  <span class="input-group-addon">меньше</span></span>
                                                  <input type="text" class="form-control" id="filter_duration_less"  name="filter_duration_less">
                                                  <span class="input-group-addon">минут</span></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                  <span class="input-group-addon">больше</span></span>
                                                  <input type="text" class="form-control" id="filter_duration_greater"  name="filter_duration_greater">
                                                  <span class="input-group-addon">минут</span></span>
                                                </div>
                                            </div>
                                        </div>                                           
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Продолжительность разговора</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                  <span class="input-group-addon">меньше</span></span>
                                                  <input type="text" class="form-control" id="filter_billsec_less"  name="filter_billsec_less">
                                                  <span class="input-group-addon">минут</span></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                  <span class="input-group-addon">больше</span></span>
                                                  <input type="text" class="form-control" id="filter_billsec_greater"  name="filter_billsec_greater">
                                                  <span class="input-group-addon">минут</span></span>
                                                </div>
                                            </div>
                                        </div>                                           
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                    <button class="btn btn-primary"  type="button"  onclick="FilterSubmitForm()" >Применить</button>
                                </div>				  
                        </div>
                    </div>
                </form>                    
            </div>	            
            <script>
                
                function goOnPage(page_number){
                    document.getElementById("page").value = page_number;
                    set_parameters();
                    FilterSubmitForm();
                }
                
                function showFilterModal(){
                    document.getElementById("page").value = 1;
                    set_parameters();
                    $("#FilterModal").modal("show");					
                }            
                
                function FilterSubmitForm(){       
                    
                    //CALLERID PREFIX
                    var prefix_arr = document.getElementById("filter_operator_prefix");                    
                    var prefix_string = "";
                    for(var i=0;i<prefix_arr.length;i++){
                        if(prefix_arr[i].selected){
                             prefix_string += prefix_arr[i].value + ";";                          
                        }                        
                    }
                    document.getElementById("filter_prefix_string").value = prefix_string;                    
                    
                    
                    //CALL STATUS
                    var status_arr = document.getElementById("filter_status");                    
                    var status_string = "";
                    for(var i=0;i<status_arr.length;i++){
                        if(status_arr[i].selected){
                             status_string += status_arr[i].value + ";";                          
                        }                        
                    }
                    document.getElementById("filter_status_string").value = status_string;                      
                    
                    
                    //SUBMIT
                    document.getElementById("FilterForm").submit();               
                }
                
                function refresh(){
                    document.getElementById("FilterForm").submit();               
                }                
                
                function set_parameters(){   
                    
                    //CALLERID PREFIX
                    if(document.getElementById("filter_prefix_string").value.indexOf(";")!=-1){
                        var prefix_arr = document.getElementById("filter_prefix_string").value.split(";");
                        $('#filter_operator_prefix').selectpicker('val',prefix_arr);
                        $('#filter_operator_prefix').selectpicker('render');
                    }    
                    
                    //CALL STATUS
                    if(document.getElementById("filter_status_string").value.indexOf(";")!=-1){
                        var status_arr = document.getElementById("filter_status_string").value.split(";");
                        $('#filter_status').selectpicker('val',status_arr);
                        $('#filter_status').selectpicker('render');
                    }                     
                }
            </script>
            <!--CONTENT END--> 
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

  </body>
</html>
