<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "inbound_route";
  $title = "Список маршрутов";
  $page = "inbound_route";
  $item_table = "inbound_route";  
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
                                    $priority=0;
                                    $query="select max(priority) as priority from ".$item_table; 
                                    $rs = mysql_query($query);			
                                    while($row= mysql_fetch_array($rs))
                                    {
                                            $priority=$row["priority"]+1;
                                    }

                                    if($priority==NULL){
                                            $priority=1;
                                    }                                            

                                    $insert_query ="INSERT INTO ".$item_table."(";
                                    $insert_query.="description,";
                                    $insert_query.="custumer_id,";
                                    $insert_query.="is_active,";   
                                    $insert_query.="in_trunk,";                                            
                                    $insert_query.="in_did,";
                                    $insert_query.="in_callerid,";
                                    $insert_query.="out_route_type,";
                                    $insert_query.="out_trunk,";
                                    $insert_query.="out_did,";
                                    $insert_query.="out_callerid,";
                                    $insert_query.="ivr_id,";
                                    $insert_query.="menu_voice_id,";
                                    $insert_query.="priority,";
                                    $insert_query.="work_time,";                                            
                                    $insert_query.="create_date";
                                    $insert_query.=")";
                                    $insert_query.=" VALUES(";
                                    $insert_query.="'".$_REQUEST["item_description"]."',";
                                    $insert_query.="'".$_REQUEST["item_custumer_id"]."',";
                                    $is_active = ($_REQUEST["item_is_active"]=="on")?"true":"false";
                                    $insert_query.="'".$is_active."',";
                                    $insert_query.="'".$_REQUEST["item_in_trunk"]."',";
                                    $insert_query.="'".$_REQUEST["item_in_did"]."',";
                                    $insert_query.="'".$_REQUEST["item_in_callerid"]."',";
                                    $insert_query.="'".$_REQUEST["item_out_route_type"]."',";
                                    $insert_query.="'".$_REQUEST["item_out_trunk"]."',";
                                    $insert_query.="'".$_REQUEST["item_out_did"]."',";
                                    $insert_query.="'".$_REQUEST["item_out_callerid"]."',";
                                    $insert_query.="'".$_REQUEST["item_ivr_id"]."',";
                                    $insert_query.="'".$_REQUEST["item_menu_voice_id"]."',";
                                    $insert_query.="'".$priority."',";
                                    $insert_query.="'".$_REQUEST["item_work_time"]."',";
                                    $insert_query.="sysdate()";		
                                    $insert_query.=")";
                                     
                                    //echo "====".$insert_query."===*<br>";
                                    mysql_query($insert_query);	

                                    ?>
                                        <script>			
                                            location.href = "<?php echo $page;?>.php?message=Запись успешно добавлена!";
                                        </script>		
                                    <?php	
                                        
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_description"]."', ";					
					$query.=" custumer_id = '".$_REQUEST["item_custumer_id"]."', ";			
                                        $is_active = ($_REQUEST["item_is_active"]=="on")?"true":"false";
                                        $query.=" is_active = ".$is_active.", ";
                                        $query.=" in_trunk = '".$_REQUEST["item_in_trunk"]."',";
					$query.=" in_did = '".$_REQUEST["item_in_did"]."',";					
					$query.=" in_callerid = '".$_REQUEST["item_in_callerid"]."',";
                                        $query.=" out_route_type = '".$_REQUEST["item_out_route_type"]."',";
					$query.=" out_trunk = '".$_REQUEST["item_out_trunk"]."',";
					$query.=" out_did = '".$_REQUEST["item_out_did"]."',";					
					$query.=" out_callerid = '".$_REQUEST["item_out_callerid"]."',";
                                        $query.=" work_time='".$_REQUEST["item_work_time"]."',";
					$query.=" menu_voice_id = '".$_REQUEST["item_menu_voice_id"]."', ";
                                        $query.=" ivr_id = '".$_REQUEST["item_ivr_id"]."'";
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
                                            location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
                                
                                if($_REQUEST["priority_item_id"]){
                                    $tmp_old_pr="select priority from ".$item_table." where id='".$_REQUEST['priority_item_id']."'" ;
                                    $rs_old_pr = mysql_query($tmp_old_pr);
                                    //echo $tmp_old_pr."</br>";
                                    while($row_old_pr = mysql_fetch_array($rs_old_pr))
                                    {
                                            $old_pr=$row_old_pr['priority'];                                            
                                    }			

                                    //echo "st=".$_REQUEST['priority_action']."<br>";
                                    if($_REQUEST['priority_action']=="<")
                                    {
                                            $order="desc";
                                    }
                                    else
                                    {
                                            $order="asc";
                                    }
                                    $tmp_old_pr="select id,priority from ".$item_table." where priority".$_REQUEST['priority_action'].$old_pr." order by priority ".$order." limit 0,1";
                                    //echo $tmp_old_pr."<br>";
                                    $rs_old_pr = mysql_query($tmp_old_pr);
                                    while($row_old_pr = mysql_fetch_array($rs_old_pr))
                                    {
                                            $new_pr=$row_old_pr['priority'];
                                            $sibling_node_id=$row_old_pr['id'];					
                                    }			

                                    $query="update ".$item_table." set priority=".$new_pr." where id=".$_REQUEST['priority_item_id'];
                                   // echo $query."<br>";
                                    $rs = mysql_query($query);	

                                    $query="update ".$item_table." set priority=".$old_pr." where id=".$sibling_node_id;
                                    //echo $query."<br>";
                                    $rs = mysql_query($query);	
                                    ?>
                                    <script>			
                                            location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
                                    </script>		
                                    <?php				
		                                    
                                    
                                }
			?>
		   
			<div>										
				<fieldset>
					<legend>
					<?php echo $title;?>
					</legend>	
					<?php include("ivr_title.php");?>								
					<div class="alert alert-danger">
						<b>DID</b> - номер на который происходит звонок, <b>CALLERID</b> - номер c которого происходит звонок. 
					</div>
					<table class="table table-condensed table-hover" border="0">							
						<thead>
							<TR>													
								<th colspan="4"></th>
								<th colspan="3" class="text-center" style="border-left: solid 1px grey;border-right: solid 1px grey;">Прием звонка</th>								
								<th colspan="4" class="text-center" style="border-left: solid 1px grey;border-right: solid 1px grey;">Обработка звонка</th>								
								<th></th>																
							</TR>					
							<TR>	
                                                                <th class="text-center">Активен</th>
								<th class="text-center">Описание</th>
								<th class="text-center">Клиент</th>
                                                                <th class="text-center">Голос</th>
                                                                <th class="text-center" style="border-left: solid 1px grey;">Канал</th>
								<th class="text-center">DID</th>																							
								<th class="text-center" style="border-right: solid 1px grey;">CALLERID</th>								
								<th class="text-center">Канал</th>
								<th class="text-center">DID</th>
								<th class="text-center">CALLERID</th>
								<th class="text-center" style="border-right: solid 1px grey;">Меню IVR</th>								
								<th></th>
																
							</TR>
						<thead>
                                                <tbody>
							<?php 
							$counter = 0;
							$query =" SELECT ";
                                                        $query.=" i.*,";
                                                        $query.=" case when i.is_active=true then 'true' else 'false' end as is_active,";
                                                        $query.=" c.description as c_description,";
                                                        $query.=" ivr.description as ivr_description,";
                                                        $query.=" m.id as m_id,";
                                                        $query.=" m.description as m_description ";
                                                        $query.=" FROM ".$item_table." i  LEFT JOIN ivr ON i.ivr_id = ivr.id,custumer c,menu_voice m  ";
                                                        $query.=" WHERE i.menu_voice_id = m.id and i.custumer_id = c.id order by i.priority";
							//echo $query;
							$rs = mysql_query($query);	
                                                        $row_counter =mysql_num_rows($rs);
                                                        
							while($row= mysql_fetch_array($rs))								
							{
                                                            $counter++;
							?>
                                                                <tr>		
                                                                    <td style="display: none;"
                                                                            id="work_time_<?php echo $row["id"]?>" 
                                                                            value="<?php echo $row["work_time"]?>" 
                                                                    > 
                                                                            <?php echo $row["work_time"]?>
                                                                    </td>                                                                    
                                                                    <td class="col-xs-1 text-center" style="vertical-align: middle;"
										id="is_active_<?php echo $row["id"]?>" 
										value="<?php echo $row["is_active"];?>" 
									> 
                                                                            <input type="checkbox" <?php echo ($row["is_active"]=="true")?"checked":""?> disabled>
									</td>
                                                                        
                                                                        <td class="col-xs-1 text-center"  style="vertical-align: middle;"
										id="description_<?php echo $row["id"]?>" 
										value="<?php echo $row["description"]?>" 
									> 
										<?php echo $row["description"]?>
									</td>
									
									<td class="col-xs-1 text-center"  style="vertical-align: middle;"
										id="custumer_id_<?php echo $row["id"]?>" 
										value="<?php echo $row["custumer_id"]?>" 
									> 
										<?php echo $row["c_description"]?>
									</td> 
                                                                        <td class="col-xs-1 text-center"   style="vertical-align: middle;"
                                                                                id="menu_voice_id_<?php echo $row["id"]?>" 
                                                                                value="<?php echo $row["m_id"]?>" 
                                                                        > 
                                                                                <?php echo $row["m_description"]?>
                                                                        </td>

				                                        <td class="col-xs-1 text-center" style="vertical-align: middle;border-left: solid 1px grey;"
										id="in_trunk_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_trunk"]?>" 
									> 
                                                                                <?php echo ($row["in_trunk"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["in_trunk"];?>	
                                                                            
									</td>									
									
									<td class="col-xs-1 text-center"  style="vertical-align: middle;"
										id="in_did_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_did"]?>" 
									> 
										<?php echo ($row["in_did"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["in_did"];?>
									</td>

									<td class="col-xs-1 text-center" style="border-right: solid 1px grey;vertical-align: middle;"
										id="in_callerid_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_callerid"]?>" 
									> 
										<?php echo ($row["in_callerid"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["in_callerid"];?>
									</td>	
                                                                        
                                                                        <td class="col-xs-1 text-center"  style="display: none;"
										id="out_route_type_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_route_type"]?>" 
									> 
                                                                        <?php echo ($row["out_route_type"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["out_route_type"];?>	                                                                                
									</td>	
                                                                        
									<td class="col-xs-1 text-center" style="border-left: solid 1px grey;vertical-align: middle;"
										id="out_trunk_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_trunk"]?>" 
									> 
                                                                        <?php echo ($row["out_trunk"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["out_trunk"];?>	                                                                                
									</td>									
									
									<td class="col-xs-1 text-center" style="vertical-align: middle;"
										id="out_did_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_did"]?>" 
									> 
                                                                        <?php echo ($row["out_did"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["out_did"];?>	                                                                                
									</td>

									<td class="col-xs-1 text-center" style="vertical-align: middle;"
										id="out_callerid_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_callerid"]?>" 
									> 
										<?php echo ($row["out_callerid"]=="")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["out_callerid"];?>
									</td>	
	
									<td class="col-xs-1 text-center" style="border-right: solid 1px grey;vertical-align: middle;"
										id="ivr_id_<?php echo $row["id"]?>" 
										value="<?php echo $row["ivr_id"]?>" 
									> 
                                                                        <?php echo ($row["ivr_description"]=="FOREIGN")?"<span class='glyphicon glyphicon-asterisk'></span>":$row["ivr_description"];?>	                                                                            
									</td>										
									
									<td class="col-xs-2 text-right"  style="vertical-align: middle;">
                                                                                <?php                                                                                                        
                                                                                    if($counter==1){$up_visibility = "hidden";}else{$up_visibility = "visible";}
                                                                                    if($counter==$row_counter){$down_visibility = "hidden";}else{$down_visibility = "visible";}
                                                                                ?>
                                                                                
                                                                                <button style="visibility: <?php echo $down_visibility?>" data-toggle="modal" type="button" class="btn btn-default" action='>'  id="<?php echo $row["id"]?>" onclick="change_priority(this)"  >
											<span class="glyphicon glyphicon-chevron-down"></span>
										</button>	
										<button style="visibility: <?php echo $up_visibility?>" data-toggle="modal" type="button" class="btn btn-default" action='<'  id="<?php echo $row["id"]?>" onclick="change_priority(this)"  >
											<span class="glyphicon glyphicon-chevron-up"></span>
										</button>	
                                                                            
										<!--button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
											<span class="glyphicon glyphicon-pencil"></span>
										</button-->	

										<!--button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
											<span class="glyphicon glyphicon-trash"></span>
										</button-->											
									</td>										
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
					<!-- Button trigger modal -->
					<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
					  <span class="glyphicon glyphicon-plus"></span> Добавить
					</button>					
				</fieldset>
			</div>	

                        <form id="PriorityForm" method="post" >						                                				
                                <input type="hidden" id="priority_item_id" name="priority_item_id"  value="" />
                                <input type="hidden" id="priority_action" name="priority_action"  value="" />										
                        </form>
                                        
			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">
                            <div class="modal-dialog modal-lg">
				<div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" >
                                            <span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> маршрута</h4>
                                    </div>
                                    <div class="modal-body">		
                                        <input type="hidden" id="item_action" name="item_action" value="">	
                                        <input type="hidden" id="item_id" name="item_id" value="">                                                  
                                        
                                        <input type="hidden" id="item_work_time" name="item_work_time" value="">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Описание</label>
                                            <div class="col-sm-8">
                                              <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
                                            </div>
                                        </div>

                                        <div class="form-group" style="vertical-align: middle;">
                                              <label class="col-sm-4 control-label" >                                                        
                                                  Клиент                                                        
                                              </label>

                                              <div class="col-sm-8">
                                                <select class="form-control" id="item_custumer_id" name="item_custumer_id">
                                                              <option value="">не выбран</option>
                                                              <?php
                                                              $c_query="select * from custumer";									
                                                              $c_rs = mysql_query($c_query);	
                                                              while($c_row= mysql_fetch_array($c_rs))								
                                                              {?>
                                                                      <option value="<?php echo $c_row["id"]?>"><?php echo $c_row["description"]?></option>									
                                                              <?php
                                                              }
                                                              ?>
                                                </select>
                                              </div>
                                        </div>

                                        <div class="form-group">
                                              <label class="col-sm-4 control-label">Голос меню</label>
                                              <div class="col-sm-8">							  
                                                <select class="form-control" id="item_menu_voice_id" name="item_menu_voice_id">
                                                              <option value="">не выбран</option>
                                                              <?php
                                                              $c_query="select * from menu_voice";									
                                                              $c_rs = mysql_query($c_query);	
                                                              while($c_row= mysql_fetch_array($c_rs))								
                                                              {?>
                                                                      <option value="<?php echo $c_row["id"]?>"><?php echo $c_row["description"]?></option>									
                                                              <?php
                                                              }
                                                              ?>
                                                </select>
                                              </div>
                                        </div>

                                        <div class="form-group" style="vertical-align: middle;">
                                            <label class="col-sm-4 control-label" >Активен</label>
                                            <div class="col-sm-8">
                                                <div class="checkbox">
                                                    <label>
                                                       <input type="checkbox" id="item_is_active" name="item_is_active">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <span id="work_time">
                                            <div class="form-group">
                                                <label class="col-md-offset-3 col-sm-4 control-label">График рабочего времени</label>
                                            </div>									

                                            <div class="form-group" >
                                                <label class="col-sm-4 control-label">Понедельник</label>										
                                                <div class="col-sm-1">
                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_monday" name="item_monday">										
                                                </div>
                                                <div class="col-sm-2">
                                                    С <input type="time"  value="09:00"  id="item_monday_begin" name="item_monday_begin">										
                                                </div>
                                                <div class="col-sm-2">
                                                    До <input type="time"  value="18:00"  id="item_monday_end" name="item_monday_end">										
                                                </div>
                                            </div>
                                            <div class="form-group" >
                                                <label class="col-sm-4 control-label">Вторник</label>										
                                                <div class="col-sm-1">
                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_tuesday" name="item_tuesday">										
                                                </div>
                                                <div class="col-sm-2">
                                                    С <input type="time"  value="09:00"  id="item_tuesday_begin" name="item_tuesday_begin">										
                                                </div>
                                                <div class="col-sm-2">
                                                    До <input type="time"  value="18:00"  id="item_tuesday_end" name="item_tuesday_end">										
                                                </div>
                                            </div>
                                            <div class="form-group" >
                                                <label class="col-sm-4 control-label">Среда</label>										
                                                <div class="col-sm-1">
                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_wednesday" name="item_wednesday">										
                                                </div>
                                                <div class="col-sm-2">
                                                    С <input type="time"  value="09:00"  id="item_wednesday_begin" name="item_wednesday_begin">										
                                                </div>
                                                <div class="col-sm-2">
                                                    До <input type="time"  value="18:00"  id="item_wednesday_end" name="item_wednesday_end">										
                                                </div>
                                            </div>
                                            <div class="form-group" >
                                                <label class="col-sm-4 control-label">Четвег</label>										
                                                <div class="col-sm-1">
                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_thursday" name="item_thursday">										
                                                </div>
                                                <div class="col-sm-2">
                                                    С <input type="time"  value="09:00"  id="item_thursday_begin" name="item_thursday_begin">										
                                                </div>
                                                <div class="col-sm-2">
                                                    До <input type="time"  value="18:00"  id="item_thursday_end" name="item_thursday_end">										
                                                </div>
                                            </div>
                                            <div class="form-group" >
                                                  <label class="col-sm-4 control-label">Пятница</label>										
                                                  <div class="col-sm-1">
                                                      <input type="checkbox" checked style="margin-top: 10px;" id="item_friday" name="item_friday">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      С <input type="time"  value="09:00"  id="item_friday_begin" name="item_friday_begin">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      До <input type="time"  value="18:00"  id="item_friday_end" name="item_friday_end">										
                                                  </div>
                                            </div>
                                            <div class="form-group" >
                                                  <label class="col-sm-4 control-label">Суббота</label>										
                                                  <div class="col-sm-1">
                                                      <input type="checkbox" style="margin-top: 10px;" id="item_saturday" name="item_saturday">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      С <input type="time"  value="09:00"  id="item_saturday_begin" name="item_saturday_begin">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      До <input type="time"  value="18:00"  id="item_saturday_end" name="item_saturday_end">										
                                                  </div>
                                            </div>
                                            <div class="form-group" >
                                                  <label class="col-sm-4 control-label">Воскресение</label>										
                                                  <div class="col-sm-1">
                                                      <input type="checkbox" style="margin-top: 10px;" id="item_sunday" name="item_sunday">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      С <input type="time"  value="09:00"  id="item_sunday_begin" name="item_sunday_begin">										
                                                  </div>
                                                  <div class="col-sm-2">
                                                      До <input type="time"  value="18:00"  id="item_sunday_end" name="item_sunday_end">										
                                                  </div>
                                            </div>
                                        </span>                                                      
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                Прием звонка
                                            </div>
                                            <div class="panel-footer" id="in_route_footer">
                                                <div class="form-group" id="in_trunk_div">
                                                    <label class="col-sm-4 control-label" >Канал</label>
                                                    <div class="col-sm-8">							  
                                                        <select id="item_in_trunk" name="item_in_trunk" class="form-control inparameter">
                                                            <option value="" >не выбран</option>
                                                            <?php
                                                            while($trunk_row= mysql_fetch_array($trunk_rs))
                                                            {
                                                            ?>
                                                              <option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>

                                                            <?php
                                                            }								
                                                            ?>
                                                        </select>									  
                                                    </div>
                                                </div>
                                                <div class="form-group" id="in_did_div">
                                                    <label class="col-sm-4 control-label">DID</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control inparameter" id="item_in_did" name="item_in_did" placeholder="Введите номер">
                                                    </div>
                                                </div>	
                                                <div class="form-group" id="in_callerid_div">
                                                    <label class="col-sm-4 control-label">CALLERID</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control inparameter" id="item_in_callerid" name="item_in_callerid" placeholder="Введите номер">
                                                    </div>
                                                </div>                                                          
                                            </div>
                                        </div>                                                  

                                        <div class="panel panel-default">
                                            <div class="panel-body">                                                          
                                                Обработка звонка
                                            </div>
                                            <div class="panel-footer">						  
                                                
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Тип обработки</label>
                                                    <div class="col-sm-8">
                                                        <select class="form-control" id="item_out_route_type" name="item_out_route_type" value="" onclick="set_out_route_type()">
                                                            <option value="redirect">Переадресация</option>       
                                                            <option value="ivr">Меню IVR</option>                                                                       
                                                        </select>                                                                
                                                    </div>
                                                </div>	
                                                
                                                <span style="display: none;" id="out_route_footer">
                                                
                                                    <div class="form-group"  id="out_trunk_div">
                                                        <label class="col-sm-4 control-label">Канал</label>
                                                        <div class="col-sm-8">							  
                                                            <select class="form-control" id="item_out_trunk" name="item_out_trunk" >
                                                                <option value="" >не выбран</option>
                                                                <?php
                                                                mysql_data_seek($trunk_rs, 0);
                                                                while($trunk_row= mysql_fetch_array($trunk_rs))
                                                                {
                                                                ?>
                                                                        <option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>

                                                                <?php
                                                                }								
                                                                ?>
                                                            </select>									  
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="out_did_div">
                                                        <label class="col-sm-4 control-label">DID</label>
                                                        <div class="col-sm-8">
                                                          <input type="text" class="form-control" id="item_out_did" name="item_out_did" placeholder="Введите номер">
                                                        </div>
                                                    </div>	
                                                    <div class="form-group" id="out_callerid_div">
                                                        <label class="col-sm-4 control-label">CALLERID</label>
                                                        <div class="col-sm-8">
                                                          <input type="text" class="form-control" id="item_out_callerid" name="item_out_callerid" placeholder="Введите номер">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="out_ivr_div">
                                                        <label class="col-sm-4 control-label">Меню IVR</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="item_ivr_id" name="item_ivr_id">
                                                                <option value="1">не выбран</option>
                                                                <?php
                                                                $ivr_query="select * from ivr where id <> 1   ";									
                                                                $ivr_rs = mysql_query($ivr_query);	
                                                                while($ivr_row= mysql_fetch_array($ivr_rs))								
                                                                {?>
                                                                        <option value="<?php echo $ivr_row["id"]?>"><?php echo $ivr_row["description"]?></option>									
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>														  
                                                        </div>
                                                    </div>
                                                </span>    
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                        <button class="btn btn-primary"  type="submit">Выполнить</button>
                                    </div>				  
				</div>
                            </div>
                            </form>
                                
			</div>			
			
			<script>
                                
                                $(document).ready(function() {
                                    $('#ItemForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        submitHandler: function(validator, form, submitButton) {
                                            getWorkTime();
                                            document.getElementById("ItemForm").submit();						                                          
                                        },                                        
                                        fields: {
                                            item_description: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },
                                            item_custumer_id: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },
                                            item_menu_voice_id: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },
                                            item_out_route_type: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },                                            
                                            inparameter: {
                                                selector: '.inparameter',
                                                validators: {
                                                    callback: {
                                                        message: 'Заполните хотя бы 1 из 3 полей Канал,DID,CALLERID ',
                                                        callback: function(value, validator) {
                                                            var inparameters = validator.getFieldElements('inparameter'),
                                                                length     = inparameters.length,
                                                                are_all_empty = true;
                                                            for (var i = 0; i < length; i++) {
                                                                if($(inparameters[i]).val()!=""){
                                                                    are_all_empty = false;
                                                                }
                                                            }
                                                            if (!are_all_empty) {
                                                                validator.updateStatus('inparameter', 'VALID', 'callback');
                                                                return true;
                                                            }

                                                            return false;
                                                        }
                                                    }
                                                }
                                            },
                                            ivr: {
                                                selector: '#item_ivr_id',
                                                validators: {
                                                    callback: {
                                                        message: 'Выберите Меню IVR',
                                                        callback: function(value, validator) {                                                            
                                                            var out_route_type = document.getElementById("item_out_route_type").value;                                                                
                                                            if(out_route_type=="ivr" && value!="1"){
                                                                validator.updateStatus('ivr', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                            return false;
                                                        }
                                                    }
                                                }
                                            },
                                            out_trunk: {
                                                selector: '#item_out_trunk',
                                                validators: {
                                                    callback: {
                                                        message: 'Выберите Канал',
                                                        callback: function(value, validator) {                                                            
                                                            var out_route_type = document.getElementById("item_out_route_type").value;                                                                
                                                            if(out_route_type=="redirect" && value!=""){
                                                                validator.updateStatus('ivr', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                            return false;
                                                        }
                                                    }
                                                }
                                            },
                                            out_did: {
                                                selector: '#item_out_did',
                                                validators: {
                                                    callback: {
                                                        message: 'Введите номер DID',
                                                        callback: function(value, validator) {                                                            
                                                            var out_route_type = document.getElementById("item_out_route_type").value;                                                                
                                                            if(out_route_type=="redirect" && value!=""){
                                                                validator.updateStatus('ivr', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                            return false;
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                    });
                                });

                                function showItemModal(obj){
                                        $('#ItemForm').data('bootstrapValidator').resetForm(true);
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	                    
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");	
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}							
                                        $('#ItemModal').modal('show');				
				}


                                function setWorkTime(){
                                    var action = document.getElementById("item_action").value;
                                    if(action=="edit"){
                                        var item_id = document.getElementById("item_id").value;
                                        var work_timeString = document.getElementById("work_time_" + item_id).getAttribute("value")+"";                                              
                                        document.getElementById("item_monday").checked =(work_timeString.split(";")[0].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_monday_begin").value = work_timeString.split(";")[0].split("/")[1].split("-")[0];
                                        document.getElementById("item_monday_end").value = work_timeString.split(";")[0].split("/")[1].split("-")[1];

                                        document.getElementById("item_tuesday").checked =(work_timeString.split(";")[1].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_tuesday_begin").value = work_timeString.split(";")[1].split("/")[1].split("-")[0];
                                        document.getElementById("item_tuesday_end").value = work_timeString.split(";")[1].split("/")[1].split("-")[1];

                                        document.getElementById("item_wednesday").checked =(work_timeString.split(";")[2].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_wednesday_begin").value = work_timeString.split(";")[2].split("/")[1].split("-")[0];
                                        document.getElementById("item_wednesday_end").value = work_timeString.split(";")[2].split("/")[1].split("-")[1];

                                        document.getElementById("item_thursday").checked =(work_timeString.split(";")[3].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_thursday_begin").value = work_timeString.split(";")[3].split("/")[1].split("-")[0];
                                        document.getElementById("item_thursday_end").value = work_timeString.split(";")[3].split("/")[1].split("-")[1];

                                        document.getElementById("item_friday").checked =(work_timeString.split(";")[4].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_friday_begin").value = work_timeString.split(";")[4].split("/")[1].split("-")[0];
                                        document.getElementById("item_friday_end").value = work_timeString.split(";")[4].split("/")[1].split("-")[1];

                                        document.getElementById("item_saturday").checked =(work_timeString.split(";")[5].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_saturday_begin").value = work_timeString.split(";")[5].split("/")[1].split("-")[0];
                                        document.getElementById("item_saturday_end").value = work_timeString.split(";")[5].split("/")[1].split("-")[1];

                                        document.getElementById("item_sunday").checked =(work_timeString.split(";")[6].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_sunday_begin").value = work_timeString.split(";")[6].split("/")[1].split("-")[0];
                                        document.getElementById("item_sunday_end").value = work_timeString.split(";")[6].split("/")[1].split("-")[1];
                                     }else{
                                        document.getElementById("item_monday").checked = true;                                    
                                        document.getElementById("item_monday_begin").value = "09:00";
                                        document.getElementById("item_monday_end").value = "18:00";

                                        document.getElementById("item_tuesday").checked = true;                                                                        
                                        document.getElementById("item_tuesday_begin").value = "09:00";
                                        document.getElementById("item_tuesday_end").value = "18:00";
                                        
                                        document.getElementById("item_wednesday").checked = true;                                                                       
                                        document.getElementById("item_wednesday_begin").value = "09:00";
                                        document.getElementById("item_wednesday_end").value = "18:00";

                                        document.getElementById("item_thursday").checked = true;                                                             
                                        document.getElementById("item_thursday_begin").value = "09:00";
                                        document.getElementById("item_thursday_end").value = "18:00";

                                        document.getElementById("item_friday").checked = true;                                                                       
                                        document.getElementById("item_friday_begin").value = "09:00";
                                        document.getElementById("item_friday_end").value = "18:00";

                                        document.getElementById("item_saturday").checked =false;                                    
                                        document.getElementById("item_saturday_begin").value = "09:00";
                                        document.getElementById("item_saturday_end").value = "18:00";

                                        document.getElementById("item_sunday").checked =false;                                    
                                        document.getElementById("item_sunday_begin").value = "09:00";
                                        document.getElementById("item_sunday_end").value = "18:00";
                                         
                                     }   
                                }
                                
                                function getWorkTime(){
                                    
                                    var work_timeString = "";
                                    var monday = document.getElementById("item_monday").checked;
                                    var monday_begin = document.getElementById("item_monday_begin").value;
                                    var monday_end = document.getElementById("item_monday_end").value;
                                    var tuesday = document.getElementById("item_tuesday").checked;
                                    var tuesday_begin = document.getElementById("item_tuesday_begin").value;
                                    var tuesday_end = document.getElementById("item_tuesday_end").value;
                                    var wednesday = document.getElementById("item_wednesday").checked;
                                    var wednesday_begin = document.getElementById("item_wednesday_begin").value;
                                    var wednesday_end = document.getElementById("item_wednesday_end").value;
                                    var thursday = document.getElementById("item_thursday").checked;
                                    var thursday_begin = document.getElementById("item_thursday_begin").value;
                                    var thursday_end = document.getElementById("item_thursday_end").value;
                                    var friday = document.getElementById("item_friday").checked;
                                    var friday_begin = document.getElementById("item_friday_begin").value;
                                    var friday_end = document.getElementById("item_friday_end").value;
                                    var saturday = document.getElementById("item_saturday").checked;
                                    var saturday_begin = document.getElementById("item_saturday_begin").value;
                                    var saturday_end = document.getElementById("item_saturday_end").value;
                                    var sunday = document.getElementById("item_sunday").checked;
                                    var sunday_begin = document.getElementById("item_sunday_begin").value;
                                    var sunday_end = document.getElementById("item_sunday_end").value;
                                    
                                    if(monday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + monday_begin + "-" + monday_end + ";";

                                    if(tuesday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + tuesday_begin + "-" + tuesday_end + ";";

                                    if(wednesday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + wednesday_begin + "-" + wednesday_end + ";";

                                    if(thursday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + thursday_begin + "-" + thursday_end + ";";

                                    if(friday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + friday_begin + "-" + friday_end + ";";

                                    if(saturday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + saturday_begin + "-" +saturday_end + ";";

                                    if(sunday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + sunday_begin + "-" + sunday_end + ";";
                                    
                                    document.getElementById("item_work_time").value = work_timeString;
                                }
                            
                                function change_priority(obj)
                                {
                                        document.getElementById("priority_item_id").value=obj.id;					
                                        document.getElementById("priority_action").value=obj.getAttribute("action");														
                                        document.getElementById("PriorityForm").submit();
                                }
                                function set_out_route_type(){
                                    var type  = document.getElementById("item_out_route_type").value;
                                    if(type=="redirect"){                                        
                                        document.getElementById("out_route_footer").style.display ="";
                                        document.getElementById("out_trunk_div").style.display ="";
                                        document.getElementById("out_did_div").style.display ="";
                                        document.getElementById("out_callerid_div").style.display ="";
                                        document.getElementById("out_ivr_div").style.display ="none";  
                                        document.getElementById("item_ivr_id").value = "1";
                                    }else if(type=="ivr"){                                        
                                        document.getElementById("out_route_footer").style.display =""; 
                                        document.getElementById("out_trunk_div").style.display ="none";
                                        document.getElementById("out_did_div").style.display ="none";                                        
                                        document.getElementById("out_callerid_div").style.display ="none";                                                       
                                        document.getElementById("out_ivr_div").style.display ="";                                        
                                        document.getElementById("item_out_callerid").value = "";
                                        document.getElementById("item_out_did").value = "";
                                        document.getElementById("item_out_trunk").value = "";
                                    }else{                                        
                                        document.getElementById("out_route_footer").style.display ="none"; 
                                        document.getElementById("out_trunk_div").style.display ="none";
                                        document.getElementById("out_did_div").style.display ="none";                                        
                                        document.getElementById("out_callerid_div").style.display ="none";                                                       
                                        document.getElementById("out_ivr_div").style.display ="none";                                        
                                        document.getElementById("item_out_callerid").value = "";
                                        document.getElementById("item_out_did").value = "";
                                        document.getElementById("item_out_trunk").value = "";
									
                                    }
                                }                                
			
				
				function setItemParameters(){		
                                        var action = document.getElementById("item_action").value;                                        
					var item_id = document.getElementById("item_id").value;	
                                        setWorkTime();
                                        if(action=="new"){
                                            document.getElementById("item_is_active").checked = true;
                                            document.getElementById("item_description").value = "";				
                                            document.getElementById("item_custumer_id").value = "";
                                            document.getElementById("item_in_trunk").value = "";
                                            document.getElementById("item_in_did").value = "";
                                            document.getElementById("item_in_callerid").value = "";
                                            document.getElementById("item_out_route_type").value = "";
                                            document.getElementById("item_out_trunk").value = "";
                                            document.getElementById("item_out_did").value = "";
                                            document.getElementById("item_out_callerid").value = "";
                                            document.getElementById("item_ivr_id").value = "1";
                                            document.getElementById("item_menu_voice_id").value = "";
                                            set_out_route_type();  											
                                        }else{                                                                                        
                                            document.getElementById("item_is_active").checked = document.getElementById("is_active_" + item_id).getAttribute("value");
                                            
                                            if(document.getElementById("is_active_" + item_id).getAttribute("value")=="false"){
                                                document.getElementById("item_is_active").checked = false;
                                            }else{
                                                document.getElementById("item_is_active").checked = true;
                                            }               
                                            
                                            document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");				
                                            document.getElementById("item_custumer_id").value = document.getElementById("custumer_id_" + item_id).getAttribute("value");			
                                            document.getElementById("item_in_trunk").value = document.getElementById("in_trunk_" + item_id).getAttribute("value");									
                                            document.getElementById("item_in_did").value = document.getElementById("in_did_" + item_id).getAttribute("value");				
                                            document.getElementById("item_in_callerid").value = document.getElementById("in_callerid_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_route_type").value = document.getElementById("out_route_type_" + item_id).getAttribute("value");														
                                            document.getElementById("item_out_trunk").value = document.getElementById("out_trunk_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_did").value = document.getElementById("out_did_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_callerid").value = document.getElementById("out_callerid_" + item_id).getAttribute("value");				
                                            document.getElementById("item_ivr_id").value = document.getElementById("ivr_id_" + item_id).getAttribute("value");				
                                            document.getElementById("item_menu_voice_id").value = document.getElementById("menu_voice_id_" + item_id).getAttribute("value");										
                                            set_out_route_type(); 
                                            
                                        }    
				}
                                /*
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
                                        var action = document.getElementById("item_action").value;
                                        var out_route_type = document.getElementById("item_out_route_type").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
                                                        getWorkTime();
                                                	document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					
					var description = document.getElementById("item_description").value;
					var custumer_id = document.getElementById("item_custumer_id").value;
					var in_trunk = document.getElementById("item_in_trunk").value;
					var in_did = document.getElementById("item_in_did").value;
					var in_callerid = document.getElementById("item_in_callerid").value;                                        
					var out_trunk = document.getElementById("item_out_trunk").value;
					var out_did = document.getElementById("item_out_did").value;
					var out_callerid = document.getElementById("item_out_callerid").value;					
					var ivr_id = document.getElementById("item_ivr_id").value;					
					var menu_voice_id = document.getElementById("item_menu_voice_id").value;
					
                                        
                                        if(in_trunk=="" && in_did=="" && in_callerid==""){
                                            error_text+="Заполните хотя бы одно поле Приема звонка!!!\n";				                                                                                        
                                        }
                                        
                                        
                                        var out_route_type = document.getElementById("item_out_route_type").value;
                                        if(out_route_type==""){
                                            error_text+="Выберите способ Обработки звонка !!!\n";
                                        }else{
                                            if(out_route_type=="redirect"){
                                                
                                                if(out_trunk==""){
                                                    error_text+="Выберите 'Канал' Обработки звонка !!!\n";
                                                }											
                                                if(out_did==""){
                                                    error_text+="Выберите 'DID' Обработки звонка !!!\n";
                                                }                                                
                                           }else{
                                                if(ivr_id==""){
                                                    error_text+="Выберите 'Меню IVR ' Обработки звонка !!!\n";
                                                }
                                            }                                          
                                        }

					return 	error_text;
				}
                                 */
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>
