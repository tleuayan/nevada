﻿<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "ivr";
  $ivr_menu = "ivr";
  $title = "Список IVR";
  $img_width  = "60px";
  $img_height = "60px";  
  //$item_img = "<img src='img/list-8.png' width='".$img_width."' height='".$img_height."' class='img-thumbnail'>";
  $page = "ivr";
  $item_table = "ivr";  
  $ivr_id = "";
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		    <!--CONTENT--> 	
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
				
					$insert_query ="INSERT INTO ".$item_table."(";
					$insert_query.="description,";                                        
                                        $insert_query.="menu_voice_id,";
                                        $insert_query.="is_active,";   
                                        $insert_query.="create_date";
					$insert_query.=")";
					$insert_query.=" VALUES(";
					$insert_query.="'".$_REQUEST["item_description"]."',";
                                        $insert_query.="'".$_REQUEST["item_menu_voice_id"]."',";
                                        $is_active = ($_REQUEST["item_is_active"]=="on")?"true":"false";
                                        $insert_query.="'".$is_active."',";                                        					
					$insert_query.="sysdate()";		
					$insert_query.=")";
					//echo "====".$insert_query."===*<br>";
					mysql_query($insert_query);	
				
				?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно добавлена!";
					</script>		
				<?php					
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_description"]."', ";					
                                        $is_active = ($_REQUEST["item_is_active"]=="on")?"true":"false";
                                        $query.=" is_active = ".$is_active.", ";
					//$query.=" workTime='".$_REQUEST["item_workTime"]."',";	
                                        $query.=" menu_voice_id = '".$_REQUEST["item_menu_voice_id"]."' ";
                                        
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
			?>
		   
													
			<fieldset>
				<legend>
					<?php echo $item_img." ".$title;?>
				</legend>	
				</br>
				<?php include("ivr_nav.php");?>								
				</br>
				<table class="table table-condensed table-hover">							
					<thead>
						<TR>	
                                                        <th>Активно</th>
							<th>Описание</th>
                                                        <th>Голос меню</th>
							<th><span class="glyphicon glyphicon-calendar"></span> Дата создания</th>
							<th></th>
								
						</TR>
					<thead>
					<tbody>
						<?php 
						$counter = 0;
						$query="  SELECT  ";
                                                $query.=" i.*, ";
                                                $query.=" m.description as m_description, ";
                                                $query.=" m.id as m_id, ";
                                                $query.=" case when i.is_active=true then 'true' else 'false' end as is_active ";
                                                $query.=" FROM ".$item_table." i, menu_voice m  ";
                                                $query.=" WHERE i.menu_voice_id = m.id ";
						//echo $query;
						$rs = mysql_query($query);	
						while($row= mysql_fetch_array($rs))								
						{
						$counter++;
						?>
							<tr>				
                                                            <td class="col-xs-1 text-center" style="vertical-align: middle;"
                                                                        id="is_active_<?php echo $row["id"]?>" 
                                                                        value="<?php echo $row["is_active"];?>" 
                                                                > 
                                                                    <input type="checkbox" <?php echo ($row["is_active"]=="true")?"checked":""?> disabled>
                                                             </td>                                                            
                                                            <td class="col-xs-2" 
                                                                    id="description_<?php echo $row["id"]?>" 
                                                                    value="<?php echo $row["description"]?>" 
                                                            > 
                                                                    <?php echo $row["description"]?>
                                                            </td>
                                                            <!--td class="col-xs-3" style="display: none;"
                                                                    id="workTime_<?php echo $row["id"]?>" 
                                                                    value="<?php echo $row["workTime"]?>" 
                                                            > 
                                                                    <?php echo $row["workTime"]?>
                                                            </td-->
                                                            <td class="col-xs-3" 
                                                                    id="menu_voice_id_<?php echo $row["id"]?>" 
                                                                    value="<?php echo $row["m_id"]?>" 
                                                            > 
                                                                    <?php echo $row["m_description"]?>
                                                            </td>

                                                            <td class="col-xs-2"><?php echo $row["create_date"]?></td>																											
                                                            <td class="col-xs-3 text-right">

                                                                    <button title="Отчет по звонкам" data-toggle="modal" type="button" class="btn btn-default" item_id="<?php echo $row["id"]?>"   >
                                                                            <img src="./../img/call-logs-1.png" width="14px" height="14px">
                                                                    </button>
                                                                    <a href="node.php?ivr_id=<?php echo $row["id"]?>">	
                                                                            <button title="Структура дерева IVR" data-toggle="modal" type="button" class="btn btn-default"  item_id="<?php echo $row["id"]?>"   >										 
                                                                                    <img src="./../img/list-4.png" width="14px" height="14px">	
                                                                                    <!--span class="glyphicon glyphicon-tree-deciduous"></span-->										
                                                                            </button>						
                                                                    </a>
                                                                    <button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
                                                                            <span class="glyphicon glyphicon-pencil"></span>
                                                                    </button>						
                                                                    <button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
                                                                            <span class="glyphicon glyphicon-trash"></span>
                                                                    </button>											
                                                            </td>										
							</tr>									
						<?php
						}																																	
						?>							
					</tbody>
				</table>				
				<!-- Button trigger modal -->
				<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
				  <span class="glyphicon glyphicon-plus"></span> Добавить
				</button>					
			</fieldset>
			

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">

                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" >
                                            <span id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> IVR</h4>
                                        </div>
                                        <div class="modal-body">		
                                            <input type="hidden" id="item_action" name="item_action" value="">	
                                            <input type="hidden" id="item_id" name="item_id" value="">
                                            <input type="hidden" id="item_workTime" name="item_workTime" value="">

                                            <div class="form-group">
                                                  <label class="col-sm-4 control-label">Описание</label>
                                                  <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
                                                  </div>
                                            </div>
                                            <div class="form-group">
                                                  <label class="col-sm-4 control-label">Голос меню</label>
                                                  <div class="col-sm-8">							  
                                                      <select class="form-control" id="item_menu_voice_id" name="item_menu_voice_id">
                                                          <option value="">не выбран</option>
                                                          <?php
                                                          $c_query="select * from menu_voice";									
                                                          $c_rs = mysql_query($c_query);	
                                                          while($c_row= mysql_fetch_array($c_rs))								
                                                          {?>
                                                                  <option value="<?php echo $c_row["id"]?>"><?php echo $c_row["description"]?></option>									
                                                          <?php
                                                          }
                                                          ?>
                                                      </select>
                                                  </div>
                                            </div>
                                            <div class="form-group" style="vertical-align: middle;">
                                                  <label class="col-sm-4 control-label" >                                                        
                                                          Активен                                                           
                                                  </label>

                                                  <div class="col-sm-8">
                                                      <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" id="item_is_active" name="item_is_active">
                                                        </label>
                                                      </div>
                                                  </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                            <button class="btn btn-primary"  type="submit" >Выполнить</button>
                                        </div>				  
                                    </div>
                                </div>
                            </form>    
			</div>			
			
			<script>
                                $(document).ready(function() {
                                    $('#ItemForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            item_description: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },
                                            item_menu_voice_id: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'выберите Голос меню'
                                                    }
                                                }
                                            }                                            
                                        }
                                    });
                                });
                            
                                function setWorkTime(){
                                    var item_id = document.getElementById("item_id").value;
                                    var workTimeString = document.getElementById("workTime_" + item_id).getAttribute("value");
                                    
                                    document.getElementById("item_monday").checked =(workTimeString.split(";")[0].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_monday_begin").value = workTimeString.split(";")[0].split("/")[1].split("-")[0];
                                    document.getElementById("item_monday_end").value = workTimeString.split(";")[0].split("/")[1].split("-")[1];

                                    document.getElementById("item_tuesday").checked =(workTimeString.split(";")[1].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_tuesday_begin").value = workTimeString.split(";")[1].split("/")[1].split("-")[0];
                                    document.getElementById("item_tuesday_end").value = workTimeString.split(";")[1].split("/")[1].split("-")[1];

                                    document.getElementById("item_wednesday").checked =(workTimeString.split(";")[2].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_wednesday_begin").value = workTimeString.split(";")[2].split("/")[1].split("-")[0];
                                    document.getElementById("item_wednesday_end").value = workTimeString.split(";")[2].split("/")[1].split("-")[1];

                                    document.getElementById("item_thursday").checked =(workTimeString.split(";")[3].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_thursday_begin").value = workTimeString.split(";")[3].split("/")[1].split("-")[0];
                                    document.getElementById("item_thursday_end").value = workTimeString.split(";")[3].split("/")[1].split("-")[1];

                                    document.getElementById("item_friday").checked =(workTimeString.split(";")[4].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_friday_begin").value = workTimeString.split(";")[4].split("/")[1].split("-")[0];
                                    document.getElementById("item_friday_end").value = workTimeString.split(";")[4].split("/")[1].split("-")[1];

                                    document.getElementById("item_saturday").checked =(workTimeString.split(";")[5].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_saturday_begin").value = workTimeString.split(";")[5].split("/")[1].split("-")[0];
                                    document.getElementById("item_saturday_end").value = workTimeString.split(";")[5].split("/")[1].split("-")[1];

                                    document.getElementById("item_sunday").checked =(workTimeString.split(";")[6].split("/")[0]=="1")?true:false;                                    
                                    document.getElementById("item_sunday_begin").value = workTimeString.split(";")[6].split("/")[1].split("-")[0];
                                    document.getElementById("item_sunday_end").value = workTimeString.split(";")[6].split("/")[1].split("-")[1];

                                }
                                
                                function getWorkTime(){
                                    
                                    var workTimeString = "";
                                    var monday = document.getElementById("item_monday").checked;
                                    var monday_begin = document.getElementById("item_monday_begin").value;
                                    var monday_end = document.getElementById("item_monday_end").value;
                                    var tuesday = document.getElementById("item_tuesday").checked;
                                    var tuesday_begin = document.getElementById("item_tuesday_begin").value;
                                    var tuesday_end = document.getElementById("item_tuesday_end").value;
                                    var wednesday = document.getElementById("item_wednesday").checked;
                                    var wednesday_begin = document.getElementById("item_wednesday_begin").value;
                                    var wednesday_end = document.getElementById("item_wednesday_end").value;
                                    var thursday = document.getElementById("item_thursday").checked;
                                    var thursday_begin = document.getElementById("item_thursday_begin").value;
                                    var thursday_end = document.getElementById("item_thursday_end").value;
                                    var friday = document.getElementById("item_friday").checked;
                                    var friday_begin = document.getElementById("item_friday_begin").value;
                                    var friday_end = document.getElementById("item_friday_end").value;
                                    var saturday = document.getElementById("item_saturday").checked;
                                    var saturday_begin = document.getElementById("item_saturday_begin").value;
                                    var saturday_end = document.getElementById("item_saturday_end").value;
                                    var sunday = document.getElementById("item_sunday").checked;
                                    var sunday_begin = document.getElementById("item_sunday_begin").value;
                                    var sunday_end = document.getElementById("item_sunday_end").value;
                                    
                                    if(monday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + monday_begin + "-" + monday_end + ";";

                                    if(tuesday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + tuesday_begin + "-" + tuesday_end + ";";

                                    if(wednesday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + wednesday_begin + "-" + wednesday_end + ";";

                                    if(thursday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + thursday_begin + "-" + thursday_end + ";";

                                    if(friday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + friday_begin + "-" + friday_end + ";";

                                    if(saturday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + saturday_begin + "-" +saturday_end + ";";

                                    if(sunday){
                                        workTimeString += "1";
                                    }else{
                                        workTimeString += "0";
                                    }
                                    workTimeString += "/" + sunday_begin + "-" + sunday_end + ";";
                                    
                                    document.getElementById("item_workTime").value = workTimeString;
                                }
			
				function showItemModal(obj){	
                                        $('#ItemForm').data('bootstrapValidator').resetForm(true);
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();						
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");
						setItemParameters();
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
				
				function setItemParameters(){	
					var action = document.getElementById("item_action").value;
					var item_id = document.getElementById("item_id").value;	
					
					if(action=="new"){
                                                document.getElementById("item_is_active").checked = true;
						document.getElementById("item_description").value = "";				
                                                document.getElementById("item_menu_voice_id").value = "";
					}else{		
                                                if(document.getElementById("is_active_" + item_id).getAttribute("value")=="false"){
                                                    document.getElementById("item_is_active").checked = false;
                                                }else{
                                                    document.getElementById("item_is_active").checked = true;
                                                }                                                
						document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");										
                                                document.getElementById("item_menu_voice_id").value = document.getElementById("menu_voice_id_" + item_id).getAttribute("value");										
                                                //setWorkTime();
					}    
				}
                                /*
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
					var action = document.getElementById("item_action").value;
					var error_text="";								

					if(action=="delete"){					
							obj.innerHTML='Удаление...';
							document.getElementById("item_id").value = id;	
							document.getElementById("ItemForm").submit();
					}else{

							error_text = checkForm();				

							if(error_text=="")
							{	
                                                                       // getWorkTime();
									document.getElementById("ItemForm").submit();						
									if(action=="new"){					
                                                                            obj.innerHTML='Добавление...';
									}else if(action=="edit"){					
                                                                            obj.innerHTML='Сохранение...';
                                                                            document.getElementById("item_id").value = id;
									}						
							}else{
									alert(error_text);	
							}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					
					var description = document.getElementById("item_description").value;
					var menu_voice_id = document.getElementById("item_menu_voice_id").value;
					
					if(description=="")
					{
						error_text+="Заполните поле 'Описание' !!!\n";				
					}

					if(menu_voice_id=="")
					{
						error_text+="Выберите 'Голос меню' !!!\n";				
					}
                                        
					return 	error_text;
				}
                                 */
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>
