<html lang="en">
  <?php 
  include("head.php"); 
  ?>
  <body>

  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	      <!--CONTENT BEGIN--> 
		  <?php 
		  include("content.html"); 
		  ?>
		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.html"); 
	  ?>
  </body>
</html>
