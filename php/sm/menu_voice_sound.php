<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "menu_voice";  
  $page = "menu_voice_sound";
  $item_table = "menu_voice_sound";
  $menu_voice_id = $_REQUEST["menu_voice_id"];    
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
				
					$insert_query ="INSERT INTO ".$item_table."(";
                                        $insert_query.="menu_voice_id,";
					$insert_query.="description_kz,";
                                        $insert_query.="description_ru,";
                                        $insert_query.="file_name,";
					$insert_query.="create_date";
					$insert_query.=")";
					$insert_query.=" VALUES(";
                                        $insert_query.="'".$menu_voice_id."',";
					$insert_query.="'".$_REQUEST["item_description_kz"]."',";
                                        $insert_query.="'".$_REQUEST["item_description_ru"]."',";
                                        $insert_query.="'".$_REQUEST["item_file_name"]."',";
					$insert_query.="sysdate()";		
					$insert_query.=")";
					//echo "====".$insert_query."===*<br>";
					mysql_query($insert_query);	
                                        
                                        $dir =  "/var/www/html/sm/sounds/menu_voice/".$menu_voice_id;
                                        
					upload_file($dir."/kz","item_file_kz",$_REQUEST["item_file_name"]);
					upload_file($dir."/ru","item_file_ru",$_REQUEST["item_file_name"]);					
                                        
				
				?>
					<script>			
						location.href = "<?php echo $page;?>.php?menu_voice_id=<?php echo $menu_voice_id;?>&message=Запись успешно добавлена!";
					</script>		
				<?php					
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";                                        
					$query.=" description_kz = '".$_REQUEST["item_description_kz"]."',";					
                                        $query.=" description_ru = '".$_REQUEST["item_description_ru"]."'";	                                        
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?menu_voice_id=<?php echo $menu_voice_id;?>&message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?menu_voice_id=<?php echo $menu_voice_id;?>&message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
			?>
		   
			<div>										
				<fieldset>
					<legend>
                                            <span class="label label-info"><?php echo $menu_voice_description;?></span> / <span class="label label-default">Звуковые файлы голоса</span>
                                            </br>
                                            </br>
					</legend>	
													
					<table class="table table-condensed table-hover">							
						<thead>
							<TR>	
                                                            <th>Имя файла</th>
							    <th><?php echo $kz_img; ?></th>
                                                            <th><?php echo $ru_img; ?></th>
							    <th></th>									
							</TR>
						<thead>
						<tbody>
							<?php 
							$counter = 0;
							$query="select * from ".$item_table." where menu_voice_id = ".$menu_voice_id." order by file_name";
							//echo $query;
							$rs = mysql_query($query);	
							while($row= mysql_fetch_array($rs))								
							{
							$counter++;
							?>
								<tr>									
									<td class="col-xs-3" 
										id="file_name_<?php echo $row["id"]?>" 
										value="<?php echo $row["file_name"]?>" 
									> 
										<?php echo $row["file_name"]?>
									</td>
									<td class="col-xs-4" 
										id="description_kz_<?php echo $row["id"]?>" 
										value="<?php echo $row["description_kz"]?>" 
									> 
                                                                                <button type="button" class="btn btn-default"  onClick="playMainSound('sounds/menu_voice/<?php echo $menu_voice_id ?>/kz/<?php echo $row["file_name"]; ?>.wav')">
                                                                                    <img src="img/playsound-1.png" width="15px" height="15px" />
                                                                                </button>
										<?php echo $row["description_kz"]?>
									</td>
									<td class="col-xs-4" 
										id="description_ru_<?php echo $row["id"]?>" 
										value="<?php echo $row["description_ru"]?>" 
									> 
                                                                                <button type="button" class="btn btn-default"  onClick="playMainSound('sounds/menu_voice/<?php echo $menu_voice_id ?>/ru/<?php echo $row["file_name"]; ?>.wav')">
                                                                                    <img src="img/playsound-1.png" width="15px" height="15px" />
                                                                                </button>                                                                            
										<?php echo $row["description_ru"]?>
									</td>
									<td class="col-xs-2 text-right">
                                                                            <button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
                                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                            </button>	

                                                                            <button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
                                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                            </button>											
									</td>										
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
					<!-- Button trigger modal -->
					<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
					  <span class="glyphicon glyphicon-plus"></span> Добавить
					</button>					
				</fieldset>
			</div>	

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
			  <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" ><span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> звукового файла</h4>
					</div>
					<div class="modal-body">		
						<form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">

						  <input type="hidden" id="item_action" name="item_action" value="">	
						  <input type="hidden" id="item_id" name="item_id" value="">
                                                  <input type="hidden" id="menu_voice_id" name="menu_voice_id" value="<?php echo $menu_voice_id?>"> 
						  
						  <div class="form-group" id="file_name_div">
							<label class="col-sm-4 control-label">Тип файла</label>
							<div class="col-sm-8">
							  <select class="form-control" id="item_file_name" name="item_file_name" >
                                                              <option value="">не выбран</option>                                                              
                                                                <?php
                                                                $menu_file_query="select * from menu_file where file_name not in(select file_name from menu_voice_sound where menu_voice_id = ".$menu_voice_id.")";									
                                                                $menu_file_rs = mysql_query($menu_file_query);	
                                                                while($menu_file_row= mysql_fetch_array($menu_file_rs))								
                                                                {?>
                                                                        <option value="<?php echo $menu_file_row["file_name"]?>"><?php echo $menu_file_row["description"]?></option>									
                                                                <?php
                                                                }
                                                                ?>                                                              
                                                          </select>
							</div>
						  </div>
                                                  
						  <div class="form-group">
							<label class="col-md-offset-4 col-sm-8 control-label">
                                                            <ul class="nav nav-tabs">
                                                                <li onclick="set_lang_nav('kz')" id="kz_nav"><a href="#"><?php echo $kz_img;?></a></li>
                                                                <li onclick="set_lang_nav('ru')" id="ru_nav"><a href="#"><?php echo $ru_img;?></a></li>                                                  
                                                            </ul>                                                  
                                                        </label>
							<div class="col-sm-8">
							</div>
						  </div>
                                                  
                                                  <span id="kz_span" style="display: none">
                                                        <div class="form-group" id="kz_play_div">
                                                              <label class="col-sm-4 control-label">Прослушать</label>
                                                              <div class="col-sm-8">
                                                                    <audio id="playsound_kz" controls="controls">
                                                                          <source id="playsound_kz_source" src="" type="audio/ogg" />
                                                                    </audio>						  					  
                                                              </div>
                                                        </div>

                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label">Новый файл</label>
                                                              <div class="col-sm-8">
                                                                <input type="file" class="form-control" id="item_file_kz" name="item_file_kz" placeholder="Введите текст">
                                                              </div>
                                                        </div>

                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label">Текст</label>
                                                              <div class="col-sm-8">
                                                                <textarea rows="5" class="form-control" id="item_description_kz" name="item_description_kz" placeholder="Введите текст файла на казахском языке">
                                                                </textarea>  
                                                              </div>
                                                        </div>
                                                  </span>    
                                                  <span id="ru_span" style="display: none">
                                                        <div class="form-group" id="ru_play_div">
                                                              <label class="col-sm-4 control-label">Прослушать </label>
                                                              <div class="col-sm-8">
                                                                    <audio id="playsound_ru" controls="controls">
                                                                          <source id="playsound_ru_source" src="" type="audio/ogg" />
                                                                    </audio>						  					  
                                                              </div>
                                                        </div>
                                                      
                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label">Новый файл</label>
                                                              <div class="col-sm-8">
                                                                <input type="file" class="form-control" id="item_file_ru" name="item_file_ru" placeholder="Введите текст">
                                                              </div>
                                                        </div>

                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label">Текст</label>
                                                              <div class="col-sm-8">
                                                                <textarea rows="5" class="form-control" id="item_description_ru" name="item_description_ru" placeholder="Введите текст файла на русском языке">
                                                                </textarea>
                                                              </div>
                                                        </div>
                                                  </span>    
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
						<button class="btn btn-primary"  type="button"  onclick="ItemSubmitForm(this)" >Выполнить</button>
					</div>				  
				</div>
			  </div>
			</div>			
			
			<script>
                                function set_lang_nav(lang){
                                    if(lang=="kz"){
                                          document.getElementById("kz_nav").className = "active";                                  
                                          document.getElementById("kz_span").style.display="";
                                          document.getElementById("ru_nav").className = "";                                  
                                          document.getElementById("ru_span").style.display="none";                                          
                                    }else if(lang=="ru"){
                                          document.getElementById("kz_nav").className = "";                                  
                                          document.getElementById("kz_span").style.display="none";
                                          document.getElementById("ru_nav").className = "active";                                  
                                          document.getElementById("ru_span").style.display="";                                        
                                    }else{
                                          document.getElementById("kz_nav").className = "";                                  
                                          document.getElementById("kz_span").style.display="none";
                                          document.getElementById("ru_nav").className = "";                                  
                                          document.getElementById("ru_span").style.display="none";                                        
                                    }
                                    
                                    
                                }
				function showItemModal(obj){
					
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
					
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();					
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
			
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
				    var action = document.getElementById("item_action").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;	
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
							document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					var action = document.getElementById("item_action").value;
                                        
                                        var file_name = document.getElementById("item_file_name").value;
					if(action=="new" && file_name=="")
					{
						error_text+="Заполните поле 'Имя файла' !!!\n";				
					}
                                        
					var description_kz = document.getElementById("item_description_kz").value;
					if(description_kz=="")
					{
						error_text+="Заполните поле 'Текст на казахском' !!!\n";				
					}		

                                        var description_ru = document.getElementById("item_description_ru").value;
					if(description_ru=="")
					{
						error_text+="Заполните поле 'Текст на русском' !!!\n";				
					}		
                                        
                                        if(action=="new"){
                                            var file_kz = document.getElementById("item_file_kz").value;
                                            var file_ru= document.getElementById("item_file_ru").value;
                                            if(file_kz=="")
                                            {
                                                    error_text+="Выберите файл на казахском языке !!!\n";				
                                            }	                                            
                                            if(file_ru=="")
                                            {
                                                    error_text+="Выберите файл на русском языке !!!\n";				
                                            }	                                            
                                            
                                        }
					
                                        
					return 	error_text;
				}
				
				function setItemParameters(){	
					
					var action = document.getElementById("item_action").value;
					var item_id = document.getElementById("item_id").value;	
                                        set_lang_nav("");
					if(action=="new"){
						document.getElementById("item_description_kz").value = "";				
                                                document.getElementById("item_description_ru").value = "";				
                                                document.getElementById("file_name_div").style.display = "";
                                                document.getElementById("kz_play_div").style.display="none";
                                                document.getElementById("ru_play_div").style.display="none";
					}else{
                                            
						document.getElementById("item_description_kz").value = document.getElementById("description_kz_" + item_id).getAttribute("value");				
                                                document.getElementById("item_description_ru").value = document.getElementById("description_ru_" + item_id).getAttribute("value");				
                                                document.getElementById("file_name_div").style.display = "none";			
                                                document.getElementById("kz_play_div").style.display="";
                                                document.getElementById("ru_play_div").style.display="";
                                                
                                                updateSource("#playsound_ru_source","sounds/menu_voice/" + document.getElementById("menu_voice_id").value + "/ru/" + document.getElementById("file_name_" + item_id).getAttribute("value")+".wav");
                                                updateSource("#playsound_kz_source","sounds/menu_voice/" + document.getElementById("menu_voice_id").value + "/kz/" + document.getElementById("file_name_" + item_id).getAttribute("value")+".wav");
					}	
				}
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>

