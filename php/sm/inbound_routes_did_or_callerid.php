﻿<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "inbound_routes";
  $title = "Маршруты";
  $page = "inbound_routes";
  $item_table = "inbound_routes";  
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
				
                                        $is_exists = 0;
					$query="select count(0) as counter from ".$item_table." where in_did=".$_REQUEST["item_in_did"]; 
					$rs = mysql_query($query);			
					while($row= mysql_fetch_array($rs))
					{
						$is_exists=$row["counter"];
					}
					
					if($is_exists == 0){
                                            $insert_query ="INSERT INTO ".$item_table."(";
                                            $insert_query.="description,";
                                            $insert_query.="custumer_id,";
                                            $insert_query.="in_route_type,";
                                            $insert_query.="in_trunk,";                                            
                                            $insert_query.="in_did,";
                                            $insert_query.="in_callerid,";
                                            $insert_query.="out_route_type,";
                                            $insert_query.="out_trunk,";
                                            $insert_query.="out_did,";
                                            $insert_query.="out_callerid,";
                                            $insert_query.="ivr_id,";
                                            $insert_query.="createDate";
                                            $insert_query.=")";
                                            $insert_query.=" VALUES(";
                                            $insert_query.="'".$_REQUEST["item_description"]."',";
                                            $insert_query.="'".$_REQUEST["item_custumer_id"]."',";
                                            $insert_query.="'".$_REQUEST["item_in_route_type"]."',";
                                            $insert_query.="'".$_REQUEST["item_in_trunk"]."',";
                                            $insert_query.="'".$_REQUEST["item_in_did"]."',";
                                            $insert_query.="'".$_REQUEST["item_in_callerid"]."',";
                                            $insert_query.="'".$_REQUEST["item_out_route_type"]."',";
                                            $insert_query.="'".$_REQUEST["item_out_trunk"]."',";
                                            $insert_query.="'".$_REQUEST["item_out_did"]."',";
                                            $insert_query.="'".$_REQUEST["item_out_callerid"]."',";
                                            $insert_query.="'".$_REQUEST["item_ivr_id"]."',";
                                            $insert_query.="sysdate()";		
                                            $insert_query.=")";
                                            //echo "====".$insert_query."===*<br>";
                                            mysql_query($insert_query);	

                                            ?>
                                                    <script>			
                                                        location.href = "<?php echo $page;?>.php?message=Запись успешно добавлена!";
                                                    </script>		
                                            <?php	
                                        }else{
                                            
                                        ?>
                                                <script>			
                                                        location.href = "<?php echo $page;?>.php?message=Запись с таким DID уже существует!";
                                                </script>		
                                        <?php	
                                            
                                        }
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_description"]."', ";					
					$query.=" custumer_id = '".$_REQUEST["item_custumer_id"]."', ";					
                                        $query.=" in_route_type = '".$_REQUEST["item_in_route_type"]."',";
					$query.=" in_trunk = '".$_REQUEST["item_in_trunk"]."',";
					$query.=" in_did = '".$_REQUEST["item_in_did"]."',";					
					$query.=" in_callerid = '".$_REQUEST["item_in_callerid"]."',";
                                        $query.=" out_route_type = '".$_REQUEST["item_out_route_type"]."',";
					$query.=" out_trunk = '".$_REQUEST["item_out_trunk"]."',";
					$query.=" out_did = '".$_REQUEST["item_out_did"]."',";					
					$query.=" out_callerid = '".$_REQUEST["item_out_callerid"]."',";
					$query.=" ivr_id = '".$_REQUEST["item_ivr_id"]."'";
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
			?>
		   
			<div>										
				<fieldset>
					<legend>
					<?php echo $title;?>
					</legend>	
					<?php include("ivr_title.php");?>								
					<div class="alert alert-danger">
						<b>DID</b> - номер на который происходит звонок, <b>CALLERID</b> - номер c которого происходит звонок. 
					</div>
					<table class="table table-condensed table-hover" border="0">							
						<thead>
							<TR>													
								<th colspan="2"></th>
								<th colspan="3" class="text-center" style="border-left: solid 1px grey;border-right: solid 1px grey;">Прием звонка</th>								
								<th colspan="4" class="text-center" style="border-left: solid 1px grey;border-right: solid 1px grey;">Обработка звонка</th>								
								<th></th>																
							</TR>					
							<TR>													
								<th class="text-center">Описание</th>
								<th class="text-center">Клиент</th>
								<th class="text-center" style="border-left: solid 1px grey;">Канал</th>
								<th class="text-center">DID</th>																							
								<th class="text-center" style="border-right: solid 1px grey;">CALLERID</th>								
								<th class="text-center">Канал</th>
								<th class="text-center">DID</th>
								<th class="text-center">CALLERID</th>
								<th class="text-center" style="border-right: solid 1px grey;">Меню IVR</th>								
								<th></th>
																
							</TR>
						<thead>
						<tbody>
							<?php 
							$counter = 0;
							$query="select i.*,c.description as c_description,ivr.description as ivr_description from ".$item_table." i  LEFT JOIN ivr ON i.ivr_id = ivr.id,custumer c where i.custumer_id = c.id";
							//echo $query;
							$rs = mysql_query($query);	
							while($row= mysql_fetch_array($rs))								
							{
							$counter++;
							?>
								<tr>									
									<td class="col-xs-2 text-center" 
										id="description_<?php echo $row["id"]?>" 
										value="<?php echo $row["description"]?>" 
									> 
										<?php echo $row["description"]?>
									</td>
									
									<td class="col-xs-1 text-center" 
										id="custumer_id_<?php echo $row["id"]?>" 
										value="<?php echo $row["custumer_id"]?>" 
									> 
										<?php echo $row["c_description"]?>
									</td> 

                                                                        <td class="col-xs-1 text-center"  style="display: none;"
										id="in_route_type_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_route_type"]?>" 
									> 
										<?php echo $row["in_route_type"]?>
									</td>	
                                                                        
									<td class="col-xs-1 text-center" style="border-left: solid 1px grey;"
										id="in_trunk_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_trunk"]?>" 
									> 
                                                                                <?php echo ($row["in_trunk"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["in_trunk"];?>	
                                                                            
									</td>									
									
									<td class="col-xs-1 text-center" 
										id="in_did_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_did"]?>" 
									> 
										<?php echo ($row["in_did"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["in_did"];?>
									</td>

									<td class="col-xs-1 text-center" style="border-right: solid 1px grey;"
										id="in_callerid_<?php echo $row["id"]?>" 
										value="<?php echo $row["in_callerid"]?>" 
									> 
										<?php echo ($row["in_callerid"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["in_callerid"];?>
									</td>	
                                                                        
                                    <td class="col-xs-1 text-center"  style="display: none;"
										id="out_route_type_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_route_type"]?>" 
									> 
					                    <?php echo ($row["out_route_type"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["out_route_type"];?>	                                                                                
									</td>	
                                                                        
									<td class="col-xs-1 text-center" style="border-left: solid 1px grey;"
										id="out_trunk_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_trunk"]?>" 
									> 
                                        <?php echo ($row["out_trunk"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["out_trunk"];?>	                                                                                
									</td>									
									
									<td class="col-xs-1 text-center" 
										id="out_did_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_did"]?>" 
									> 
                                        <?php echo ($row["out_did"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["out_did"];?>	                                                                                
									</td>

									<td class="col-xs-1 text-center" 
										id="out_callerid_<?php echo $row["id"]?>" 
										value="<?php echo $row["out_callerid"]?>" 
									> 
										<?php echo ($row["out_callerid"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["out_callerid"];?>
									</td>	
	
									<td class="col-xs-1 text-center" style="border-right: solid 1px grey;"
										id="ivr_id_<?php echo $row["id"]?>" 
										value="<?php echo $row["ivr_id"]?>" 
									> 
                                        <?php echo ($row["ivr_description"]=="")?"<span class='glyphicon glyphicon-remove'></span>":$row["ivr_description"];?>	                                                                            
									</td>										
									
									<td class="col-xs-2 text-right">
										<button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
											<span class="glyphicon glyphicon-pencil"></span>
										</button>	

										<button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
											<span class="glyphicon glyphicon-trash"></span>
										</button>											
									</td>										
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
					<!-- Button trigger modal -->
					<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
					  <span class="glyphicon glyphicon-plus"></span> Добавить
					</button>					
				</fieldset>
			</div>	

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" >
						<span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> маршрута</h4>
					</div>
					<div class="modal-body">		
						<form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">

						  <input type="hidden" id="item_action" name="item_action" value="">	
						  <input type="hidden" id="item_id" name="item_id" value="">
                                                  <input type="hidden" id="item_in_route_type" name="item_in_route_type" value="">
                                                  <input type="hidden" id="item_out_route_type" name="item_out_route_type" value="">
						  
						  <div class="form-group">
							<label class="col-sm-4 control-label">Описание</label>
							<div class="col-sm-8">
							  <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
							</div>
						  </div>

						  <div class="form-group" style="vertical-align: middle;">
							<label class="col-sm-4 control-label" >                                                        
                                                            <span data-toggle="popover" data-placement="left" data-content="" title="подсказка" data-original-title="каз">
                                                                <span class="glyphicon glyphicon-question-sign">&nbsp;</span>
                                                                Клиент
                                                           </span>                                                        
                                                        </label>
                                                       
							<div class="col-sm-8">
							  <select class="form-control" id="item_custumer_id" name="item_custumer_id">
									<option value="">не выбран</option>
									<?php
									$c_query="select * from custumer";									
									$c_rs = mysql_query($c_query);	
									while($c_row= mysql_fetch_array($c_rs))								
									{?>
										<option value="<?php echo $c_row["id"]?>"><?php echo $c_row["description"]?></option>									
									<?php
									}
									?>
							  </select>
							</div>
						  </div>
                                                  <div class="panel panel-default">
                                                      <div class="panel-body">
                                                          <table width="100%" border="0">
                                                              <tr>
                                                                  <td>Прием звонка</td>
                                                                  <td align="right">
                                                                    <div class="btn-group">
                                                                      <button type="button" id="did_button" class="btn btn-default" onclick="set_in_route_type('did')"><b>по DID</b></button>                                                              
                                                                      <button type="button" id="callerid_button" class="btn btn-default" onclick="set_in_route_type('callerid')"><b>по CALLERID</b></button>
                                                                    </div>                                                                                                                                              
                                                                  </td>
                                                              </tr>                                                              
                                                          </table>
                                                      </div>
                                                      <div class="panel-footer" style="display: none;" id="in_route_footer">
                                                            <div class="form-group" id="in_trunk_div">
                                                                  <label class="col-sm-4 control-label" >Канал</label>
                                                                  <div class="col-sm-8">							  
                                                                    <select id="item_in_trunk" name="item_in_trunk" class="form-control">
                                                                          <option value="" >не выбран</option>
                                                                          <?php
                                                                          while($trunk_row= mysql_fetch_array($trunk_rs))
                                                                          {
                                                                          ?>
                                                                                  <option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>

                                                                          <?php
                                                                          }								
                                                                          ?>
                                                                    </select>									  
                                                                  </div>
                                                            </div>

                                                            <div class="form-group" id="in_did_div">
                                                                  <label class="col-sm-4 control-label">DID</label>
                                                                  <div class="col-sm-8">
                                                                    <input type="text" class="form-control" id="item_in_did" name="item_in_did" placeholder="Введите номер">
                                                                  </div>
                                                            </div>	

                                                            <div class="form-group" id="in_callerid_div">
                                                                  <label class="col-sm-4 control-label">CALLERID</label>
                                                                  <div class="col-sm-8">
                                                                    <input type="text" class="form-control" id="item_in_callerid" name="item_in_callerid" placeholder="Введите номер">
                                                                  </div>
                                                            </div>                                                          
                                                      </div>
                                                  </div>                                                  
						  
                                                  <div class="panel panel-default">
                                                      <div class="panel-body">                                                          
                                                          <table width="100%" border="0">
                                                              <tr>
                                                                  <td>Обработка звонка</td>
                                                                  <td align="right">
                                                                    <div class="btn-group">
                                                                      <button type="button" id ="redirect_button" class="btn btn-default" onclick="set_out_route_type('redirect')"><b>Переадресация</b></button>                                                              
                                                                      <button type="button" id ="ivr_button" class="btn btn-default" onclick="set_out_route_type('ivr')"><b>Меню IVR</b></button>
                                                                    </div>                                                                                                                                              
                                                                  </td>
                                                              </tr>                                                              
                                                          </table>
                                                      </div>
                                                      <div class="panel-footer" style="display: none;" id="out_route_footer">						  
                                                        <div class="form-group"  id="out_trunk_div">
                                                              <label class="col-sm-4 control-label">Канал</label>
                                                              <div class="col-sm-8">							  
                                                                <select id="item_out_trunk" name="item_out_trunk" class="form-control">
                                                                      <option value="" >не выбран</option>
                                                                      <?php
                                                                      mysql_data_seek($trunk_rs, 0);
                                                                      while($trunk_row= mysql_fetch_array($trunk_rs))
                                                                      {
                                                                      ?>
                                                                              <option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>

                                                                      <?php
                                                                      }								
                                                                      ?>
                                                                </select>									  
                                                              </div>
                                                        </div>

                                                        <div class="form-group" id="out_did_div">
                                                              <label class="col-sm-4 control-label">DID</label>
                                                              <div class="col-sm-8">
                                                                <input type="text" class="form-control" id="item_out_did" name="item_out_did" placeholder="Введите номер">
                                                              </div>
                                                        </div>	

                                                        <div class="form-group" id="out_callerid_div">
                                                              <label class="col-sm-4 control-label">CALLERID</label>
                                                              <div class="col-sm-8">
                                                                <input type="text" class="form-control" id="item_out_callerid" name="item_in_callerid" placeholder="Введите номер">
                                                              </div>
                                                        </div>

                                                        <div class="form-group" id="out_ivr_div">
                                                              <label class="col-sm-4 control-label">Меню IVR</label>
                                                              <div class="col-sm-8">
                                                                <select class="form-control" id="item_ivr_id" name="item_ivr_id">
                                                                              <option value="">не выбран</option>
                                                                              <?php
                                                                              $ivr_query="select * from ivr";									
                                                                              $ivr_rs = mysql_query($ivr_query);	
                                                                              while($ivr_row= mysql_fetch_array($ivr_rs))								
                                                                              {?>
                                                                                      <option value="<?php echo $ivr_row["id"]?>"><?php echo $ivr_row["description"]?></option>									
                                                                              <?php
                                                                              }
                                                                              ?>
                                                                </select>														  
                                                              </div>
                                                        </div>
                                                      </div>
                                                    </div> 
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
						<button class="btn btn-primary"  type="button"  onclick="ItemSubmitForm(this)" >Выполнить</button>
					</div>				  
				</div>
			  </div>
			</div>			
			
			<script>
                                function set_in_route_type(type){
                                    document.getElementById("item_in_route_type").value = type;
                                    if(type=="did"){
                                        document.getElementById("did_button").className = "btn btn-warning";
                                        document.getElementById("callerid_button").className = "btn btn-default";
                                        document.getElementById("in_route_footer").style.display ="";
                                        document.getElementById("in_did_div").style.display ="";                                        
                                        document.getElementById("in_callerid_div").style.display ="none";                                         
                                        document.getElementById("item_in_callerid").value = ""; 
                                        
                                    }else if(type=="callerid"){
                                        document.getElementById("did_button").className = "btn btn-default";
                                        document.getElementById("callerid_button").className = "btn btn-warning";                                        
                                        document.getElementById("in_route_footer").style.display ="";
                                        document.getElementById("in_did_div").style.display ="none";                                                                                
                                        document.getElementById("in_callerid_div").style.display ="";                                       
                                        document.getElementById("item_in_did").value = "";
                                    }else{
                                        document.getElementById("did_button").className = "btn btn-default";
                                        document.getElementById("callerid_button").className = "btn btn-default";                                        
                                        document.getElementById("in_route_footer").style.display ="none";
                                        document.getElementById("in_did_div").style.display ="none";                                                                                
                                        document.getElementById("in_callerid_div").style.display ="none";                                       
					document.getElementById("item_in_callerid").value = ""; 
                                        document.getElementById("item_in_did").value = "";		
										
									}
                                }
                                
                                function set_out_route_type(type){
                                    document.getElementById("item_out_route_type").value = type;
                                    if(type=="redirect"){
                                        document.getElementById("redirect_button").className = "btn btn-warning";
                                        document.getElementById("ivr_button").className = "btn btn-default";                                                                              
                                        document.getElementById("out_route_footer").style.display ="";
                                        document.getElementById("out_trunk_div").style.display ="";
                                        document.getElementById("out_did_div").style.display ="";
                                        document.getElementById("out_callerid_div").style.display ="";
                                        document.getElementById("out_ivr_div").style.display ="none";  
                                        document.getElementById("item_ivr_id").value = "";
                                    }else if(type=="ivr"){
                                        document.getElementById("redirect_button").className = "btn btn-default";
                                        document.getElementById("ivr_button").className = "btn btn-warning";                                                                              
                                        document.getElementById("out_route_footer").style.display =""; 
                                        document.getElementById("out_trunk_div").style.display ="none";
                                        document.getElementById("out_did_div").style.display ="none";                                        
                                        document.getElementById("out_callerid_div").style.display ="none";                                                       
                                        document.getElementById("out_ivr_div").style.display ="";                                        
                                        document.getElementById("item_out_callerid").value = "";
                                        document.getElementById("item_out_did").value = "";
                                        document.getElementById("item_out_trunk").value = "";
                                    }else{
                                        document.getElementById("redirect_button").className = "btn btn-default";
                                        document.getElementById("ivr_button").className = "btn btn-default";                                                                              
                                        document.getElementById("out_route_footer").style.display ="none"; 
                                        document.getElementById("out_trunk_div").style.display ="none";
                                        document.getElementById("out_did_div").style.display ="none";                                        
                                        document.getElementById("out_callerid_div").style.display ="none";                                                       
                                        document.getElementById("out_ivr_div").style.display ="none";                                        
                                        document.getElementById("item_out_callerid").value = "";
                                        document.getElementById("item_out_did").value = "";
                                        document.getElementById("item_out_trunk").value = "";
									
									}
                                }                                
				function showItemModal(obj){
					
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
                    
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");	
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}	
										
					$('#ItemModal').modal('show');					
				}
			
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
                                        var action = document.getElementById("item_action").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;	
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
							document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					
					var description = document.getElementById("item_description").value;
					var custumer_id = document.getElementById("item_custumer_id").value;
					var in_trunk = document.getElementById("item_in_trunk").value;
					var in_did = document.getElementById("item_in_did").value;
					var in_callerid = document.getElementById("item_in_callerid").value;
					var out_trunk = document.getElementById("item_out_trunk").value;
					var out_did = document.getElementById("item_out_did").value;
					var out_callerid = document.getElementById("item_out_callerid").value;					
					var ivr_id = document.getElementById("item_ivr_id").value;					
					
					if(description=="")
					{
						error_text+="Заполните поле 'Описание' !!!\n";				
					}

					if(custumer_id=="")
					{
						error_text+="Выберите Клиента !!!\n";				
					}

                                        var in_route_type = document.getElementById("item_in_route_type").value;
                                        var out_route_type = document.getElementById("item_out_route_type").value;
                                        
                                        if(in_route_type==""){
                                            error_text+="Выберите способ Приема звонка !!!\n";
                                        }else{
                                            //if(in_trunk==""){
                                            //    error_text+="Выберите 'Канал' Приема звонка !!!\n";
                                            //}
                                            if(in_route_type=="did"){
                                                if(in_did==""){
                                                    error_text+="Выберите 'DID' Приема звонка !!!\n";
                                                }                                                
                                            }else{
                                                if(in_callerid==""){
                                                    error_text+="Выберите 'CALLERID' Приема звонка !!!\n";
                                                }                                                
                                            }                                          
                                        }

                                        if(out_route_type==""){
                                            error_text+="Выберите способ Обработки звонка !!!\n";
                                        }else{
                                            if(out_route_type=="redirect"){
                                                if(out_trunk==""){
                                                    error_text+="Выберите 'Канал' Обработки звонка !!!\n";
                                                }											
                                                if(out_did==""){
                                                    error_text+="Выберите 'DID' Обработки звонка !!!\n";
                                                }                                                
                                                //if(out_callerid==""){
                                                //    error_text+="Выберите 'CALLERID' Обработки звонка !!!\n";
                                                //}                                                

                                            }else{
                                                if(ivr_id==""){
                                                    error_text+="Выберите 'Меню IVR ' Обработки звонка !!!\n";
                                                }
                                            }                                          
                                        }

					return 	error_text;
				}
				
				function setItemParameters(){		
                                        var action = document.getElementById("item_action").value;                                        
					var item_id = document.getElementById("item_id").value;	
                                        if(action=="new"){
                                            document.getElementById("item_description").value = "";				
                                            document.getElementById("item_custumer_id").value = "";
                                            document.getElementById("item_in_route_type").value = "";
                                            document.getElementById("item_in_trunk").value = "";
                                            document.getElementById("item_in_did").value = "";
                                            document.getElementById("item_in_callerid").value = "";
                                            document.getElementById("item_out_route_type").value = "";
                                            document.getElementById("item_out_trunk").value = "";
                                            document.getElementById("item_out_did").value = "";
                                            document.getElementById("item_out_callerid").value = "";
                                            document.getElementById("item_ivr_id").value = "";
                                            set_in_route_type("");
                                            set_out_route_type("");  											
                                        }else{
                                            document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");				
                                            document.getElementById("item_custumer_id").value = document.getElementById("custumer_id_" + item_id).getAttribute("value");			
                                            document.getElementById("item_in_route_type").value = document.getElementById("in_route_type_" + item_id).getAttribute("value");									
                                            document.getElementById("item_in_trunk").value = document.getElementById("in_trunk_" + item_id).getAttribute("value");									
                                            document.getElementById("item_in_did").value = document.getElementById("in_did_" + item_id).getAttribute("value");				
                                            document.getElementById("item_in_callerid").value = document.getElementById("in_callerid_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_route_type").value = document.getElementById("out_route_type_" + item_id).getAttribute("value");														
                                            document.getElementById("item_out_trunk").value = document.getElementById("out_trunk_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_did").value = document.getElementById("out_did_" + item_id).getAttribute("value");				
                                            document.getElementById("item_out_callerid").value = document.getElementById("out_callerid_" + item_id).getAttribute("value");				
                                            document.getElementById("item_ivr_id").value = document.getElementById("ivr_id_" + item_id).getAttribute("value");				
                                            set_in_route_type(document.getElementById("item_in_route_type").value);
                                            set_out_route_type(document.getElementById("item_out_route_type").value);                                            
                                        }    
				}
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.html"); 
	  ?>
  </body>
</html>
