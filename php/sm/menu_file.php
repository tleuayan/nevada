<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "menu_file";
  $title = "Список файлов";
  $page = "menu_file";
  $item_table = "menu_file";  
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
				
					$insert_query ="INSERT INTO ".$item_table."(";
					$insert_query.="description,";
					$insert_query.="file_name";
					$insert_query.=")";
					$insert_query.=" VALUES(";
					$insert_query.="'".$_REQUEST["item_description"]."',";
					$insert_query.="'".$_REQUEST["item_file_name"]."'";		
					$insert_query.=")";
					//echo "====".$insert_query."===*<br>";
					mysql_query($insert_query);	
				
				?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно добавлена!";
					</script>		
				<?php					
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_description"]."',";					
                                        $query.=" file_name = '".$_REQUEST["item_file_name"]."'";
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
			?>
		   
			<div>										
				<fieldset>
					<legend>
					<?php echo $title;?>
					</legend>	
					<table class="table table-condensed table-hover">							
						<thead>
							<TR>							
								<th>Описание</th>
								<th>Имя файла</th>
								<th></th>									
							</TR>
						<thead>
						<tbody>
							<?php 
							$counter = 0;
                                                            $query="select * from ".$item_table." order by file_name";
							//echo $query;
							$rs = mysql_query($query);	
							while($row= mysql_fetch_array($rs))								
							{
							$counter++;
							?>
								<tr>									
									<td class="col-xs-5" 
										id="description_<?php echo $row["id"]?>" 
										value="<?php echo $row["description"]?>" 
									> 
										<?php echo $row["description"]?>
									</td>
									<td class="col-xs-5" 
										id="file_name_<?php echo $row["id"]?>" 
										value="<?php echo $row["file_name"]?>" 
									> 
										<?php echo $row["file_name"]?>
									</td>
																																			
									<td class="col-xs-2 text-right">

										<button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
											<span class="glyphicon glyphicon-pencil"></span>
										</button>	

										<button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
											<span class="glyphicon glyphicon-trash"></span>
										</button>											
									</td>										
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
					<!-- Button trigger modal -->
					<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
					  <span class="glyphicon glyphicon-plus"></span> Добавить
					</button>					
				</fieldset>
			</div>	

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" ><span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> Файла</h4>
                                        </div>
                                        <div class="modal-body">		
                                            <input type="hidden" id="item_action" name="item_action" value="">	
                                            <input type="hidden" id="item_id" name="item_id" value="">

                                            <div class="form-group">
                                                  <label class="col-sm-4 control-label">Описание</label>
                                                  <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
                                                  </div>
                                            </div>
                                            <div class="form-group">
                                                  <label class="col-sm-4 control-label">Имя файла</label>
                                                  <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="item_file_name" name="item_file_name" placeholder="Введите текст">
                                                  </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                                <button class="btn btn-primary"  type="submit" >Выполнить</button>
                                        </div>				  
                                    </div>
                                </div>
                            </form>    
			</div>			
			
			<script>
			
                                $(document).ready(function() {
                                    $('#ItemForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            item_description: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            },
                                            item_file_name: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            }                                            
                                        }
                                    });
                                });
                        
				function showItemModal(obj){
					$('#ItemForm').data('bootstrapValidator').resetForm(true);
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
					
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();					
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
				
				function setItemParameters(){	
					
					var action = document.getElementById("item_action").value;
					var item_id = document.getElementById("item_id").value;		
					if(action=="new"){
						document.getElementById("item_description").value = "";				
                                                document.getElementById("item_file_name").value = "";	
					}else{
						document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");				
                                                document.getElementById("item_file_name").value = document.getElementById("file_name_" + item_id).getAttribute("value");				
					}	
				}
                                /*
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
				    var action = document.getElementById("item_action").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;	
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
							document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					
					var description = document.getElementById("item_description").value;
                                        var file_name = document.getElementById("item_file_name").value;
					
					if(description=="")
					{
						error_text+="Заполните поле 'Описание' !!!\n";				
					}		
                                        
					if(file_name=="")
					{
						error_text+="Заполните поле 'Имя файла' !!!\n";				
					}		
					
					return 	error_text;
				}
                                 */
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>
