﻿<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "trunks";
  $title = "Список каналов";
  $page = $menu;
  $item_table = "trunks";  
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			
			<div>										
				<fieldset>
					<legend>
					<?php echo $title;?>
					</legend>	
					<?php include("ivr_title.php");?>								
					
					<table class="table table-condensed table-hover">							
						<thead>
							<TR>							
								<th>Название</th>
								<th>Тип</th>															
                                                                <th>IP адрес</th>															
							</TR>
						<thead>
						<tbody>
							<?php 
							$counter = 0;	
							while($trunk_row= mysql_fetch_array($trunk_rs))								
							{
							$counter++;
							?>
								<tr>									
									<td class="col-xs-4" > 
										<?php echo $trunk_row["channelid"]?>
									</td>																																				
									<td class="col-xs-4" > 
										<?php echo $trunk_row["tech"]?>
									</td>
									<td class="col-xs-4" > 
										<?php echo $trunk_row["ip"]?>
									</td>
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
				</fieldset>
			</div>	

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
			  <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" ><span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> Клиента</h4>
					</div>
					<div class="modal-body">		
						<form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">

						  <input type="hidden" id="item_action" name="item_action" value="">	
						  <input type="hidden" id="item_id" name="item_id" value="">
						  
						  <div class="form-group">
							<label class="col-sm-4 control-label">Описание</label>
							<div class="col-sm-8">
							  <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
							</div>
						  </div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
						<button class="btn btn-primary"  type="button"  onclick="ItemSubmitForm(this)" >Выполнить</button>
					</div>				  
				</div>
			  </div>
			</div>			
			
			<script>
			
				function showItemModal(obj){
					
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
					
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();					
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
			
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
				    var action = document.getElementById("item_action").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;	
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
							document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}		

				function checkForm(){
				
					var error_text="";
					
					var description = document.getElementById("item_description").value;
					
					if(description=="")
					{
						error_text+="Заполните поле 'Описание' !!!\n";				
					}		
					
					return 	error_text;
				}
				
				function setItemParameters(){	
					
					var action = document.getElementById("item_action").value;
					var item_id = document.getElementById("item_id").value;		
					if(action=="new"){
						document.getElementById("item_description").value = "";				
					}else{
						document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");				
					}	
				}
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>
