﻿<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "ivr";
  $ivr_menu = "node";
  $ivr_title = "Структура меню IVR";
  $page = "node";
  $item_table = "node";  
  $ivr_id = $_REQUEST["ivr_id"];    
  include("head.php"); 
  ?>
  <body>

  <?php 
  include("top_navigation_bar.php"); 
  include("ivr_attributes.php");
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	      <!--CONTENT BEGIN--> 
		  <!--ON PAGE LOAD SCRIPTS-->
			<?php
			
				$sounds_path =  "/var/www/html/sm/sounds/ivr/nodes";   
				if($_REQUEST["item_action"]=="new")	
				{
				
					$priority=0;
					$query="select max(priority) as priority from ".$item_table." where parent_id=".$_REQUEST["item_node_parent_id"]; 
					$rs = mysql_query($query);			
					while($row= mysql_fetch_array($rs))
					{
						$priority=$row["priority"] + 1;
					}
					
					if($priority==NULL){
						$priority=1;
					}
					
		
					$insert_query ="INSERT INTO ".$item_table."(";
					$insert_query.="ivr_id,";
					$insert_query.="parent_id,";
					$insert_query.="description,";
					$insert_query.="txt_kz,";
					$insert_query.="txt_ru,";
					$insert_query.="node_type,";
					$insert_query.="command,";
					$insert_query.="trunk,";
					$insert_query.="dst_kz,";
					$insert_query.="dst_ru,";
                                        $insert_query.="work_time,";
					$insert_query.="priority,";
					$insert_query.="is_active";
					$insert_query.=")";
					$insert_query.=" VALUES(";
					$insert_query.=$ivr_id.",";
					$insert_query.=$_REQUEST["item_node_parent_id"].",";
					$insert_query.="'".$_REQUEST["item_node_description"]."',";
					$insert_query.="'".$_REQUEST["item_node_txt_kz"]."',";
					$insert_query.="'".$_REQUEST["item_node_txt_ru"]."',";
					$insert_query.="'".$_REQUEST["item_node_type"]."',";
					$insert_query.="'".$_REQUEST["item_node_command"]."',";
					$insert_query.="'".$_REQUEST["item_node_trunk"]."',";
					$insert_query.="'".$_REQUEST["item_node_dst_kz"]."',";
					$insert_query.="'".$_REQUEST["item_node_dst_ru"]."',";
                                        $insert_query.="'".$_REQUEST["item_work_time"]."',";
					$insert_query.=$priority.",";
					$insert_query.="1";
					$insert_query.=")";

					//echo "====".$insert_query."===*<br>";
					mysql_query($insert_query);	
								
					$query="select max(id) as item_id from ".$item_table." where ivr_id = ".$ivr_id; 
					$rs = mysql_query($query);	
					$item_id="";
					while($row= mysql_fetch_array($rs))
					{
						$item_id=$row["item_id"];
					}
					
					//echo 	$item_id;	
					upload_file($sounds_path."/kz","item_node_sound_kz",$item_id);
					upload_file($sounds_path."/ru","item_node_sound_ru",$item_id);					
				
				?>
					<script>			
						location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Запись успешно добавлена!";
					</script>		
				<?php					
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_node_description"]."',";
					$query.=" node_type='".$_REQUEST["item_node_type"]."',";
					$query.=" command='".$_REQUEST["item_node_command"]."',";
					$query.=" trunk='".$_REQUEST["item_node_trunk"]."',";
					$query.=" dst_kz='".$_REQUEST["item_node_dst_kz"]."',";
					$query.=" dst_ru='".$_REQUEST["item_node_dst_ru"]."',";
					$query.=" txt_kz='".$_REQUEST["item_node_txt_kz"]."',";
					$query.=" txt_ru='".$_REQUEST["item_node_txt_ru"]."',";
                                        $query.=" work_time='".$_REQUEST["item_work_time"]."'";			
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					if($_FILES["item_node_sound_kz"]["name"]!=""){		
						upload_file($sounds_path."/kz","item_node_sound_kz",$_REQUEST["item_id"]);
					}	
					if($_FILES["item_node_sound_ru"]["name"]!=""){		
						upload_file($sounds_path."/ru","item_node_sound_ru",$_REQUEST["item_id"]);
					}						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Запись успешно удалена!";
					</script>		
					<?php				
				}	

				if($_REQUEST["LangNode"]==1)	
				{
				
				
					$query="select count(0) as counter from  ".$item_table." where ivr_id = ".$ivr_id." and node_type = 'LANG'";
					//echo $query."<br>";
					$langrs = mysql_query($query);	
					$counter = "0";
					while($langrow= mysql_fetch_array($langrs))
					{
						if($langrow["counter"]!="0"){
							$counter = $langrow["counter"];
						}
						
					}			
					
					if($counter == "0"){
						$query="insert into ".$item_table."(ivr_id,node_type,txt_kz,txt_ru) values (".$ivr_id.",'LANG','".$_REQUEST["lang_node_txt_kz"]."', '".$_REQUEST["lang_node_txt_ru"]."')";
						//echo $query."<br>";
						$rs = mysql_query($query);	
					}else{
						$query="update ".$item_table." set txt_kz='".$_REQUEST["lang_node_txt_kz"]."', txt_ru='".$_REQUEST["lang_node_txt_ru"]."' where node_type='LANG' and ivr_id = ".$ivr_id;
						//echo $query."<br>";
						$rs = mysql_query($query);	
					}
					
					if($_FILES["lang_node_sound_kz"]["name"]!=""){					
						upload_file($sounds_path."/kz","lang_node_sound_kz","ivr_".$ivr_id."_lang");
					}	
					if($_FILES["lang_node_sound_ru"]["name"]!=""){					
						upload_file($sounds_path."/ru","lang_node_sound_ru","ivr_".$ivr_id."_lang");
					}	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Изменения успешно сохранены.";
					</script>		
					<?php
					
				}			

				if($_REQUEST["ParentNode"]==1)	
				{
				
					$priority=0;
					$query="select max(priority) as priority from ".$item_table." where parent_id=".$_REQUEST["parent_node_id"]; 
					$rs = mysql_query($query);			
					while($row= mysql_fetch_array($rs))
					{
						$priority=$row["priority"] + 1;
					}					
                                    
					$query =" UPDATE ".$item_table." SET ";
                                        $query.=" priority=".$priority.",";
					$query.=" parent_id=".$_REQUEST["parent_node_id"]."";
					$query.=" WHERE id = ".$_REQUEST["node_id"]; 
					$rs = mysql_query($query);	
					//echo $query;

					?>
					<script>			
						location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Изменения успешно сохранены.";
					</script>		
					<?php
					
				}					
			
                                if($_REQUEST["priority_item_id"]){
                                    $tmp_old_pr="select priority,parent_id from ".$item_table." where id='".$_REQUEST['priority_item_id']."'" ;
                                    $rs_old_pr = mysql_query($tmp_old_pr);
                                    //echo $tmp_old_pr."</br>";
                                    while($row_old_pr = mysql_fetch_array($rs_old_pr))
                                    {
                                            $parent_id = $row_old_pr['parent_id'];                                            
                                            $old_pr=$row_old_pr['priority'];                                            
                                    }			

                                    //echo "st=".$_REQUEST['priority_action']."<br>";
                                    if($_REQUEST['priority_action']=="<")
                                    {
                                            $order="desc";
                                    }
                                    else
                                    {
                                            $order="asc";
                                    }
                                    $tmp_old_pr="select id,priority from ".$item_table." where parent_id = '".$parent_id."' and priority".$_REQUEST['priority_action'].$old_pr." order by priority ".$order." limit 0,1";
                                   // echo $tmp_old_pr."<br>";
                                    $rs_old_pr = mysql_query($tmp_old_pr);
                                    while($row_old_pr = mysql_fetch_array($rs_old_pr))
                                    {
                                            $new_pr=$row_old_pr['priority'];
                                            $sibling_node_id=$row_old_pr['id'];					
                                    }			

                                    $query="update ".$item_table." set priority=".$new_pr." where id=".$_REQUEST['priority_item_id'];
                                   // echo $query."<br>";
                                    $rs = mysql_query($query);	

                                    $query="update ".$item_table." set priority=".$old_pr." where id=".$sibling_node_id;
                                   // echo $query."<br>";
                                    $rs = mysql_query($query);	
                                    ?>
                                    <script>			
                                          location.href = "<?php echo $page;?>.php?ivr_id=<?php echo $ivr_id;?>&message=Изменения успешно сохранены.";
                                    </script>		
                                    <?php				
		                                    
                                    
                                }
                                
			?>		  

				<fieldset>
					<legend>
                                            <span class="label label-info" style="margin-bottom: 15px;"><?php echo $ivr_description;?></span> <img src="./../img/right-arrow-3.png" width="20px" height="20px"> <span class="label label-default">Структура меню IVR</span>
                                            <br>
					</legend>						
                                <br>

				<?php include("ivr_nav.php");?>	
				</br>
				<?php
				if($ivr_id!=""){?>
					<div style="overflow:scroll;height:100%;width:100%;vertical-align:middle" class="affix">   
								
						<table style="vertical-align:middle;margin-bottom:500px;height:75%;" cellpadding="0" cellspacing="0">
							<tr>									
								<?php													
								
								$query="select * from  ".$item_table." where ivr_id=".$ivr_id." and node_type = 'LANG'";
								//echo $query;
								$langrs = mysql_query($query);	
								while($langrow= mysql_fetch_array($langrs))
								{
									$lang_txt_kz = $langrow["txt_kz"];
									$lang_txt_ru = $langrow["txt_ru"];
								}		
								?>												
								<td style="padding-right:5px;">
								  <div class="btn-group">
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">							  
                                                                            <img src="./../img/lang-1.png" width="20px" height="20px" />
                                                                                   
									</button>
									<ul class="dropdown-menu">
										<li role="presentation" class="dropdown-header">Приветствие и выбор языка</li>
										<li role="presentation" role="menuitem" tabindex="-1" href="#" onclick="showLangModal(this)" txt_kz="<?php echo $lang_txt_kz;?>" txt_ru="<?php echo $lang_txt_ru;?>">
											<a href="#">
												<span class="glyphicon glyphicon-pencil" ></span> Редактировать
											</a>
										</li>
										<li role="presentation" onclick="showItemModal(this)" parent_id="1"  action="new">
											<a role="menuitem" tabindex="-1" href="#">
												<span class="glyphicon glyphicon-plus" ></span>  Добавить внутреннюю ветку
											</a>
										</li>
									</ul>
								  </div>	
								</td>
								<td style="border-left:solid 2px #009721;">																	
									<table  height="100%" cellpadding="0" cellspacing="0">
										<?php
										$path = "Выбор языка";
										get_tree($ivr_id,"1",$path);
										?>											
									</table>
									
								</td>   								
							</tr>	
						</table>
					</div>	
				</fieldset>	
					
				<?php
				}
				?>				
				</br>
				
                        <form id="PriorityForm" method="post" >						                                				
                                <input type="hidden" id="priority_item_id" name="priority_item_id"  value="" />
                                <input type="hidden" id="priority_action" name="priority_action"  value="" />										
                        </form>
 			
			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ItemForm"  method="post"  enctype="multipart/form-data">
			  <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" >
						<span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> Ветки</h4>
					</div>
					<div class="modal-body">		
						

						  <input type="hidden" id="item_action" name="item_action" value="">	
						  <input type="hidden" id="item_id" name="item_id" value="">
						  <input type="hidden" id="item_node_parent_id" name="item_node_parent_id"  value=""/>
                                                  <input type="hidden" id="item_work_time" name="item_work_time" value="">
						  <div class="form-group">
							<label class="col-sm-4 control-label">Описание</label>
							<div class="col-sm-8">
							  <input type="text" class="form-control" id="item_node_description" name="item_node_description" placeholder="Введите текст">
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-4 control-label">Выберите тип ветки</label>
							<div class="col-sm-8">
							  <select id="item_node_type" name="item_node_type" class="form-control" onchange="set_item_type()">
									<option value="">не выбран</option>
									<option value="info">информационый</option>
									<option value="call">соединение</option>
									<option value="list">список</option>													
									<option value="back">переход в предыдущюю ветку</option>																					
									<option value="mainmenu">переход в главное меню</option>
							  </select>							  
							</div>
						  </div>

						  <div class="form-group" id="item_node_comand_div" name="item_node_comand_div">
							<label class="col-sm-4 control-label">Команда</label>
							<div class="col-sm-8">
							  <select id="item_node_command" name="item_node_command" class="form-control" onchange="set_item_type()">
									<option value="">не выбран</option>
									<option value="autocall" id="item_autocallOption" style="display:none">автоматическое соединение</option>		
									<option value="byorder" >по очередности от 1 до 9</option>
									<option value="*" >* (звездочка)</option>
									<option value="0">0 (ноль)</option>
							  </select>							  
							</div>
						  </div>
						  
						  <div class="form-group" id="item_node_trunk_div" name="item_node_trunk_div"  style="display:none">
							<label class="col-sm-4 control-label">Канал</label>
							<div class="col-sm-8">
							  <select id="item_node_trunk" name="item_node_trunk" class="form-control">
								<option value="" >не выбран</option>
								<?php
								while($trunk_row= mysql_fetch_array($trunk_rs))
								{
								?>
									<option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>
									
								<?php
								}								
								?>
							  </select>							  
							</div>
						  </div>						  
						  
                                                    <span id="work_time" style="display: none;">
                                                        <div class="form-group">
                                                              <label class="col-md-offset-3 col-sm-4 control-label">График рабочего времени</label>
                                                        </div>									

                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Понедельник</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_monday" name="item_monday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_monday_begin" name="item_monday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_monday_end" name="item_monday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Вторник</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_tuesday" name="item_tuesday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_tuesday_begin" name="item_tuesday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_tuesday_end" name="item_tuesday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Среда</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_wednesday" name="item_wednesday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_wednesday_begin" name="item_wednesday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_wednesday_end" name="item_wednesday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Четвег</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_thursday" name="item_thursday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_thursday_begin" name="item_thursday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_thursday_end" name="item_thursday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Пятница</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" checked style="margin-top: 10px;" id="item_friday" name="item_friday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_friday_begin" name="item_friday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_friday_end" name="item_friday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Суббота</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" style="margin-top: 10px;" id="item_saturday" name="item_saturday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_saturday_begin" name="item_saturday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_saturday_end" name="item_saturday_end">										
                                                                </div>

                                                        </div>
                                                        <div class="form-group" >
                                                                <label class="col-sm-4 control-label">Воскресение</label>										
                                                                <div class="col-sm-1">
                                                                    <input type="checkbox" style="margin-top: 10px;" id="item_sunday" name="item_sunday">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    С <input type="time"  value="09:00"  id="item_sunday_begin" name="item_sunday_begin">										
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    До <input type="time"  value="18:00"  id="item_sunday_end" name="item_sunday_end">										
                                                                </div>

                                                        </div>
                                                    </span>    
                                                  
						  <span id="item_node_sound" name="item_node_sound">
                                                        </br></br>
                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label"></label>
                                                              <div class="col-sm-8">
                                                                      <div class="controls">	
                                                                        <div class="row">			 			
                                                                                <div class="col-md-6" id="item_kz_div" name="item_kz_div" class="span4">
                                                                                        <?php echo $kz_img;?>	
                                                                                         казахский
                                                                                </div>	   
                                                                                <div class="col-md-6" id="item_ru_div" name="item_ru_div" class="span4">
                                                                                        <?php echo $ru_img;?>	
                                                                                         русский
                                                                                </div>	   
                                                                              </div>  
                                                                      </div>						  
                                                              </div>
                                                        </div>	
                                                      
                                                        <div class="form-group" style="vertical-align:middle;">
                                                              <label class="col-sm-4 control-label">Звуковой файл</label>										
                                                              <div class="col-sm-4">
                                                                  <input type="file" class="form-control" id="item_node_sound_kz" name="item_node_sound_kz" title="Выберите файл на казахском языке">										
                                                              </div>
                                                              <div class="col-sm-4">
                                                                  <input type="file" class="form-control" id="item_node_sound_ru" name="item_node_sound_ru" title="Выберите файл на русском языке">										
                                                               </div>   
                                                        </div>

                                                        <div class="form-group">
                                                              <label class="col-sm-4 control-label">Текст</label>
                                                              <div class="col-sm-4">
                                                                  <textarea rows="5" class="form-control" id="item_node_txt_kz" name="item_node_txt_kz" placeholder="Введите текст звукового файла на казахском языке"></textarea>
                                                              </div>
                                                              <div class="col-sm-4">
                                                                  <textarea rows="5" class="form-control" id="item_node_txt_ru" name="item_node_txt_ru" placeholder="Введите текст звукового файла на русском языке"></textarea>
                                                              </div>

                                                        </div>

                                                        <div class="form-group" id="item_node_dst_div" name="item_node_dst_div"  style="display:none">
                                                              <label class="col-sm-4 control-label">Номер дозвона</label>
                                                              <div class="col-sm-4">
                                                                  <input type="text" class="form-control" id="item_node_dst_kz" name="item_node_dst_kz" placeholder="Введите номер">
                                                              </div>
                                                              <div class="col-sm-4">
                                                                  <input type="text" class="form-control" id="item_node_dst_ru" name="item_node_dst_ru" placeholder="Введите номер">
                                                              </div>

                                                        </div>									  
						  </span>
						  
						
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                                <button class="btn btn-primary"  type="submit" >Выполнить</button>
					</div>				  
				</div>
			  </div>
                            </form>
			</div>		

			<!--LANG -->
			<div class="modal fade" id="LangModal" name="LangModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
			    <form class="form-horizontal" role="form" id="LangNodeForm"  method="post"  enctype="multipart/form-data">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" >Приветсвие и выбор языка</h4>
                                        </div>
                                        <div class="modal-body">		
                                            <input type="hidden" name="LangNode" id="LangNode" value=""/>
                                            <div class="form-group">							
                                                <div class="col-sm-12">
                                                    <div class="controls">	
                                                        <div class="row">			 			
                                                            <div class="col-md-6 text-left">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">                                                          
                                                                        <img src="./../img/kz.png" width="30" height="30" >	
                                                                        Казахский                                                                            
                                                                    </div>
                                                                    <div class="panel-footer">						  
                                                                        <button type="button" class="btn btn-default"  onClick="playMainSound('sounds/ivr/nodes/kz/ivr_<?php echo $ivr_id ?>_lang.wav')">
                                                                           <img src="./../img/playsound-1.png" width="15px" height="15px" />
                                                                        </button> Прослушать 
                                                                        </br>
                                                                        </br>                                                                                                                                                                    
                                                                        <input type="file" id="lang_node_sound_kz" name="lang_node_sound_kz" placeholder="Выберите файл на казахском языке">										
                                                                        </br>
                                                                        </br>                                                                                                                                                                    
                                                                        <textarea rows="7" cols="50"  placeholder="Введите текст звукового файла на казахском языке" id="lang_node_txt_kz" name="lang_node_txt_kz"></textarea>										  										   
                                                                    </div>
                                                                </div> 
                                                            
                                                            
                                                            </div>	   
                                                            
                                                            <div class="col-md-6 text-left">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">                                                          
                                                                        <img src="./../img/ru.png" width="30" height="30" >	
                                                                        Русский                                                                        
                                                                    </div>
                                                                    <div class="panel-footer">						  
                                                                        <button type="button" class="btn btn-default"  onClick="playMainSound('sounds/ivr/nodes/ru/ivr_<?php echo $ivr_id ?>_lang.wav')">
                                                                           <img src="./../img/playsound-1.png" width="15px" height="15px" />
                                                                        </button> Прослушать
                                                                        </br>
                                                                        </br>
                                                                        <input type="file" id="lang_node_sound_ru" name="lang_node_sound_ru" placeholder="Выберите файл на русском языке">										
                                                                        </br>
                                                                        </br>
                                                                        <textarea rows="7" cols="50"  placeholder="Введите текст звукового файла на русском языке" id="lang_node_txt_ru" name="lang_node_txt_ru"></textarea>										  
                                                                    </div>
                                                                </div> 
                                                            </div>	   
                                                        </div>  
                                                    </div>						  
                                                </div>
                                            </div>	
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                            <button class="btn btn-primary"  type="submit"  >Сохранить</button>
                                        </div>				  
                                    </div>
                                </div>
                            </form>
			</div>			

			<div class="modal fade" id="ParentModal" name="ParentModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ParentNodeForm"  method="post"  enctype="multipart/form-data">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                 <h4 class="modal-title" >Перемещение ветки по дереву</h4>
                                        </div>
                                        <div class="modal-body">		
                                            <input type="hidden" name="ParentNode" id="ParentNode" value=""/>
                                            <input type="hidden" name="node_id" id="node_id" value=""/>
                                            <div class="form-group">
                                                  <label class="col-sm-4 control-label">Выберите номер ветки в которую выхотите переместить данную ветку</label>
                                                  <div class="col-sm-8">
                                                        <select  id="parent_node_id" name="parent_node_id" style="width:100%;" class="selectpicker form-control">
                                                            <option value="" selected> не выбрано </option>
                                                            <option value="0"> главное меню </option>
                                                            <?php
                                                            $query="select * from  ".$item_table." where ivr_id=".$ivr_id." and node_type = 'list' order by id";
                                                            //echo $query;
                                                            $parentrs = mysql_query($query);	
                                                            while($parentrow= mysql_fetch_array($parentrs))
                                                            {?>					
                                                                    <option value="<?php echo $parentrow["id"]?>"><?php echo "(".$parentrow["id"].") ".substr($parentrow["description"],0,50); ?></option>	
                                                            <?php
                                                            }					  
                                                            ?>				  
                                                        </select>	
                                                  </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                            <button class="btn btn-primary"  type="submit" >Выполнить</button>
                                        </div>				  
                                    </div>
                                </div>
                            </form>   
			</div>					
			<script>                                
                            
                                $(document).ready(function() {
                                    $('#ItemForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            item_node_description: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Заполните Описание'
                                                    }
                                                }
                                            },
                                            item_node_type: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Выберите тип'
                                                    }
                                                }
                                            },
                                            item_node_command: {
                                                selector: '#item_node_command',
                                                validators: {
                                                    callback: {
                                                        message: 'Выберите поле Команда',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type!="info" && value==""){
                                                                return false;                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_command', 'VALID', 'callback');
                                                                return true;
                                                            }               

                                                        }
                                                    }
                                                }
                                            },
                                            item_node_sound_kz: {
                                                selector: '#item_node_sound_kz',
                                                validators: {
                                                    callback: {
                                                        message: 'Выберите звуковой файл на казахском языке',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            var action = document.getElementById("item_action").value;
                                                            if(type != "back" && type != "mainmenu" ){
                                                                if(value=="" && action=="new")
                                                                {
                                                                    return false;                                                                
                                                                }else{
                                                                    validator.updateStatus('item_node_sound_kz', 'VALID', 'callback');
                                                                    return true;
                                                                }	                                                            
                                                            }else{
                                                                validator.updateStatus('item_node_sound_kz', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_sound_ru: {
                                                selector: '#item_node_sound_ru',
                                                validators: {
                                                    callback: {
                                                        message: 'Выберите звуковой файл на русском языке',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            var action = document.getElementById("item_action").value;
                                                            if(type != "back" && type != "mainmenu" ){
                                                                if(value=="" && action=="new")
                                                                {
                                                                    return false;                                                                
                                                                }else{
                                                                    validator.updateStatus('item_node_sound_ru', 'VALID', 'callback');
                                                                    return true;
                                                                }	                                                            
                                                            }else{
                                                                validator.updateStatus('item_node_sound_ru', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_txt_kz: {
                                                selector: '#item_node_txt_kz',
                                                validators: {
                                                    callback: {
                                                        message: 'Введите текст звукового файла на казахском языке',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type != "back" && type != "mainmenu" && value==""){
                                                                return false;                                                                                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_txt_kz', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_txt_ru: {
                                                selector: '#item_node_txt_ru',
                                                validators: {
                                                    callback: {
                                                        message: 'Введите текст звукового файла на русском языке',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type != "back" && type != "mainmenu" && value==""){
                                                                return false;                                                                                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_txt_ru', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_trunk: {
                                                selector: '#item_node_trunk',
                                                validators: {
                                                    callback: {
                                                        message: 'Укажите Канал(транк)',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type == "call" && value==""){
                                                                return false;                                                                                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_trunk', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_dst_kz: {
                                                selector: '#item_node_dst_kz',
                                                validators: {
                                                    callback: {
                                                        message: 'Укажите номер',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type == "call" && value==""){
                                                                return false;                                                                                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_dst_kz', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            },
                                            item_node_dst_ru: {
                                                selector: '#item_node_dst_ru',
                                                validators: {
                                                    callback: {
                                                        message: 'Укажите номер',
                                                        callback: function(value, validator) {                                                            
                                                            var type = document.getElementById("item_node_type").value;	
                                                            if(type == "call" && value==""){
                                                                return false;                                                                                                                                
                                                            }else{
                                                                validator.updateStatus('item_node_dst_ru', 'VALID', 'callback');
                                                                return true;
                                                            }               
                                                        }
                                                    }
                                                }
                                            }                                            
                                        }
                                    });
                                });
                            
                                $(document).ready(function() {
                                    $('#LangNodeForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            lang_node_txt_kz: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Введите текст звукового файла на казахском языке'
                                                    }
                                                }
                                            },
                                            lang_node_txt_ru: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Введите текст звукового файла на русском языке'
                                                    }
                                                }
                                            }                                            
                                            
                                        }
                                    });
                                });


                                $(document).ready(function() {
                                    $('#ParentNodeForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            parent_node_id: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            }                                            
                                        }
                                    });
                                });
                            
                            
                                function change_priority(obj)
                                {       
                                       // alert(obj.getAttribute('action'));
                                        document.getElementById("priority_item_id").value=obj.id;					
                                        document.getElementById("priority_action").value=obj.getAttribute('action');															
                                        document.getElementById("PriorityForm").submit();
                                }
                                
				function showParentNodeModal(obj){
                                        $('#ParentNodeForm').data('bootstrapValidator').resetForm(true);
					document.getElementById("node_id").value = obj.getAttribute('node_id');	
					document.getElementById("parent_node_id").value = obj.getAttribute('parent_id');					
					$("#ParentModal").modal("show");	
				
				}
				
				function parentNodeSubmitForm(obj){
					if(document.getElementById("parent_node_id").value!=""){
						document.getElementById("ParentNode").value=1;
						document.getElementById("ParentNodeForm").submit()					
					}	
				
				}				
				
				function showLangModal(obj){
                                        $('#LangNodeForm').data('bootstrapValidator').resetForm(true);
					document.getElementById("lang_node_txt_ru").value = obj.getAttribute('txt_ru');
					document.getElementById("lang_node_txt_kz").value = obj.getAttribute('txt_kz');
					$("#LangModal").modal("show");					
				}
				
				function LangSubmitForm(obj){
					obj.innerHTML='Сохранение...';
					var txt_kz = document.getElementById("lang_node_txt_kz").value;	
					var txt_ru = document.getElementById("lang_node_txt_ru").value;	

					var error_text="";
					if(txt_kz=="")
					{
						error_text+="Введите текст звукового на казахском языке!!!\n";				
					}							
					
					if(txt_ru=="")
					{
						error_text+="Введите текст звукового на русском языке!!!\n";				
					}							

					if(error_text=="")
					{
						document.getElementById("LangNode").value=1;
						document.getElementById("LangNodeForm").submit()
					}
					else
					{
						alert(error_text);								
						obj.innerHTML='Сохранить';
					}						
				}				
				
				function showItemModal(obj){
					$('#ItemForm').data('bootstrapValidator').resetForm(true);
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;
					if(action=="new"){
						document.getElementById("item_node_parent_id").value = obj.getAttribute("parent_id");										
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters(obj);
											
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");							
						setItemParameters(obj);
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
                                /*
				function ItemSubmitForm(obj)
				{				
					var id = document.getElementById("item_id").value;
                                        var action = document.getElementById("item_action").value;
					var error_text="";								
					
					if(action=="delete"){					
						obj.innerHTML='Удаление...';
						document.getElementById("item_id").value = id;	
						document.getElementById("ItemForm").submit();
					}else{
					
						error_text = checkForm();				
						
						if(error_text=="")
						{	
                                                        getWorkTime();
                                                        document.getElementById("ItemForm").submit();						
							if(action=="new"){					
								obj.innerHTML='Добавление...';
							}else if(action=="edit"){					
								obj.innerHTML='Сохранение...';
								document.getElementById("item_id").value = id;
							}						
						}else{
							alert(error_text);	
						}					
					}
					
				}	
                                

				function checkForm(){
					
					var error_text="";
					var action = document.getElementById("item_action").value;
					var type = document.getElementById("item_node_type").value;						
					var description = document.getElementById("item_node_description").value;	
					var command = document.getElementById("item_node_command").value;	
					var trunk = document.getElementById("item_node_trunk").value;	
					var sound_kz = document.getElementById("item_node_sound_kz").value;	
					var sound_ru = document.getElementById("item_node_sound_ru").value;						
					
					var txt_kz = document.getElementById("item_node_txt_kz").value;	
					var txt_ru = document.getElementById("item_node_txt_ru").value;	
					var dst_kz = document.getElementById("item_node_dst_kz").value;		
					var dst_ru = document.getElementById("item_node_dst_ru").value;	
										
					var error_text="";
					
					if(type=="")
					{
						error_text+="Выберите тип!!!\n";							
					}
					else
					{
						if(description=="")
						{
							error_text+="Заполните поле 'Описание' !!!\n";				
						}				
						
						if(type!="info" && command=="")
						{
							error_text+="Выберите поле 'Команда'!!!\n";				
						}
												
						if(type != "back" && type != "mainmenu" ){
							if(sound_kz=="" && action=="new")
							{
								error_text+="Выберите звуковой файл на казахском языке!!!\n";				
							}	
							if(txt_kz=="")
							{
								error_text+="Введите текст звукового на казахском языке!!!\n";				
							}	
							if(sound_ru=="" && action=="new")
							{
								error_text+="Выберите звуковой файл на русском языке!!!\n";				
							}							

							if(txt_ru=="")
							{
								error_text+="Введите текст звукового на русском языке!!!\n";				
							}	
						}
						
						if(type=="call" && trunk=="")
						{
							error_text+="Укажите 'Канал (транк)'!!!\n";				
						}			
						
						
						if(type=="call" && dst_kz=="")
						{
							error_text+="Укажите 'Номер дозвона'!!!\n";				
						}			
						
						if(type=="call" && dst_ru=="")
						{
							error_text+="Укажите 'Номер дозвона'!!!\n";				
						}			
						
						
					}
				
					
					return 	error_text;
				}
				*/
				function setItemParameters(obj){
					var action = document.getElementById("item_action").value;
                                        var type = document.getElementById("item_node_type").value;	
                                        
                                        if(type=="call"){
                                            setWorkTime(obj);
                                        }    
					
                                        if(action=="new"){
						document.getElementById("item_node_description").value = "";		
						document.getElementById("item_node_type").value = "";
						document.getElementById("item_node_command").value = "";
						document.getElementById("item_node_trunk").value = "";
						document.getElementById("item_node_dst_kz").value = "";
						document.getElementById("item_node_dst_ru").value = "";
						document.getElementById("item_node_txt_kz").value = "";
						document.getElementById("item_node_txt_ru").value = "";

					}else if(action=="edit"){                                                
						document.getElementById("item_node_description").value = obj.getAttribute("description");		
						document.getElementById("item_node_type").value = obj.getAttribute("node_type");									
						document.getElementById("item_node_command").value = obj.getAttribute("command");									
						document.getElementById("item_node_trunk").value = obj.getAttribute("trunk");									
						document.getElementById("item_node_dst_kz").value = obj.getAttribute("dst_kz");									
						document.getElementById("item_node_dst_ru").value = obj.getAttribute("dst_ru");							
						document.getElementById("item_node_txt_kz").value = obj.getAttribute("txt_kz");	
						document.getElementById("item_node_txt_ru").value = obj.getAttribute("txt_ru");					
						set_item_type();                                                
					}	
                                        
                                        
				}
				
                                function setWorkTime(obj){
                                    
                                    var action = document.getElementById("item_action").value;
                                    if(action=="edit"){                                        
                                        var work_timeString = obj.getAttribute("work_time");
                                        document.getElementById("item_monday").checked =(work_timeString.split(";")[0].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_monday_begin").value = work_timeString.split(";")[0].split("/")[1].split("-")[0];
                                        document.getElementById("item_monday_end").value = work_timeString.split(";")[0].split("/")[1].split("-")[1];

                                        document.getElementById("item_tuesday").checked =(work_timeString.split(";")[1].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_tuesday_begin").value = work_timeString.split(";")[1].split("/")[1].split("-")[0];
                                        document.getElementById("item_tuesday_end").value = work_timeString.split(";")[1].split("/")[1].split("-")[1];

                                        document.getElementById("item_wednesday").checked =(work_timeString.split(";")[2].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_wednesday_begin").value = work_timeString.split(";")[2].split("/")[1].split("-")[0];
                                        document.getElementById("item_wednesday_end").value = work_timeString.split(";")[2].split("/")[1].split("-")[1];

                                        document.getElementById("item_thursday").checked =(work_timeString.split(";")[3].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_thursday_begin").value = work_timeString.split(";")[3].split("/")[1].split("-")[0];
                                        document.getElementById("item_thursday_end").value = work_timeString.split(";")[3].split("/")[1].split("-")[1];

                                        document.getElementById("item_friday").checked =(work_timeString.split(";")[4].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_friday_begin").value = work_timeString.split(";")[4].split("/")[1].split("-")[0];
                                        document.getElementById("item_friday_end").value = work_timeString.split(";")[4].split("/")[1].split("-")[1];

                                        document.getElementById("item_saturday").checked =(work_timeString.split(";")[5].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_saturday_begin").value = work_timeString.split(";")[5].split("/")[1].split("-")[0];
                                        document.getElementById("item_saturday_end").value = work_timeString.split(";")[5].split("/")[1].split("-")[1];

                                        document.getElementById("item_sunday").checked =(work_timeString.split(";")[6].split("/")[0]=="1")?true:false;                                    
                                        document.getElementById("item_sunday_begin").value = work_timeString.split(";")[6].split("/")[1].split("-")[0];
                                        document.getElementById("item_sunday_end").value = work_timeString.split(";")[6].split("/")[1].split("-")[1];
                                     }else{
                                        document.getElementById("item_monday").checked = true;                                    
                                        document.getElementById("item_monday_begin").value = "09:00";
                                        document.getElementById("item_monday_end").value = "18:00";

                                        document.getElementById("item_tuesday").checked = true;                                                                        
                                        document.getElementById("item_tuesday_begin").value = "09:00";
                                        document.getElementById("item_tuesday_end").value = "18:00";
                                        
                                        document.getElementById("item_wednesday").checked = true;                                                                       
                                        document.getElementById("item_wednesday_begin").value = "09:00";
                                        document.getElementById("item_wednesday_end").value = "18:00";

                                        document.getElementById("item_thursday").checked = true;                                                             
                                        document.getElementById("item_thursday_begin").value = "09:00";
                                        document.getElementById("item_thursday_end").value = "18:00";

                                        document.getElementById("item_friday").checked = true;                                                                       
                                        document.getElementById("item_friday_begin").value = "09:00";
                                        document.getElementById("item_friday_end").value = "18:00";

                                        document.getElementById("item_saturday").checked =false;                                    
                                        document.getElementById("item_saturday_begin").value = "09:00";
                                        document.getElementById("item_saturday_end").value = "18:00";

                                        document.getElementById("item_sunday").checked =false;                                    
                                        document.getElementById("item_sunday_begin").value = "09:00";
                                        document.getElementById("item_sunday_end").value = "18:00";
                                         
                                     }   


                                }
                                
                                function getWorkTime(){
                                    
                                    var work_timeString = "";
                                    var monday = document.getElementById("item_monday").checked;
                                    var monday_begin = document.getElementById("item_monday_begin").value;
                                    var monday_end = document.getElementById("item_monday_end").value;
                                    var tuesday = document.getElementById("item_tuesday").checked;
                                    var tuesday_begin = document.getElementById("item_tuesday_begin").value;
                                    var tuesday_end = document.getElementById("item_tuesday_end").value;
                                    var wednesday = document.getElementById("item_wednesday").checked;
                                    var wednesday_begin = document.getElementById("item_wednesday_begin").value;
                                    var wednesday_end = document.getElementById("item_wednesday_end").value;
                                    var thursday = document.getElementById("item_thursday").checked;
                                    var thursday_begin = document.getElementById("item_thursday_begin").value;
                                    var thursday_end = document.getElementById("item_thursday_end").value;
                                    var friday = document.getElementById("item_friday").checked;
                                    var friday_begin = document.getElementById("item_friday_begin").value;
                                    var friday_end = document.getElementById("item_friday_end").value;
                                    var saturday = document.getElementById("item_saturday").checked;
                                    var saturday_begin = document.getElementById("item_saturday_begin").value;
                                    var saturday_end = document.getElementById("item_saturday_end").value;
                                    var sunday = document.getElementById("item_sunday").checked;
                                    var sunday_begin = document.getElementById("item_sunday_begin").value;
                                    var sunday_end = document.getElementById("item_sunday_end").value;
                                    
                                    if(monday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + monday_begin + "-" + monday_end + ";";

                                    if(tuesday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + tuesday_begin + "-" + tuesday_end + ";";

                                    if(wednesday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + wednesday_begin + "-" + wednesday_end + ";";

                                    if(thursday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + thursday_begin + "-" + thursday_end + ";";

                                    if(friday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + friday_begin + "-" + friday_end + ";";

                                    if(saturday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + saturday_begin + "-" +saturday_end + ";";

                                    if(sunday){
                                        work_timeString += "1";
                                    }else{
                                        work_timeString += "0";
                                    }
                                    work_timeString += "/" + sunday_begin + "-" + sunday_end + ";";
                                    
                                    document.getElementById("item_work_time").value = work_timeString;
                                }
                                
				function expandLang(lang){
				
					var node_type = document.getElementById("item_node_type").value;
					if(node_type=="back" || node_type=="mainmenu"){
						lang="";
					}
					
					if(lang=="kz"){
						document.getElementById("item_kz_span").style.display="";
						document.getElementById("item_kz_div").style.opacity=1;
						document.getElementById("item_kz_div").style.fontWeight  = "bold";							
						
						document.getElementById("item_ru_span").style.display="none";
						document.getElementById("item_ru_div").style.opacity=0.3;
						document.getElementById("item_ru_div").style.fontWeight  = "normal";							
						
					}else if(lang=="ru"){
						document.getElementById("item_kz_span").style.display="none";
						document.getElementById("item_kz_div").style.opacity=0.3;
						document.getElementById("item_kz_div").style.fontWeight  = "normal";
						
						document.getElementById("item_ru_span").style.display="";		
						document.getElementById("item_ru_div").style.opacity=1;
						document.getElementById("item_ru_div").style.fontWeight  = "bold";	
					}else{
						document.getElementById("item_kz_span").style.display="none";
						document.getElementById("item_kz_div").style.opacity=0.3;
						document.getElementById("item_kz_div").style.fontWeight  = "normal";
						
						document.getElementById("item_ru_span").style.display="none";		
						document.getElementById("item_ru_div").style.opacity=0.3;
						document.getElementById("item_ru_div").style.fontWeight  = "normal";	
					
					}
				}		

				function set_item_type(){
					var type=document.getElementById("item_node_type").value;                                        
					if(type=="call"){
                                                document.getElementById("work_time").style.display="";
						document.getElementById("item_autocallOption").style.display="";
						document.getElementById("item_node_trunk_div").style.display="";
						document.getElementById("item_node_dst_div").style.display="";												
						
					}else{
                                                document.getElementById("work_time").style.display="none";
						document.getElementById("item_autocallOption").style.display="none";
						document.getElementById("item_node_trunk_div").style.display="none";
						document.getElementById("item_node_dst_div").style.display="none";						
					}
					
					
					if(type=="info"){
						document.getElementById("item_node_comand_div").style.display="none";
					}else{
						document.getElementById("item_node_comand_div").style.display="";
					}
					
					if(type=="back" || type=="mainmenu"){												
						document.getElementById("item_node_sound").style.display="none";						
					}else{						
						document.getElementById("item_node_sound").style.display="";
					}
				}	

				function expand_node_children(obj){					
					var id = obj.getAttribute('id');					
					//alert(document.getElementById("span" + id).style.display=="")
					
					if(document.getElementById("span" + id).style.display==""){
						obj.className = "glyphicon glyphicon-eye-close";
						document.getElementById("span" + id).style.display = "none";						
					}else{
						obj.className = "glyphicon glyphicon-eye-open";
						document.getElementById("span" + id).style.display = "";						
					}
				}				
			</script>			
			
		 <!--CONTENT END-->
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 	  
	  ?>
  </body>
</html>
