  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="./.././../bootstrap.3.1.1/css/bootstrap.css" rel="stylesheet">
    <link href="./../bootstrap.3.1.1/css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="./../bootstrap.3.1.1/css/bootstrap-datetimepicker.min.css">    
    <link href="./../bootstrap.3.1.1/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="./../bootstrap.3.1.1/css/bootstrapValidator.css" rel="stylesheet">

    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/bootstrap.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/docs.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/moment-with-langs.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/bootstrap-datetimepicker.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/bootstrap-select.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/bootstrapValidator.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/validator/notEmpty.js"></script>
    <script language="JavaScript" type="text/javascript" src="./../bootstrap.3.1.1/js/validator/callback.js"></script>
    
    <script type="text/javascript">
       
    /**
     * Russian translation for bootstrap-datepicker
     * Victor Taranenko <darwin@snowdale.com>
     */
    ;(function($){
            $.fn.datepicker.dates['ru'] = {
                    days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
                    daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
                    daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
                    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                    monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                    today: "Сегодня",
                    weekStart: 1
            };
    }(jQuery));
    
    
    $.fn.datetimepicker.defaults = {
        pickDate: true,                 //en/disables the date picker
        pickTime: true,                 //en/disables the time picker
        useMinutes: true,               //en/disables the minutes picker
        useSeconds: true,               //en/disables the seconds picker
        useCurrent: true,               //when true, picker will set the value to the current date/time     
        minuteStepping:1,               //set the minute stepping
        minDate:`1/1/1900`,               //set a minimum date
        maxDate: ,     //set a maximum date (defaults to today +100 years)
        showToday: true,                 //shows the today indicator
        language:"ru",                  //sets language locale
        defaultDate:"",                 //sets a default date, accepts js dates, strings and moment objects
        disabledDates:[],               //an array of dates that cannot be selected
        enabledDates:[],                //an array of dates that can be selected
        icons = {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up:   'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down'
        }
        useStrict: false,               //use "strict" when validating dates  
        sideBySide: false,              //show the date and time picker side by side
        daysOfWeekDisabled:[]          //for example use daysOfWeekDisabled: [0,6] to disable weekends 
    };	



    /**
     * Kazakh translation for bootstrap-datepicker
     * Yerzhan Tolekov <era.tolekov@gmail.com>
     */
    ;(function($){
            $.fn.datepicker.dates['kk'] = {
                    days: ["Жексенбі", "Дүйсенбі", "Сейсенбі", "Сәрсенбі", "Бейсенбі", "Жұма", "Сенбі", "Жексенбі"],
                    daysShort: ["Жек", "Дүй", "Сей", "Сәр", "Бей", "Жұм", "Сен", "Жек"],
                    daysMin: ["Жк", "Дс", "Сс", "Ср", "Бс", "Жм", "Сн", "Жк"],
                    months: ["Қаңтар", "Ақпан", "Наурыз", "Сәуір", "Мамыр", "Маусым", "Шілде", "Тамыз", "Қыркүйек", "Қазан", "Қараша", "Желтоқсан"],
                    monthsShort: ["Қаң", "Ақп", "Нау", "Сәу", "Мамыр", "Мау", "Шлд", "Тмз", "Қыр", "Қзн", "Қар", "Жел"],
                    today: "Бүгін",
                    weekStart: 1
            };
    }(jQuery));

    </script> 	
    
    <?php
    if($_REQUEST["message"]){
        ?>							
        <script>						
                $('#messageModal').modal('show')
        </script>	
        <?php
    }	
    ?>    


    <script type="text/javascript">
    //Validator
    $(window).on('load', function () {
        $('.selectpicker').selectpicker({             
            'selectedText': 'cat'
        });

    });
    </script>

    
    
  </head>