<html lang="en">
  <?php 
  include("head_template.php"); 
  ?>
  <body>

  <?php 
  include("top_navigation_bar_template.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu_template.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	      <!--CONTENT BEGIN--> 
		  <?php 
		  include("content.html"); 
		  ?>
		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js_template.html"); 
	  ?>
  </body>
</html>
