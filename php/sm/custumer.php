<html lang="en">
  <?php 
  
  //PAGE VARIABLES
  $menu = "custumer";
  $title = "Список клиентов";
  $page = $menu;
  $item_table = "custumer";  
  
  include("head.php"); 
  ?>
  <body>
  <?php 
  include("top_navigation_bar.php"); 
  ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
		  <?php 
		  include("left_menu.php"); 
		  ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		   <!--CONTENT--> 	
			
			<!--ON PAGE LOAD SCRIPTS-->
			<?php
				if($_REQUEST["item_action"]=="new")	
				{
				
					$insert_query ="INSERT INTO ".$item_table."(";
					$insert_query.="description,";
                                        $insert_query.="trunk,";                                        
					$insert_query.="create_date";
					$insert_query.=")";
					$insert_query.=" VALUES(";
					$insert_query.="'".$_REQUEST["item_description"]."',";
                                        $insert_query.="'".$_REQUEST["item_trunk"]."',";
					$insert_query.="sysdate()";		
					$insert_query.=")";
					//echo "====".$insert_query."===*<br>";
					mysql_query($insert_query);	
				
				?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно добавлена!";
					</script>		
				<?php					
				}else if($_REQUEST["item_action"]=="edit"){
				
					$query =" UPDATE ".$item_table." SET ";
					$query.=" description = '".$_REQUEST["item_description"]."',";	
                                        $query.=" trunk = '".$_REQUEST["item_trunk"]."'";	
					$query.=" WHERE id = ".$_REQUEST["item_id"]; 
					
					//echo $query;
					$rs = mysql_query($query);	
					
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Изменения успешно сохранены!";
					</script>		
					<?php				
				}else if($_REQUEST["item_action"]=="delete"){
				
					$query =" DELETE FROM ".$item_table." WHERE id = ".$_REQUEST["item_id"]; 					
					//echo $query;
					$rs = mysql_query($query);						
					?>
					<script>			
						location.href = "<?php echo $page;?>.php?message=Запись успешно удалена!";
					</script>		
					<?php				
				}		  	
			
			?>
		   
			<div>										
				<fieldset>
					<legend>
					<?php echo $title;?>
					</legend>	
					<?php include("ivr_title.php");?>								
					
					<table class="table table-condensed table-hover">							
						<thead>
							<TR>							
								<th>Описание</th>
                                                                <th>Канал</th>
								<th><span class="glyphicon glyphicon-calendar"></span> Дата создания</th>
								<th></th>									
							</TR>
						<thead>
						<tbody>
							<?php 
							$counter = 0;
							$query="select * from ".$item_table;
							//echo $query;
							$rs = mysql_query($query);	
							while($row= mysql_fetch_array($rs))								
							{
							$counter++;
							?>
								<tr>									
									<td class="col-xs-4" 
										id="description_<?php echo $row["id"]?>" 
										value="<?php echo $row["description"]?>" 
									> 
										<?php echo $row["description"]?>
									</td>
									<td class="col-xs-4" 
										id="trunk_<?php echo $row["id"]?>" 
										value="<?php echo $row["trunk"]?>" 
									> 
										<?php echo $row["trunk"]?>
									</td>
									<td class="col-xs-2"><?php echo $row["create_date"]?></td>																											
									<td class="col-xs-2 text-right">
										<button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>">
											<span class="glyphicon glyphicon-earphone"></span>
										</button>	
										
										<button data-toggle="modal" type="button" class="btn btn-default" action="edit" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)"  >
											<span class="glyphicon glyphicon-pencil"></span>
										</button>	

										<button type="button" class="btn btn-default"  action="delete" item_id="<?php echo $row["id"]?>" onclick="showItemModal(this)" >
											<span class="glyphicon glyphicon-trash"></span>
										</button>											
									</td>										
								</tr>									
							<?php
							}																																	
							?>							
						</tbody>
					</table>				
					<!-- Button trigger modal -->
					<button class="btn btn-primary btn-sm" data-toggle="modal" action="new" onclick="showItemModal(this)">
					  <span class="glyphicon glyphicon-plus"></span> Добавить
					</button>					
				</fieldset>
			</div>	

			<!--ITEM -->
			<div class="modal fade" id="ItemModal" name="ItemModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
                            <form class="form-horizontal" role="form" id="ItemForm"  method="post" action="custumer.php">
                                <div class="modal-dialog">
                                    <div class="modal-content">                                    
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" ><span class="" id="ItemModalLabelImg"></span>&nbsp;&nbsp;<span id="ItemModalLabel"></span> Клиента</h4>
                                        </div>
                                        <div class="modal-body">		


                                                  <input type="hidden" id="item_action" name="item_action" value="">	
                                                  <input type="hidden" id="item_id" name="item_id" value="">

                                                  <div class="form-group">
                                                        <label class="col-sm-4 control-label">Описание</label>
                                                        <div class="col-sm-8">
                                                          <input type="text" class="form-control" id="item_description" name="item_description" placeholder="Введите текст">
                                                        </div>
                                                  </div>

                                                  <div class="form-group">
                                                        <label class="col-sm-4 control-label">Канал</label>
                                                        <div class="col-sm-8">
                                                          <select id="item_trunk" name="item_trunk" class="form-control">
                                                                <option value="" >не выбран</option>
                                                                <?php
                                                                while($trunk_row= mysql_fetch_array($trunk_rs))
                                                                {
                                                                ?>
                                                                        <option value="<?php echo $trunk_row["channelid"];?>"><?php echo $trunk_row["channelid"];?></option>

                                                                <?php
                                                                }								
                                                                ?>
                                                          </select>							  
                                                        </div>
                                                  </div>						  

                                                </form>
                                        </div>
                                        <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                                                <button class="btn btn-primary"  type="submit">Выполнить</button>
                                        </div>				  
                                    </div>
                                </div>
                            </form>      
			</div>			
			
			<script  type="text/javascript">
			

                                $(document).ready(function() {
                                    $('#ItemForm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                            item_description: {                                                
                                                validators: {
                                                    notEmpty: {
                                                        message: 'не может быть пустым'
                                                    }
                                                }
                                            }
                                        }
                                    });
                                });
                        
                        
				function showItemModal(obj){		
                                        $('#ItemForm').data('bootstrapValidator').resetForm(true);
					var action = obj.getAttribute("action");					
					document.getElementById("item_action").value = action;	
					
					if(action=="new"){
						document.getElementById("ItemModalLabel").innerHTML = "Добавление";
						document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-plus";
						setItemParameters();					
					}else{		
						document.getElementById("item_id").value = obj.getAttribute("item_id");
						setItemParameters();						
						if(action=="edit"){
							document.getElementById("ItemModalLabel").innerHTML = "Редактирование";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-pencil";
						}else if(action=="delete"){
							document.getElementById("ItemModalLabel").innerHTML = "Удаление";	
							document.getElementById("ItemModalLabelImg").className = "glyphicon glyphicon-trash";							
						}
					}					
					$('#ItemModal').modal('show');					
				}
			
				
				function setItemParameters(){	
					
                                    var action = document.getElementById("item_action").value;
                                    var item_id = document.getElementById("item_id").value;		
                                    if(action=="new"){
                                            document.getElementById("item_description").value = "";				
                                    }else{
                                            document.getElementById("item_description").value = document.getElementById("description_" + item_id).getAttribute("value");				
                                    }	
				}
			</script>
			

		  <!--CONTENT END--> 
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	  <?php 
	  include("js.php"); 
	  ?>
  </body>
</html>
