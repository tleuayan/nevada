        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="./../img/logo.png" alt="..." class="img-thumbnail" width="70px" height="70px"> Silk Way Media</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Настройки</a></li>
                <li><a href="#">Профиль</a></li>
                <li><a href="#">Помощь</a></li>
              </ul>
              <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Поиск...">
              </form>
            </div>
          </div>
        </div>
	<?php
	include("conf.php");
	?>	
	<div class="modal fade" id="messageModal" name="messageModal" tabindex="-1" role="dialog" aria-labelledby="ItemModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3><span class="glyphicon glyphicon-envelope"></span>&nbsp;Сообщение</h3>				
                    </div>
                    <div class="modal-body">		
                            <h2><?php echo $_REQUEST["message"];?></h2>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>						
                    </div>				  
                </div>
            </div>
	</div>		
