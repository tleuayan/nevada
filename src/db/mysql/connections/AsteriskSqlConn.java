/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.mysql.connections;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Driver;
//import org.apache.juli.logging.Log;
//import org.apache.juli.logging.LogFactory;

/**
 *
 * @author dh
 */
public class AsteriskSqlConn {

 // private static final Log LOGGER = LogFactory.getLog(MySqlConn.class);
  public Connection connection;

  public AsteriskSqlConn() {
  }

  public Connection createConn() {
    if (connection == null) {
      try {
        //регистрация драйвера
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());

      } catch (Exception e) {
        System.out.println("Exception while register driver: " + e);
       // LOGGER.error(e);
      }
      try {
        //ну и сам запрос в БД
        //connection = DriverManager.getConnection("jdbc:mysql://192.168.0.142:3306/ussd", "root", "passw0rd");
          connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/asterisk", "root", "@WSXxdr5%TGB");
          
      } catch (Exception e) {
        System.out.println("Мега ошибка..." + e);
       // LOGGER.error(e);
      }
    }
    return connection;
  }
}
