/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package table;

public class CallLog {
    
    private String id;       
    private String cdr_id;       
    private String callDate;
    private String ivr_answer_date;
    private String lang;    
    private String inbound_route;
    private String in_trunk;
    private String in_did;
    private String in_callerid;
    private String out_route_type;
    private String out_trunk;
    private String outDid;
    private String out_callerid;
    private String ivr_id;        
    private String status;
    private String hang_up_date;
    private String is_work_time;

    public CallLog() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(String cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getCallDate() {
        return callDate;
    }

    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }

    public String getIvr_answer_date() {
        return ivr_answer_date;
    }

    public void setIvr_answer_date(String ivr_answer_date) {
        this.ivr_answer_date = ivr_answer_date;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getInbound_route() {
        return inbound_route;
    }

    public void setInbound_route(String inbound_route) {
        this.inbound_route = inbound_route;
    }

    public String getIn_trunk() {
        return in_trunk;
    }

    public void setIn_trunk(String in_trunk) {
        this.in_trunk = in_trunk;
    }

    public String getIn_did() {
        return in_did;
    }

    public void setIn_did(String in_did) {
        this.in_did = in_did;
    }

    public String getIn_callerid() {
        return in_callerid;
    }

    public void setIn_callerid(String in_callerid) {
        this.in_callerid = in_callerid;
    }

    public String getOut_route_type() {
        return out_route_type;
    }

    public void setOut_route_type(String out_route_type) {
        this.out_route_type = out_route_type;
    }

    public String getOut_trunk() {
        return out_trunk;
    }

    public void setOut_trunk(String out_trunk) {
        this.out_trunk = out_trunk;
    }

    public String getOutDid() {
        return outDid;
    }

    public void setOutDid(String outDid) {
        this.outDid = outDid;
    }

    public String getOut_callerid() {
        return out_callerid;
    }

    public void setOut_callerid(String out_callerid) {
        this.out_callerid = out_callerid;
    }

    public String getIvr_id() {
        return ivr_id;
    }

    public void setIvr_id(String ivr_id) {
        this.ivr_id = ivr_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHang_up_date() {
        return hang_up_date;
    }

    public void setHang_up_date(String hang_up_date) {
        this.hang_up_date = hang_up_date;
    }

    public String getIs_work_time() {
        return is_work_time;
    }

    public void setIs_work_time(String is_work_time) {
        this.is_work_time = is_work_time;
    }
 
    
    
}
