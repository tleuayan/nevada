/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;
import db.mysql.connections.AsteriskSqlConn;
import entity.Node;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author User
 */
public class Function {
    
    public static String logString = " --------------------------] ";
    public static String defaultWorkTime = "1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;0/09:00-18:00;0/09:00-18:00;";

    public static String main_path = "/var/www/html/sm/sounds";	
    public static String menu_path = main_path + "/menu_voice";	
    public static String nodes_path = main_path + "/ivr/nodes";    
    
    
    public static boolean isWorkTime(String workTimeString){
    
        boolean isWorkTime = false;
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date                
        
        int day = 0;
        if(calendar.get(Calendar.DAY_OF_WEEK)==1){day = 6;}//Воскресение
        else if(calendar.get(Calendar.DAY_OF_WEEK)==2){day = 0;}//Понедельник
        else if(calendar.get(Calendar.DAY_OF_WEEK)==3){day = 1;}//Вторник
        else if(calendar.get(Calendar.DAY_OF_WEEK)==4){day = 2;}//Среда
        else if(calendar.get(Calendar.DAY_OF_WEEK)==5){day = 3;}//Четвег
        else if(calendar.get(Calendar.DAY_OF_WEEK)==6){day = 4;}//Пятница
        else if(calendar.get(Calendar.DAY_OF_WEEK)==7){day = 5;}//Суббота
        
        //System.out.println("day : " + day);
        
        boolean isDay = workTimeString.split(";")[day].split("/")[0].equals("1");
        
        //System.out.println("isDay : " + isDay);
        
        int hour_begin = Integer.parseInt(workTimeString.split(";")[day].split("/")[1].split("-")[0].split(":")[0]);
        int minute_begin = Integer.parseInt(workTimeString.split(";")[day].split("/")[1].split("-")[0].split(":")[1]);
        int hour_end = Integer.parseInt(workTimeString.split(";")[day].split("/")[1].split("-")[1].split(":")[0]);
        int minute_end = Integer.parseInt(workTimeString.split(";")[day].split("/")[1].split("-")[1].split(":")[1]);
        
        //System.out.println("hour_begin : " + hour_begin);
        //System.out.println("minute_begin : " + minute_begin);
        //System.out.println("hour_end : " + hour_end);
        //System.out.println("minute_end : " + minute_end);
        
        //System.out.println("HOUR_OF_DAY : " + calendar.get(Calendar.HOUR_OF_DAY));
        //System.out.println("MINUTE : " + calendar.get(Calendar.MINUTE));
        
        if(isDay){  
            //System.out.println("if 0 ");
            if(hour_begin < calendar.get(Calendar.HOUR_OF_DAY) && hour_end > calendar.get(Calendar.HOUR_OF_DAY)){
                //System.out.println("if 1 ");
                isWorkTime = true;                
            }else if(hour_begin == calendar.get(Calendar.HOUR_OF_DAY) && hour_end == calendar.get(Calendar.HOUR_OF_DAY)){
                //System.out.println("if 2 ");
                if(minute_begin < calendar.get(Calendar.MINUTE) && minute_end > calendar.get(Calendar.MINUTE)){
                    //System.out.println("if 3 ");
                    isWorkTime = true;
                }else{
                    //System.out.println("if 4 ");
                    isWorkTime = false;
                }
            }else if(hour_begin == calendar.get(Calendar.HOUR_OF_DAY)){
                //System.out.println("if 5 ");
                if(minute_begin < calendar.get(Calendar.MINUTE)){
                    //System.out.println("if 6 ");
                    isWorkTime = true;
                }else{
                    //System.out.println("if 7 ");
                    isWorkTime = false;
                }
            }else if(hour_end == calendar.get(Calendar.HOUR_OF_DAY)){
                //System.out.println("if 6 ");
                if(minute_end > calendar.get(Calendar.MINUTE)){
                    //System.out.println("if 9 ");
                    isWorkTime = true;
                }else{
                    //System.out.println("if 10 ");
                    isWorkTime = false;
                }
            }else{
                //System.out.println("if 11 ");
            }            
        }else{
            //System.out.println("if 12 ");
            isWorkTime = false;
        }
        
        System.out.println("isWorkTime : " + isWorkTime);
        return isWorkTime;
    }
    

    

}    
    
  

