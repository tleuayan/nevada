/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author User
 */
public class Hibernate {
    
    public static SessionFactory  getSessionFactory(){   
            Configuration conf = new Configuration();
            conf.configure();
            ServiceRegistryBuilder srBuilder = new ServiceRegistryBuilder();
            srBuilder.applySettings(conf.getProperties());            
            SessionFactory factory = conf.buildSessionFactory();
            return factory;
    }    
}
