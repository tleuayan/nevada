/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package asterisk.agi.connectivity;


import db.mysql.connections.AsteriskSqlConn;
import db.mysql.connections.SmSqlConn;
import entity.InboundRoute;
import entity.Ivr;
import entity.Node;
import entity.NodeLog;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiHangupException;
import org.asteriskjava.fastagi.AgiRequest;
import org.asteriskjava.fastagi.BaseAgiScript;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import table.CallLog;
import utils.Function;
import static utils.Function.logString;
import utils.Hibernate;
import utils.HtmlFormatter;

/**
 *
 * @author User
 */
public class InboundRoutesNativeSqlManager extends BaseAgiScript{

    
    
    @Override
    public void service(AgiRequest ar, AgiChannel ac) throws AgiException {
        
        Logger logger = Logger.getLogger(InboundRoutesNativeSqlManager.class.getName());
        // Создаём handler, который будет записывать лог
        // в файл "LogApp". Символ "%t" указывает на то, что файл 
        // будет располагаться в папке с системными временными файлами.
        try {
                // Оставляем предыдущий handler (будет создаваться файл "LogApp")
                FileHandler fh = new FileHandler("%tLogApp");
                logger.addHandler(fh);

                // Добавляем ещё файл "LogApp.htm".
                HtmlFormatter htmlformatter = new HtmlFormatter();
                FileHandler htmlfile = new FileHandler("/var/www/html/logs/"+ar.getUniqueId()+".html");
                // Устанавливаем html форматирование с помощью класса HtmlFormatter.
                htmlfile.setFormatter(htmlformatter);
                logger.addHandler(htmlfile);


        } catch (SecurityException e) {
                logger.log(Level.SEVERE,
                                "Не удалось создать файл лога из-за политики безопасности.", 
                                e);
        } catch (IOException e) {
                logger.log(Level.SEVERE,
                                "Не удалось создать файл лога из-за ошибки ввода-вывода.",
                                e);
        }
        /*
        logger.log(Level.INFO, "Запись лога с уровнем INFO (информационная)");
        logger.log(Level.WARNING,"Запись лога с уровнем WARNING (Предупреждение)");
        */        
        
        try {
            
            //GET INPUT PARAMETERS
            String ip = ar.getLocalAddress().getHostAddress();
            String in_trunk = ar.getChannel().toString().split("/")[1].split("-")[0];            
            String in_did = ar.getDnid();
            String in_callerid = ar.getCallerIdNumber();

            
            //NEW CallLog            
            CallLog callLog = new CallLog();         
            callLog.setCdr_id(ac.getUniqueId());
            callLog.setIn_trunk(in_trunk);
            callLog.setIn_did(in_did);
            callLog.setIn_callerid(in_callerid);           
            
           
            dbSave(callLog,logger);            
            /*
            logger.log(Level.INFO, " in_trunk : " + in_trunk);
            logger.log(Level.INFO, " in_did : " + in_did);
            logger.log(Level.INFO, " in_callerid : " + in_callerid);
            
            InboundRoute route = getRoute(logger,in_trunk, in_did, in_callerid);
            
            
            logger.log(Level.INFO, " out_trunk : "  + route.getOutTrunk());
            logger.log(Level.INFO, " out_did : "  + route.getOutDid());
            logger.log(Level.INFO, " out_callerid : "  + route.getOutCallerid());            
            
            callLog.setInbound_route(route);
            callLog.setOut_route_type(route.getOutRouteType());
            
            boolean isWorkTime = Function.isWorkTime(route.getWorkTime());
            String lang = "";                        
            long menu_voice;
            Ivr ivr = null;
            
            //SET output log
            callLog.setIsWorkTime(isWorkTime);
            if(route.getOutRouteType().equals("redirect")){                        
                callLog.setOutTrunk(route.getOutTrunk());
                callLog.setOutDid(route.getOutDid());
                callLog.setOutCallerid(route.getOutCallerid());
                callLog.setIvr_id(1);  
                menu_voice = route.getMenu_voice().getId();
            }else{
                ivr = route.getIvr();
                menu_voice = ivr.getMenu_voice().getId();
                callLog.setIvr_id(ivr.getId());                
            }   
            dbUpdate(callLog,logger);   
            
            logger.log(Level.INFO, " ivr : "  + route.getIvr().getId());  
            
            //Проверка рабочего времени
            if(isWorkTime){                
                //Переадресация
                if(route.getOutRouteType().equals("redirect")){
                    //Проверяем задан ли исходящий CALLERID
                    if(!route.getOutCallerid().equals("")){
                        ac.setVariable("CALLERID(NUM)", route.getOutCallerid());
                        ac.setVariable("CALLERID(NAME)",route.getOutCallerid());
                    }else{
                        ac.setVariable("CALLERID(NUM)", in_callerid);
                        ac.setVariable("CALLERID(NAME)",in_callerid);
                    }
                    
                    ac.setVariable("CALLERID(DNID)",route.getOutDid());                    
                    //Переадресовываем звонок
                    String dialString = "SIP/" + route.getOutTrunk() + "/" + route.getOutDid();
                    logger.log(Level.INFO, "DIAL  : " + dialString);                    
                    ac.exec("DIAL",dialString);                    
                }//Меню IVR
                else{
                   ac.answer();                                             
                   ivr = route.getIvr();
                   callLog.setIvr_answer_date(new Date());                   
                   //Выбираем язык 
                   lang = langSelect(ac,ivr.getId(),menu_voice,logger);    
                   callLog.setLang(lang); 
                   dbUpdate(callLog,logger);     
                   ivrGoOnNode(ac,ivr,getNodeById(1,logger),callLog,menu_voice,logger);
                }
                
            }else{//NOT WORK TIME
                ac.streamFile(Function.menu_path + "/" + menu_voice + "/kz/isNotWorkTime" ); 
                ac.streamFile(Function.menu_path + "/" + menu_voice + "/ru/isNotWorkTime" ); 
                logger.log(Level.INFO, "MAIN NOT WORK TIME : ");
            } */   
        /*        
        }catch (AgiHangupException e) {                              
            logger.log(Level.INFO, "MAIN AgiHangupException : " + e.getLocalizedMessage());	
        } catch (AgiException e) {
            logger.log(Level.SEVERE, "SEVERE","MAIN AgiException : " + e.getLocalizedMessage());
            */
        } catch (Exception e) {            
            logger.log(Level.SEVERE, "SEVERE","MAIN Exception : " + e.getLocalizedMessage());	
        } finally{
            
        }    
        
    }
    
    public String langSelect(AgiChannel ac,long ivr_id,long menu_voice_id,Logger logger) throws AgiException{
        logger.log(Level.INFO, " langSelect ");              
        String lang = "";
        String dtmf = "";
        ArrayList<String> langNamesList = new ArrayList<String>();             
        langNamesList.add(Function.nodes_path + "/kz/ivr_" + ivr_id + "_lang");
        langNamesList.add(Function.nodes_path + "/ru/ivr_" + ivr_id + "_lang");        
        while(lang.equals("")){                                 
            dtmf = PlayFilesArray(ac,langNamesList,logger);            
            if(dtmf.equals("1")){
                lang = "kz";            
                //ChangeNode(ac,"0");
            }else if(dtmf.equals("2")){
                lang = "ru";            
                //ChangeNode(ac,"0");
            }else{
                ac.streamFile(Function.menu_path + menu_voice_id + "/kz/noSuchCommand" );
                ac.streamFile(Function.menu_path + menu_voice_id + "/ru/noSuchCommand" );
            }       
        }  
        return lang;
    }
    /*
    private void ivrGoOnNode(AgiChannel ac,Ivr ivr,Node node,CallLog callLog,long menu_voice,Logger logger){ 
        long nodeId = node.getId();
        logger.log(Level.INFO, " ivrGoOnNode nodeId = "  + nodeId); 
        //Записываем и сохраняем логи ноды
        NodeLog nodeLog = new NodeLog();
        nodeLog.setEnterDate(new Date());
        nodeLog.setCall_log(callLog);
        nodeLog.setIvr(ivr);
        nodeLog.setNode(node);
        dbSave(nodeLog,logger);
        
        String dtmf ="";                
        String lang = callLog.getLang();        
        List<Node> children = getNodeChildren(ivr,node.getId(),logger);        
        
        //ОШИБКА В СТРУКТУРЕ | СПИСОК БЕЗ ДЕТЕЙ 
        if(children.size()==0){
            nodeLog.setOutDate(new Date());
            dbUpdate(nodeLog,logger);            
            //INFORM ABOUT ERROR
            logger.log(Level.WARNING, "WARNING","ivrGoOnNode ОШИБКА В СТРУКТУРЕ | СПИСОК БЕЗ ДЕТЕЙ  node = " + nodeId);	
            return;
        }
        
        
        ArrayList<String> nodeFilesList = getNodePlayFiles(ivr,node,lang,menu_voice,logger);        
        ArrayList<Node> pressChildren = new ArrayList<Node>();         
        for(int j=0; j < children.size();j++){            
            if(!children.get(j).getNodeType().equals("info")){                                
                pressChildren.add(children.get(j));
            }
        }                 
        
        int selectedNodeIndex = -1;
        boolean isAutocall = false;
        int autocallNodeIndex = 0;
        //Проверка на автоматичечкое соединиение
        for(int j=0; j < children.size();j++){
             if(children.get(j).getCommand().equals("autocall")){                       
                   isAutocall = true;
                   autocallNodeIndex = j;
             }
        }  
                        
        while (selectedNodeIndex==-1){             
           //Проигрываем пока не нажали
            try{
                ac.setVariable("isAutocall", String.valueOf(isAutocall)); 
                dtmf = PlayFilesArray(ac,nodeFilesList,logger);           
                //Проверяем что нажал                
                
                if(isAutocall && dtmf.equals("")){                     
                    selectedNodeIndex = autocallNodeIndex;
                    
                    
                }else if(dtmf.equals("*") || dtmf.equals("0")){  

                     for(int j=0; j < children.size();j++){
                        if(children.get(j).getCommand().equals(dtmf)){
                              selectedNodeIndex = j;                     
                        }
                    }                

                    if(selectedNodeIndex==-1){
                        //NO SUCH COMMAND
                        ac.streamFile(Function.menu_path + "/" + menu_voice + "/" + lang + "/noSuchCommand" );                
                    }                   
                }else if(Integer.parseInt(dtmf) > pressChildren.size()){               
                    //NO SUCH COMMAND
                    ac.streamFile(Function.menu_path + "/" + menu_voice + "/" + lang + "/noSuchCommand" );                
                }else{                               
                    ArrayList<String> byorderList = new ArrayList<String>();
                    for(int j=0; j < children.size();j++){
                        Node child = children.get(j);                            
                        if(!child.getNodeType().equals("info") && child.getCommand().equals("byorder")){                                
                            byorderList.add(String.valueOf(j));
                        }
                    }                           
                    selectedNodeIndex = Integer.parseInt(byorderList.get(Integer.parseInt(dtmf)-1));       
                }
                
                
            }catch(AgiException e){
                logger.log(Level.INFO, "ivrGoOnNode  AgiException: " + e.getLocalizedMessage());	                
                nodeLog.setOutDate(new Date());
                dbSave(nodeLog,logger);  
            }  
        }//end of while

        //Выбрали ветку, проверяем тип выбранной ветки и задаем следующюю ветку
        Node selectedNode = children.get(selectedNodeIndex);
        long newNodeId = 1;
        if(selectedNode.getNodeType().equals("list")){//список     
            newNodeId = selectedNode.getId(); 
        }else if(selectedNode.getNodeType().equals("call")){//соединение              
            newNodeId = selectedNode.getId(); 
        }else if(selectedNode.getNodeType().equals("back")){//возврат в предыдущее меню                                                    
            newNodeId = node.getParent_id();
        } else if(selectedNode.getNodeType().equals("mainmenu")){//возврат в главное меню                            
            newNodeId = 1;
        }                

        Node nextNode = getNodeById(newNodeId,logger);
        
        if(selectedNode.getNodeType().equals("list") || selectedNode.getNodeType().equals("mainmenu") || selectedNode.getNodeType().equals("back")){
            nodeLog.setOutDate(new Date());
            dbUpdate(nodeLog,logger);
            ivrGoOnNode(ac,ivr,nextNode,callLog,menu_voice,logger);
        }else if(selectedNode.getNodeType().equals("call")){
            makeCall(ac,lang,nextNode,menu_voice,logger);                
        }
        
    }
    */
    public String PlayFilesArray(AgiChannel ac,ArrayList list,Logger logger) throws AgiException{ 
        logger.log(Level.INFO, " PlayFilesArray"); 
        long zeroSecTimeout = 1;
        long oneSecTimeout = 1500; 
        long twoSecTimeout = 2500;     
        int maxDigits = 1;        
        String dtmf = "";        
        while(dtmf.equals("")){            
            for(int i=0;i<list.size();i++){
                String filePath = list.get(i).toString();                                
                dtmf = ac.getData(filePath,zeroSecTimeout,maxDigits);							                                                
                if(dtmf.equals("-1")){//Если файл не найден 
                    dtmf = "";
                    ac.exec("NOOP", Function.logString + " NO SUCH FILE ");
                }else if(!dtmf.equals("")){
                    ac.exec("NOOP", Function.logString + " DTMF : " + dtmf);
                    break;
                }             
            }  
            
            if(dtmf.equals(""))  ac.exec("wait","2");
            
            if(ac.getVariable("isAutocall")!=null){
                if(ac.getVariable("isAutocall").equals("true")){
                    break;
                }
            }
        }
        
        return dtmf;
    }    
     
    public InboundRoute getRoute(Logger logger,String in_trunk,String in_did,String in_callerid){
            logger.log(Level.INFO, " getRoute"); 
            SessionFactory factory = Hibernate.getSessionFactory();
            Session session = factory.openSession();   
            Transaction transaction = null;            
            InboundRoute match_route = null;
            try {
                transaction = session.beginTransaction();            
                Query query = session.getNamedQuery("InboundRoute.findRouteList").setBoolean("isactive", true).setString("inDid", in_did).setString("inCallerid", in_callerid).setString("inTrunk", in_trunk);  
                List results = query.list();                
                for (Iterator<InboundRoute> iter = results.iterator(); iter.hasNext(); ) {
                   InboundRoute route = iter.next();
                   //System.out.println(InboundRoutesManager.Function.logString + route.getDescription());               
                   boolean trunk_match =  false;
                   boolean did_match =  false;
                   boolean callerid_match =  false;
                   //System.out.println(InboundRoutesManager.Function.logString + "route trunk : " + route.getInTrunk());             
                   //System.out.println(InboundRoutesManager.Function.logString + "route callerid :" + route.getInCallerid());             
                   //System.out.println(InboundRoutesManager.Function.logString + "route did : " + route.getInDid());    
                   
                   if(route.getInTrunk() == null || route.getInTrunk().equals("")){
                       trunk_match = true;               
                   }else if(route.getInTrunk().equals(in_trunk)){
                       trunk_match = true;               
                   }

                   if(route.getInCallerid()== null || route.getInCallerid().equals("")){
                       callerid_match = true;               
                   }else if(route.getInCallerid().equals(in_callerid)){
                       callerid_match = true;               
                   }

                   if(route.getInDid()== null || route.getInDid().equals("")){
                       did_match = true;               
                   }else if(route.getInDid().equals(in_did)){
                       did_match = true;               
                   }               

                   //System.out.println(InboundRoutesManager.Function.logString + "trunk_match : " + trunk_match);             
                   //System.out.println(InboundRoutesManager.Function.logString + "callerid_match : " + callerid_match);             
                   //System.out.println(InboundRoutesManager.Function.logString + "did_match : " + did_match);     
                   
                   if(trunk_match && callerid_match && did_match){
                       match_route = route;
                       break;
                   }

                }            

                //System.out.println(InboundRoutesManager.Function.logString + "match_route" + match_route.getDescription());             
                
            } catch (HibernateException e) {
                logger.log(Level.SEVERE, "SEVERE","getRoute HibernateException : " + e.getLocalizedMessage());	
                transaction.rollback();
                e.printStackTrace();
            } finally {
                 session.close();
            }        
            
            return match_route;
    }    
    
    public String getTrunkTech (String trunk,Logger logger){
        logger.log(Level.INFO, " getTrunkTech " + trunk); 
        String trunk_tech = null;
        AsteriskSqlConn conn = new AsteriskSqlConn();                                        
        try{
            
            String query = "select * from trunks where channelid ='"+trunk+"'";       
            ResultSet rs = conn.createConn().createStatement().executeQuery(query);                     
            while (rs.next()) {                  
                trunk_tech = rs.getString("tech");
            }                      
        }catch (Exception e){
            logger.log(Level.SEVERE, "SEVERE","getTrunkTech Exception : " + e.getLocalizedMessage());	
        }finally{
            
        }
        return trunk_tech;
    }    

    public ArrayList<String> getNodePlayFiles(Ivr ivr,Node node,String lang,long menu_voice,Logger logger){           
        logger.log(Level.INFO, " getNodePlayFiles " + node); 
        ArrayList<String> list = new ArrayList<String>();                
        try{
            int dtmfMaxCounter = 0;            
            List<Node> nodes = getNodeChildren(ivr,node.getId(),logger);                   
            for (Iterator<Node> iter = nodes.iterator(); iter.hasNext(); ) {
                Node child = iter.next();  
                if(child.getNodeType().equals("back") || child.getNodeType().equals("mainmenu")){
                    list.add(Function.menu_path + "/" + menu_voice + "/" + lang + "/" + child.getNodeType());            
                }else{
                    list.add(Function.nodes_path + "/" + lang + "/"+child.getId());            
                }

                if(!child.getNodeType().equals("info") && !child.getCommand().equals("autocall")){

                    if(child.getCommand().equals("byorder")){                        
                        dtmfMaxCounter++;
                        list.add(Function.menu_path + "/" + menu_voice + "/" + lang + "/press"+dtmfMaxCounter);


                    }else{                 
                        //PLAY PRESS COMMAND                        
                        list.add(Function.menu_path + "/" + menu_voice + "/" + lang + "/press" + (child.getCommand().equals("*")?"Asterisk":child.getCommand()));
                    }                                        

                }
            }            
        }catch(Exception e){
            logger.log(Level.SEVERE, "SEVERE","getNodePlayFiles Exception : " + e.getLocalizedMessage());            
        }            
        return list;
    }    
    
    private Node getNodeById(long id,Logger logger){
        logger.log(Level.INFO, "getNodeById  : id = " + id);
        SessionFactory factory = Hibernate.getSessionFactory();                          
        Session session = factory.openSession();
        Transaction transaction = null;          
        Node node =null;
        try {
            transaction = session.beginTransaction();            
            Query query = session.getNamedQuery("Node.findById").setLong("id",id);
            node = (Node) query.uniqueResult();
        }catch(HibernateException e){
             logger.log(Level.SEVERE, "SEVERE","getNodeById id="+id+" HibernateException : " + e.getLocalizedMessage());	            
        }finally {
            session.close();
        } 
        return node;             
    
    }
    
    public List<Node> getNodeChildren(Ivr ivr,long parent_id,Logger logger){      
        logger.log(Level.INFO, "getNodeChildren  : parent_id = " + parent_id);
        List<Node> nodes = null;
        SessionFactory factory = Hibernate.getSessionFactory();
        Session session = factory.openSession();   
        Transaction transaction = null;                    
        try {            
            transaction = session.beginTransaction();            
            Query query = session.getNamedQuery("Node.findChildren").setEntity("ivr",ivr).setLong("parent_id", parent_id);
            nodes = (List<Node>) query.list();                        
        }catch(HibernateException e){
            logger.log(Level.SEVERE, "SEVERE","getNodeChildren parent_id="+parent_id+" HibernateException : " + e.getLocalizedMessage());            
        }finally {
            session.close();
        }     
        return nodes;
    }        
    
    private void makeCall(AgiChannel ac,String lang,Node node,long menu_voice,Logger logger){
        logger.log(Level.INFO, "makeCall  : node = " + node);
        boolean isWorkTime = Function.isWorkTime(node.getWorkTime());
        String trunk = node.getTrunk();
        String trunk_tech = getTrunkTech(trunk,logger);        
        String did = (lang.equals("kz")?node.getDstKz():node.getDstRu());        
        try {

            if(isWorkTime){            
                ac.setVariable("CALLERID(DNID)",did);
                ac.exec("DIAL", trunk_tech + "/" + trunk + "/" + did);
            }else{
                ac.streamFile(Function.menu_path + "/" + menu_voice + "/kz/isNotWorkTime" ); 
                ac.streamFile(Function.menu_path + "/" + menu_voice + "/ru/isNotWorkTime" );                             
                logger.log(Level.INFO, "NotWorkTime ");            
            }
        } catch (AgiException ex) {
            logger.log(Level.SEVERE, "AgiException  : " + ex.getLocalizedMessage());            
        }            
    }
    
    private void dbSave(Object o,Logger logger){
        try{            
            SmSqlConn conn = new SmSqlConn();         
            Statement stmt = conn.createConn().createStatement();
            String column_list="";
            String column_value_list="";
            String table_name = "";
            if(o instanceof CallLog){
                table_name = "call_log";
            }
            Field[] fields = o.getClass().getDeclaredFields();
            for(int i=0;i<fields.length;i++){                 
                String column = fields[i].toString();
                String column_value = o.getClass().getField(column).toString();
                if(!column.equals("id") && column_value!=null){
                    if(column_list.equals("")){
                        column_list += column;
                    }else{
                        column_list += ","+column;
                    }

                    if(column_value_list.equals("")){
                        column_value_list += "'" + column_value + "'";
                    }else{
                        column_value_list += ",'"+column_value + "'";
                    }                        
                }
            }
                
            String insertQuery = " insert into "+table_name+" ("+column_list+") VALUES("+column_value_list+")"; 
            System.out.println("insertQuery : "+insertQuery);
            stmt = conn.createConn().createStatement();                 
            stmt.executeUpdate(insertQuery);          
            conn.connection.close();
   
        
        }catch(SQLException e){
            logger.log(Level.SEVERE,"dbSave  : SQLException :" + e.getLocalizedMessage());
        }catch(NoSuchFieldException e){
            logger.log(Level.SEVERE,"dbSave  : NoSuchFieldException :" + e.getLocalizedMessage());     
        }            
        
        /*
        StatelessSession session = Hibernate.getSessionFactory().openStatelessSession();        
        try {                
            Transaction transaction = session.beginTransaction();  
            session.insert(o);
            transaction.commit();               
        }catch(HibernateException e){
            logger.log(Level.SEVERE, "SEVERE","dbSave HibernateException: " + e.getLocalizedMessage());	                
        }finally {
            session.close();            
        }
        */
    }
    
    private void dbUpdate(Object o,Logger logger){ 
        logger.log(Level.INFO,"dbUpdate  : Object = " + o);
        StatelessSession session = Hibernate.getSessionFactory().openStatelessSession();
        try {                
            Transaction transaction= session.beginTransaction();            
            session.update(o);
            transaction.commit();                
        }catch(HibernateException e){
            logger.log(Level.SEVERE, "SEVERE","dbUpdate HibernateException: " + e.getLocalizedMessage());	
        }finally {
            session.close();
        }         
    }
}

