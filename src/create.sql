DROP DATABASE works;
CREATE DATABASE works CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO works.custumer VALUES (1,'2014-05-10 15:01:39','АТФ');
INSERT INTO works.menu_file VALUES (1,'нажмите 1','press1'),(2,'нажмите 2','press2'),(3,'нажмите 3','press3'),(4,'Не действительный набор цифры','noSuchCommand'),(5,'вы позвонили не в рабочее время','notWorkTime');
INSERT INTO works.menu_voice VALUES (1,'2014-05-10 15:01:48','Голос1');
INSERT INTO works.menu_voice_sound VALUES (1,'2014-05-10 17:24:30','asd','ASD','press1',1),(2,'2014-05-10 17:25:15','asdf','asdf','press2',1),(3,'2014-05-10 17:29:17','ASD','ASD','noSuchCommand',1),(4,'2014-05-10 18:44:06','sdfg','sdfg','notWorkTime',1);
INSERT INTO works.ivr VALUES (1,NULL,'FOREIGN',NULL),(2,'2014-05-10 15:01:58','????????????????',1);
INSERT INTO works.node VALUES (1,NULL,NULL,'FOREIGN',NULL,NULL,'',NULL,0,0,NULL,NULL,NULL,NULL,NULL),(2,NULL,NULL,NULL,NULL,NULL,NULL,'LANG',NULL,NULL,NULL,'zc','acsd',NULL,2),(3,'',NULL,'Инфо','','','','info',1,1,'','SASD','SD','1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;0/09:00-18:00;0/09:00-18:00;',2),(4,'byorder',NULL,'Соединение','333','333','','call',1,2,'asterisk2','ASasc','3333','1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/00:00-23:59;1/00:00-23:59;',2),(5,'byorder',NULL,'Список','','','','list',1,3,'','фысфыс','фысыф','1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;0/09:00-18:00;0/09:00-18:00;',2),(6,'byorder',NULL,'Соединение','333','333','','call',5,1,'asterisk2','ФЫс','ФЫС','1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;',2),(7,'byorder',NULL,'Список','','','','list',5,2,'','фыв','ФЫВФ','1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;0/09:00-18:00;0/09:00-18:00;',2);
INSERT INTO works.inbound_route VALUES (1,NULL,'111 to 333','','777','','','','','ivr','',1,'1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;1/00:00-23:59;',1,2);


--AUTOCALL 
CREATE TABLE IF NOT EXISTS autocall_request (
id INT NOT NULL AUTO_INCREMENT,
requestDate  datetime NOT NULL,
user   varchar(20) NOT NULL,
telNumbers   varchar(100) NOT NULL,
workTime  varchar(20) NOT NULL,
waitTime  INT NOT NULL,
retryTime  INT NOT NULL,
menu   varchar(20),
AON   varchar(100),
callDate  datetime,
callStatus   varchar(20),
callAnswerDuration INT,
callDuration  INT,
callNumbers  INT,
callTelNo   varchar(20),
requestStatus   varchar(20),
planedCallDate  datetime,
PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS autocall_logs (
id INT NOT NULL AUTO_INCREMENT,
request_id  INT NOT NULL,
callDate  datetime,
answerDate  datetime,
hangUpDate  datetime,
callStatus   varchar(20),
tellNo   varchar(20),
PRIMARY KEY (id),
FOREIGN KEY (request_id) REFERENCES autocall_request(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






