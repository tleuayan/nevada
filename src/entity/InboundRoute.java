/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javafx.scene.control.Menu;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "inbound_route")
@NamedQueries({
    @NamedQuery(name = "InboundRoute.findAll", query = "SELECT i FROM InboundRoute i"),
    @NamedQuery(name = "InboundRoute.findById", query = "SELECT i FROM InboundRoute i WHERE i.id = :id"),    
    @NamedQuery(name = "InboundRoute.findRouteList", query = "SELECT i FROM InboundRoute i WHERE i.isactive=:isactive and (i.inDid = :inDid or i.inCallerid = :inCallerid or i.inTrunk = :inTrunk)"),    
    @NamedQuery(name = "InboundRoute.findByCustumer", query = "SELECT i FROM InboundRoute i WHERE i.custumer = :custumer"),    
    @NamedQuery(name = "InboundRoute.findByInTrunk", query = "SELECT i FROM InboundRoute i WHERE i.inTrunk = :inTrunk"),
    @NamedQuery(name = "InboundRoute.findByInDid", query = "SELECT i FROM InboundRoute i WHERE i.inDid = :inDid"),
    @NamedQuery(name = "InboundRoute.findByInCallerid", query = "SELECT i FROM InboundRoute i WHERE i.inCallerid = :inCallerid"),
    @NamedQuery(name = "InboundRoute.findByOutRouteType", query = "SELECT i FROM InboundRoute i WHERE i.outRouteType = :outRouteType"),
    @NamedQuery(name = "InboundRoute.findByOutTrunk", query = "SELECT i FROM InboundRoute i WHERE i.outTrunk = :outTrunk"),
    @NamedQuery(name = "InboundRoute.findByOutDid", query = "SELECT i FROM InboundRoute i WHERE i.outDid = :outDid"),
    @NamedQuery(name = "InboundRoute.findByOutCallerid", query = "SELECT i FROM InboundRoute i WHERE i.outCallerid = :outCallerid"),
    @NamedQuery(name = "InboundRoute.findByIvr", query = "SELECT i FROM InboundRoute i WHERE i.ivr = :ivr"),    
    @NamedQuery(name = "InboundRoute.findByIsactive", query = "SELECT i FROM InboundRoute i WHERE i.isactive = :isactive")
})

public class InboundRoute{
    private long id;    
    private String description;    
    private Date createDate;        
    private Custumer custumer;    
    private String inTrunk;
    private String inDid;
    private String inCallerid;
    private String outRouteType;
    private String outTrunk;
    private String outDid;
    private String outCallerid;
    private Ivr ivr;
    private MenuVoice menu_voice;
    private int priority;
    private boolean isactive;
    private String workTime;
    public static String defaultWorkTime = "1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;1/09:00-18:00;0/09:00-18:00;0/09:00-18:00;";
    
    public InboundRoute() {
    }
    
    public InboundRoute(String description,Custumer custumer, Ivr ivr,Date createDate,int priority, boolean isactive) {        
        this.description = description;
        this.custumer = custumer;
        this.ivr = ivr;
        this.priority = priority;
        this.isactive = isactive;
        this.createDate = createDate;
    }
    
    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date create_date) {        
        this.createDate = createDate;
    }

    @ManyToOne
    public Custumer getCustumer() {
        return custumer;
    }

    public void setCustumer(Custumer custumer) {
        this.custumer = custumer;
    }
    
    @Column(name="in_trunk")
    public String getInTrunk() {
        return inTrunk;
    }

    public void setInTrunk(String inTrunk) {
        this.inTrunk = inTrunk;
    }

    @Column(name="in_did")
    public String getInDid() {
        return inDid;
    }

    public void setInDid(String inDid) {
        this.inDid = inDid;
    }

    
    @Column(name="in_callerid")
    public String getInCallerid() {
        return inCallerid;
    }

    public void setInCallerid(String inCallerid) {
        this.inCallerid = inCallerid;
    }

    @Column(name="out_route_type")
    public String getOutRouteType() {
        return outRouteType;
    }

    public void setOutRouteType(String outRouteType) {
        this.outRouteType = outRouteType;
    }

    @Column(name="out_trunk")
    public String getOutTrunk() {
        return outTrunk;
    }

    public void setOutTrunk(String outTrunk) {
        this.outTrunk = outTrunk;
    }

    @Column(name="out_did")
    public String getOutDid() {
        return outDid;
    }

    public void setOutDid(String outDid) {
        this.outDid = outDid;
    }

    @Column(name="out_callerid")
    public String getOutCallerid() {
        return outCallerid;
    }

    public void setOutCallerid(String outCallerid) {
        this.outCallerid = outCallerid;
    }

    @ManyToOne
    public Ivr getIvr() {
        return ivr;
    }

    public void setIvr(Ivr ivr) {
        this.ivr = ivr;
    }
    
    @ManyToOne
    public MenuVoice getMenu_voice() {
        return menu_voice;
    }

    public void setMenu_voice(MenuVoice menu_voice) {
        this.menu_voice= menu_voice;
    }

    
    
    @Column(name="priority")
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Column(name="is_active")
    public boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    @Column(name="work_time")
    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }
    
}
