/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import static utils.Function.logString;
import utils.Hibernate;

/**
 *
 * @author User
 */
@Entity
@Table(name = "call_log")
public class CallLog {
    
    private long id;       
    private String cdr_id;       
    private Date callDate;
    private Date ivr_answer_date;
    private String lang;    
    private InboundRoute inbound_route;
    private String inTrunk;
    private String inDid;
    private String inCallerid;
    private String outRouteType;
    private String outTrunk;
    private String outDid;
    private String outCallerid;
    private long ivr_id;        
    private String status;
    private Date hangUpDate;
    private boolean isWorkTime;

    public CallLog() {
        this.callDate= new Date();
    }
    
    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    @Column(name="call_date")
    public Date getCallDate() {
        return callDate;
    }

    public void setCallDate(Date callDate) {
        this.callDate = callDate;
    }

    @Column(name="ivr_answer_date")
    public Date getIvr_answer_date() {
        return ivr_answer_date;
    }

    public void setIvr_answer_date(Date ivr_answer_date) {
        this.ivr_answer_date = ivr_answer_date;
    }    
    
    @Column(name="lang")
    public String getLang() {
        return lang;
    }



    public void setLang(String lang) {
        this.lang = lang;
    }

    
    
    @Column(name="hang_up_date")
    public Date getHangUpDate() {
        return hangUpDate;
    }

    public void setHangUpDate(Date hangUpDate) {
        this.hangUpDate = hangUpDate;
    }

    @ManyToOne
    public InboundRoute getInbound_route() {
        return inbound_route;
    }

    public void setInbound_route(InboundRoute inbound_route) {
        this.inbound_route = inbound_route;
    }

    @Column(name="in_trunk")
    public String getInTrunk() {
        return inTrunk;
    }

    public void setInTrunk(String inTrunk) {
        this.inTrunk = inTrunk;
    }

    @Column(name="in_did")
    public String getInDid() {
        return inDid;
    }

    public void setInDid(String inDid) {
        this.inDid = inDid;
    }

    @Column(name="in_callerid")
    public String getInCallerid() {
        return inCallerid;
    }

    public void setInCallerid(String inCallerid) {
        this.inCallerid = inCallerid;
    }

    @Column(name="out_route_type")
    public String getOutRouteType() {
        return outRouteType;
    }

    public void setOutRouteType(String outRouteType) {
        this.outRouteType = outRouteType;
    }

    @Column(name="out_trunk")
    public String getOutTrunk() {
        return outTrunk;
    }

    public void setOutTrunk(String outTrunk) {
        this.outTrunk = outTrunk;
    }

    @Column(name="out_did")
    public String getOutDid() {
        return outDid;
    }

    public void setOutDid(String outDid) {
        this.outDid = outDid;
    }

    @Column(name="out_callerid")
    public String getOutCallerid() {
        return outCallerid;
    }

    public void setOutCallerid(String outCallerid) {
        this.outCallerid = outCallerid;
    }

    @Column(name="ivr_id")
    public long getIvr_id() {
        return ivr_id;
    }

    public void setIvr_id(long ivr_id) {
        this.ivr_id = ivr_id;
    }
    
    
    public Ivr ivr() {
        SessionFactory factory = Hibernate.getSessionFactory();            
        Session session = factory.openSession();
        Transaction transaction = null;          
        Ivr ivr =null;
        try {
            transaction = session.beginTransaction();            
            Query query = session.getNamedQuery("Ivr.findById").setLong("id",ivr_id);
            ivr = (Ivr) query.uniqueResult();
        }catch(HibernateException e){
            System.out.println(logString + " getIvr HibernateException : " + e.getLocalizedMessage());
        }finally {
        
        } 
        return ivr;  
    }

    @Column(name="status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
 
    @Column(name="cdr_id")
    public String getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(String cdr_id) {
        this.cdr_id = cdr_id;
    }

    @Column(name="is_work_time")
    public boolean isIsWorkTime() {
        return isWorkTime;
    }

    public void setIsWorkTime(boolean isWorkTime) {
        this.isWorkTime = isWorkTime;
    }
    
    
    
    
}
