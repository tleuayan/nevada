/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "ivr")
@NamedQueries({
    @NamedQuery(name = "Ivr.findAll", query = "SELECT i FROM Ivr i"),
    @NamedQuery(name = "Ivr.findById", query = "SELECT i FROM Ivr i WHERE i.id = :id"),
    @NamedQuery(name = "Ivr.findByMenuVoice", query = "SELECT i FROM Ivr i WHERE i.menu_voice = :menu_voice")
})    
public class Ivr{
    private long id;    
    private Date createDate;    
    private String description;
    private MenuVoice menu_voice;    
    private boolean isactive;

    public Ivr() {
    }

    public Ivr(Date createDate,String description,MenuVoice menu_voice,String workTime) {
        this.createDate = createDate;
        this.description = description;        
        this.menu_voice = menu_voice;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "description")    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    public MenuVoice getMenu_voice() {
        return menu_voice;
    }

    public void setMenu_voice(MenuVoice menu_voice) {
        this.menu_voice = menu_voice;
    }

    @Column(name="is_active")
    public boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }    
}
