/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "menu_file")
@NamedQueries({
    @NamedQuery(name = "MenuFile.findAll", query = "SELECT m FROM MenuFile m"),
    @NamedQuery(name = "MenuFile.findById", query = "SELECT m FROM MenuFile m WHERE m.id = :id"),    
    @NamedQuery(name = "MenuFile.findByFileName", query = "SELECT m FROM MenuFile m WHERE m.fileName = :fileName")})
public class MenuFile{
    private long id;
    private String description;
    private String fileName;

    public MenuFile() {
    }

    public MenuFile(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")    
    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

     @Column(name="file_name")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }   
    
}
