/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "node_log")
public class NodeLog {
    
    private long id;    
    private CallLog call_log;    
    private Ivr ivr;
    private Node node;
    private Date enterDate;
    private Date outDate;

    
    public NodeLog() {       
    }
        
    @Id
    @GeneratedValue
    @Column(name = "id")    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public CallLog getCall_log() {
        return call_log;
    }

    public void setCall_log(CallLog call_log) {
        this.call_log = call_log;
    }

    @ManyToOne
    public Ivr getIvr() {
        return ivr;
    }

    public void setIvr(Ivr ivr) {
        this.ivr = ivr;
    }

    @ManyToOne
    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Column(name="enter_date")
    public Date getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(Date enterDate) {
        this.enterDate = enterDate;
    }

     @Column(name="out_date")
    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }
    
    
    
}
