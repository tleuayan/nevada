/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "ivr_var_log")
@NamedQueries({
    @NamedQuery(name = "IvrVarLog.findAll", query = "SELECT i FROM IvrVarLog i"),
    @NamedQuery(name = "IvrVarLog.findById", query = "SELECT i FROM IvrVarLog i WHERE i.id = :id"),
    @NamedQuery(name = "IvrVarLog.findByIvr", query = "SELECT i FROM IvrVarLog i WHERE i.ivr = :ivr"),
    @NamedQuery(name = "IvrVarLog.findByNode", query = "SELECT i FROM IvrVarLog i WHERE i.node = :nodeId"),
    @NamedQuery(name = "IvrVarLog.findByCallLog", query = "SELECT i FROM IvrVarLog i WHERE i.call_log = :call_log"),
    @NamedQuery(name = "IvrVarLog.findByNodeLog", query = "SELECT i FROM IvrVarLog i WHERE i.node_log = :node_log"),
    @NamedQuery(name = "IvrVarLog.findByName", query = "SELECT i FROM IvrVarLog i WHERE i.name = :name"),
    @NamedQuery(name = "IvrVarLog.findByValue", query = "SELECT i FROM IvrVarLog i WHERE i.value = :value")})
public class IvrVarLog{
    private long id;
    private Ivr ivr;
    private Node node;
    private CallLog call_log;
    private NodeLog node_log;
    private String name;
    private String value;

    public IvrVarLog() {
    }

    public IvrVarLog(Integer id) {
        this.id = id;
    }

    public IvrVarLog(long id, Ivr ivr, Node node, CallLog call_log, NodeLog node_log) {
        this.id = id;
        this.ivr = ivr;
        this.node = node;
        this.call_log = call_log;
        this.node_log = node_log;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @ManyToOne
    public Ivr getIvr() {
        return ivr;
    }

    public void setIvr(Ivr ivr) {
        this.ivr = ivr;
    }

    @ManyToOne
    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @ManyToOne
    public CallLog getCall_log() {
        return call_log;
    }

    public void setCall_log(CallLog call_log) {
        this.call_log = call_log;
    }

    @ManyToOne
    public NodeLog getNode_log() {
        return node_log;
    }

    public void setNode_log(NodeLog node_log) {
        this.node_log = node_log;
    }

    @Column(name="name", nullable = false)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Column(name="value", nullable = false)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
   
}
