/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "menu_voice")
@NamedQueries({
    @NamedQuery(name = "MenuVoice.findAll", query = "SELECT m FROM MenuVoice m"),
    @NamedQuery(name = "MenuVoice.findById", query = "SELECT m FROM MenuVoice m WHERE m.id = :id")
})
public class MenuVoice implements Serializable {
    private long id;
    private String description;
    private Date createDate;

    public MenuVoice() {
    }

    public MenuVoice(long id) {
        this.id = id;
    }
    
    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
