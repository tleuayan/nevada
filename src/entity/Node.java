/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import static utils.Function.logString;
import utils.Hibernate;

/**
 *
 * @author User
 */
@Entity
@Table(name = "node")
@NamedQueries({
    @NamedQuery(name = "Node.findAll", query = "SELECT i FROM Node i"),
    @NamedQuery(name = "Node.findById", query = "SELECT i FROM Node i WHERE i.id = :id"),
    @NamedQuery(name = "Node.findByIvr", query = "SELECT i FROM Node i WHERE i.ivr = :ivr"),
    @NamedQuery(name = "Node.findByParent", query = "SELECT i FROM Node i WHERE i.parent_id = :parent_id"),
    @NamedQuery(name = "Node.findChildren", query = "SELECT i FROM Node i WHERE i.ivr = :ivr and i.parent_id = :parent_id order by i.priority")
})
public class Node {
    private long id;
    private Ivr ivr;
    private Date createDate;
    private String description;    
    private long parent_id;
    private String nodeType;
    private String command;
    private String trunk;
    private String dstKz;
    private String dstRu;
    private String txtKz;
    private String txtRu;
    private int priority;
    private boolean isactive;    
    private String workTime;
    

    public Node() {
    }

    public Node(Ivr ivr, String description, String nodeType, int priority, boolean isactive) {        
        this.ivr = ivr;
        this.description = description;        
        this.nodeType = nodeType;
        this.priority = priority;
        this.isactive = isactive;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public Ivr getIvr() {
        return ivr;
    }

    public void setIvr(Ivr ivr) {
        this.ivr = ivr;
    }

    @Column(name="create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public Node parent() {
        SessionFactory factory = Hibernate.getSessionFactory();            
        Session session = factory.openSession();
        Transaction transaction = null;          
        Node node =null;
        try {
            transaction = session.beginTransaction();            
            Query query = session.getNamedQuery("Node.findById").setLong("id",parent_id);
            node = (Node) query.uniqueResult();
        }catch(HibernateException e){
            System.out.println(logString + " getIvr HibernateException : " + e.getLocalizedMessage());
        }finally {
        
        } 
        return node;    
    }

    @Column(name="parent_id")
    public long getParent_id() {
        return parent_id;
    }

    public void setParent_id(long parent_id) {
        this.parent_id = parent_id;
    }
    
    

    @Column(name="node_type")
    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    @Column(name="command")
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

     @Column(name="trunk")
    public String getTrunk() {
        return trunk;
    }

    public void setTrunk(String trunk) {
        this.trunk = trunk;
    }

    @Column(name="dst_kz")
    public String getDstKz() {
        return dstKz;
    }

    public void setDstKz(String dstKz) {
        this.dstKz = dstKz;
    }

    @Column(name="dst_ru")
    public String getDstRu() {
        return dstRu;
    }

    
    public void setDstRu(String dstRu) {
        this.dstRu = dstRu;
    }

    @Column(name="txt_kz",length = 500)    
    public String getTxtKz() {
        return txtKz;
    }

    public void setTxtKz(String txtKz) {
        this.txtKz = txtKz;
    }

    @Column(name="txt_ru",length = 500)
    public String getTxtRu() {
        return txtRu;
    }

    public void setTxtRu(String txtRu) {
        this.txtRu = txtRu;
    }

    @Column(name="priority")
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
     
    @Column(name="is_active")
    public boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    @Column(name="work_time")
    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    } 
    
}
