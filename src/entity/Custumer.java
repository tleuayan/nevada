/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "custumer")
@NamedQueries({
    @NamedQuery(name = "Custumer.findAll", query = "SELECT c FROM Custumer c"),
    @NamedQuery(name = "Custumer.findById", query = "SELECT c FROM Custumer c WHERE c.id = :id"),
    @NamedQuery(name = "Custumer.findByCreateDate", query = "SELECT c FROM Custumer c WHERE c.createDate = :createDate"),
    @NamedQuery(name = "Custumer.findByDescription", query = "SELECT c FROM Custumer c WHERE c.description = :description")})
public class Custumer {
    
    private long id;    
    private Date createDate;
    private String description;
    private String trunk;

    public Custumer() {
    }

    public Custumer(Date createDate,String description) {
        this.createDate = createDate;
        this.description = description;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name="create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }  

    @Column(name="trunk")
    public String getTrunk() {
        return trunk;
    }

    public void setTrunk(String trunk) {
        this.trunk = trunk;
    }
    
    
}
