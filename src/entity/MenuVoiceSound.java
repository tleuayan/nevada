/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author User
 */
@Entity
@Table(name = "menu_voice_sound")
@NamedQueries({
    @NamedQuery(name = "MenuVoiceSound.findAll", query = "SELECT m FROM MenuVoiceSound m"),
    @NamedQuery(name = "MenuVoiceSound.findById", query = "SELECT m FROM MenuVoiceSound m WHERE m.id = :id"),
    @NamedQuery(name = "MenuVoiceSound.findByMenuVoice", query = "SELECT m FROM MenuVoiceSound m WHERE m.menu_voice = :menu_voice"),
    @NamedQuery(name = "MenuVoiceSound.findByFileName", query = "SELECT m FROM MenuVoiceSound m WHERE m.fileName = :fileName")
})
public class MenuVoiceSound {
    private long id;
    private MenuVoice menu_voice;
    private Date createDate;
    private String descriptionRu;
    private String descriptionKz;
    private String fileName;

    public MenuVoiceSound() {
    }

    public MenuVoiceSound(long id) {
        this.id = id;
    }

    public MenuVoiceSound(long id, MenuVoice menu_voice) {
        this.id = id;
        this.menu_voice = menu_voice;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    public MenuVoice getMenu_voice() {
        return menu_voice;
    }

    public void setMenu_voice(MenuVoice menu_voice) {
        this.menu_voice = menu_voice;
    }
    
    @Column(name="create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name="description_ru")
    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    @Column(name="description_kz")
    public String getDescriptionKz() {
        return descriptionKz;
    }

    public void setDescriptionKz(String descriptionKz) {
        this.descriptionKz = descriptionKz;
    }

    @Column(name="file_name")    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
       
}
